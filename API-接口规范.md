# 系统接口请求规则

## 列表接口 - POST

```
{
	"page": 1, // 当前页
	"pageSize": 10, // 当前页展示大小
	"orderBy": "", // 排序字段名（预留）
	"orderMethod": "", // 排序方法（desc: 倒序，esc: 正序，字段预留）
	"conditions": {
		"field 1": "data 1", // 查询条件
		...
	}
}
```

## 表单提交接口 - POST

```
{
	"id": "", // 提交修改信息时ID存在，如果不存在，则表示新增数据
	"field 1": "data 1", // 其他表单字段
	...
}
```

## 表单信息获取接口 - GET

```
http://host:port/模块名/功能名/方法名/id/表单ID
```

> 例：http://localhost:8080/exampleModule/example/get/id/0b430136b6f74658a93f1534585d5dad

## 删除表单接口 - GET

```
http://host:port/模块名/功能名/方法名/表单ID
```

> 例：http://localhost:8080/exampleModule/example/delete/id/0b430136b6f74658a93f1534585d5dad

## 上传文件接口 - POST

```
{
	"id": "", // 文件ID（预留，断点续传）
	"name": "文件名",
	"uploadSize": 240, // 单个上传包上传数据大小（预留，断点续传）
	"part": 1, // 当前上传包编号（预留，断点续传）
	"file": "base64" // base64编码的文件信息字符串
}
```

## 下载文件接口 - GET

```
name=实际文件名.扩展名&token=token // 直接用实际文件名下载，请求中必须带token保证系统内部人员提交，同时预留后续对外无账号人员下载需求
id=id // 根据ID下载，请求头内带token，模块与模块交互，或者系统内人员从页面点击按钮下载
```

# 系统接口响应规则

## 表单详情接口

```
{
	"code": 200,
	"message": "xx异常信息",
	"data": {
	
		"field 1": "data 1",
		"field 2": "data 2"	
	}
	
}
```

## 列表接口

```
{
	"code": 200,
	"message": "xx异常信息",
	"data": {
		"total": 100,
		"data": []
	}
}
```

## 树型目录接口

```
{
	"code": 200,
	"message": "xx异常信息",
	"data": {
		"id": "askdjfsdfasdfad",
		"field1": "data1", // 树形结构数据包含的其他信息
		"field2": "data2",
		"children": [
			{
				"id": "askdjfsdfasdfad",
				"field1": "data1", // 树形结构数据包含的其他信息
				"field2": "data2",
				"children": []
			}
		]
	}
}
```

## 表单提交接口

```
{
	"code": 200,
	"message": "xx异常信息",
	"data": "ID" // 表单提交保存成功后生成的ID，前端在保存后，用ID调用获取数据接口进行二次渲染，保证数据保存成功
}
```

## 不需要数据返回接口

```
{
	"code": 200,
	"message": "xx异常信息",
	"data": null
}
```