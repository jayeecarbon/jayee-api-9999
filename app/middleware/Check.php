<?php
declare (strict_types = 1);

namespace app\middleware;

use think\facade\Config;
use think\cache\driver\Redis;
use think\Exception;

class Check
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */

    public function handle($request, \Closure $next)
    {
        try {
            $token = $request->header('token');
            if($token != 'null' && $token != null) {
                $redis = new Redis(Config::get('cache.stores.redis'));
                $data_redis = $redis->get($token);
                if ($data_redis) {
                    $data_redis = json_decode($data_redis, true);
                    $request->withMiddleware(['data_redis' => $data_redis]);
                } else {
                    throw new Exception('token过期', 403);
                }
            } else {
                if ($request->pathinfo() != 'user/register') {
                    throw new Exception('token失效', 403);
                }
            }
        } catch (Exception $err){
            return json(['code'=>$err->getCode(), 'message'=>$err->getMessage()]);
        }
        
        return $next($request);
    }
}
