<?php
declare (strict_types = 1);

namespace app\controller\organization;

use app\BaseController;
use app\model\organization\CarbonCulateModel;
use app\model\organization\CalculateExampleModel;
use app\controller\system\Organization;
use app\model\system\OrganizationModel;
use app\model\system\FileModel;
use app\model\admin\UnitModel;
use app\model\system\OperationModel;
use app\model\data\ParamModel;

class CarbonCulate extends BaseController
{
    /**
     * 核算列表
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function index() {
        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : ParamModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : ParamModel::pageIndex;
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';

        // 过滤条件：组织、状态、核算年
        $filters = [
            'organization_id' => isset($_GET['organization_id']) ? $_GET['organization_id'] : '',
            'state' => isset($_GET['state']) ? $_GET['state'] : '',
            'calculate_year' => isset($_GET['calculate_year']) ? $_GET['calculate_year'] : '',
            'main_organization_id' => $main_organization_id
        ];

        $list = CarbonCulateModel::getCarbonCulates($page_size, $page_index, $filters)->toArray();
        $organization_all = OrganizationModel::getAllFiledOrganizations($main_organization_id)->toArray();
        $organization = [];
        foreach ($organization_all as $key => $value) {
            $organization[$key]['id'] = $value['id'];
            $organization[$key]['name'] = $value['name'];
        }
        $state_map = ParamModel::ORG_STATE_MAP;
        $list_new = [];
        foreach ($list['data'] as $k => $v) {
            if (isset($state_map[$v['state']])) {
                $v['state_name'] = $state_map[$v['state']];
            } else {
                $v['state_name'] = '';
            }

            $list_new[$k] = $v;
        }

        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['total'] = $list['total'];
        $data['data']['organization_all'] = $organization;
        array_unshift($organization, ['id'=>0,'name'=>'全部']);
        $data['data']['organization_map'] = $organization;
        $data['data']['state_map'] = ParamModel::ORG_STATE_SELECT_MAP;

        return json($data);
    }

    /**
     * see 核算详情
     *
     * @author wuyinghua
     * @return void
     */
    public function see() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $calculate_month = isset($_GET['calculate_month']) ? $_GET['calculate_month'] : '1';
        $info = CarbonCulateModel::getCarbonCulate($id);
        if ($info == null) {
            return json(['code'=>201, 'message'=>"未查询到核算相关信息"]);
        }

        $detail_list = CarbonCulateModel::getCalculateDetails($id, $info['calculate_year'], $calculate_month)->toArray();
        $state_map = ParamModel::ORGDETAIL_STATE_MAP;
        $detail_list_new = [];
        foreach ($detail_list as $k => $v) {
            if (isset($state_map[$v['state']])) {
                $v['state_name'] = $state_map[$v['state']];
            } else {
                $v['state_name'] = '';
            }

            $detail_list_new[$k] = $v;
        }

        // $detail_list_new = self::getOrgTree($detail_list_new, $info['organization_id']); // 如需要按照组织树显示放开

        $data['code'] = 200;
        $data['data'] = $info;
        $data['data']['detail_list'] = $detail_list_new;
        $data['data']['state_map'] = ParamModel::ORGDETAIL_STATE_SELECT_MAP;

        return json($data);
    }

    /**
     * dataSee 查看数据
     *
     * @author wuyinghua
     * @return void
     */
    public function dataSee() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $org_data = CarbonCulateModel::getCarbonCulateDetail($id);
        $facility_data = CarbonCulateModel::getCarbonCulateFacility($id)->toArray();
        if ($org_data == null) {
            return json(['code'=>201, 'message'=>"未查询到组织相关信息"]);
        }

        $state_map = ParamModel::ORGDETAIL_STATE_MAP;
        $org_data['state_name'] = $state_map[$org_data['state']];

        $data['code'] = 200;
        $data['data']['org_data'] = $org_data;
        $data['data']['facility_data'] = $facility_data;

        return json($data);
    }

    /**
     * dataInfo 数据详情
     *
     * @author wuyinghua
     * @return void
     */
    public function dataInfo() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $org_data = [];
        $facility_data = [];
        if (isset($_GET['data_type']) && $_GET['data_type'] == ParamModel::ORGDETAIL_DATA_ORG) {
            $org_data = CarbonCulateModel::getCarbonCulateDetail($id);
            if ($org_data == null) {
                return json(['code'=>201, 'message'=>"未查询到组织相关信息"]);
            }
            $org_data['files'] = FileModel::getFileByIds($org_data['files'])->toArray();
            $emission_data = CarbonCulateModel::getCarbonCulateDetailOrgEmission($id)->toArray();

            // 活动数据单位名称
            foreach ($emission_data as $k => &$v) {
                $unit_id_arr = explode(',', $v['active_unit']);
                if ($unit_id_arr) {
                    $unit_data = UnitModel::findUnit($unit_id_arr[1]);
                    $v['active_unit_name'] = $unit_data['name'];
                }
            }
        } else {
            $facility_data = CarbonCulateModel::getCarbonCulateFacilityDetail($id);

            if ($facility_data == null) {
                return json(['code'=>201, 'message'=>"未查询到设备相关信息"]);
            }
            $facility_datas = CarbonCulateModel::getCarbonCulateFacility($facility_data['organization_calculate_detail_id'])->toArray();
            foreach ($facility_datas as $key => $value) {
                if ($facility_data['id'] == $value['id']) {
                    $facility_data['emissions'] = $value['emissions'];
                }
            }

            $facility_data['files'] = FileModel::getFileByIds($facility_data['files'])->toArray();
            $emission_data = CarbonCulateModel::getCarbonCulateFacilityDetailEmission($facility_data['organization_calculate_detail_id'], $facility_data['facility_no'])->toArray();

            // 活动数据单位名称
            foreach ($emission_data as $k => &$v) {
                $unit_id_arr = explode(',', $v['active_unit']);
                if ($unit_id_arr) {
                    $unit_data = UnitModel::findUnit($unit_id_arr[1]);
                    $v['active_unit_name'] = $unit_data['name'];
                }
            }
        }

        $data['code'] = 200;
        $data['data']['org_data'] = $org_data;
        $data['data']['facility_data'] = $facility_data;
        $data['data']['emission_data'] = $emission_data;

        return json($data);
    }

    /**
     * factorInfo 查看因子
     * 
     * @author wuyinghua
     * @return void
     */
    public function factorInfo() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $info = CarbonCulateModel::getCarbonCulateDetailEmission($id);
        if ($info == null) {
            return json(['code'=>201, 'message'=>"未查询到因子相关信息"]);
        }

        $list = CarbonCulateModel::getInfoById($id);
        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }

    /**
     * seeTree 新增核算组织树回显
     *
     * @author wuyinghua
     * @return void
     */
    public function seeTree() {
        $data_redis = $this->request->middleware('data_redis');
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
        $organization_id = isset($_GET['id']) ? $_GET['id'] : '';
    
        if ($organization_id == $main_organization_id) {
            // 显示用户所在集团下的所有组织
            $organizations_list = OrganizationModel::getAllOrganizations()->toArray();
        } else {
            // 显示id或pid为organization_id的组织
            $organizations_list = OrganizationModel::getBranchOrganizations($organization_id)->toArray();
        }

        $organization_calculate_id = isset($_GET['organization_calculate_id']) ? $_GET['organization_calculate_id'] : '';
        $info['check_organizations'] = [];
        if ($organization_calculate_id != '') {
            $detail_list = CarbonCulateModel::getCarbonCulateDetailByCal($organization_calculate_id)->toArray();
            $info['check_organizations'] = array_values(array_unique(array_column($detail_list, 'organization_id')));
        }

        foreach ($organizations_list as &$value) {
            $value['has_model'] = FALSE;
            $model_list = CalculateExampleModel::getCalculateExampleByOrg($value['id']);
            if ($model_list != NULL) {
                $value['has_model'] = TRUE;
            }

            $value['check_show'] = in_array($value['id'], $info['check_organizations']) ? TRUE : FALSE;
        }

        if ($organization_id == $main_organization_id) {
            $trees = Organization::getTree($organizations_list, 0, 3);

            foreach ($trees as $value) {
                if ($value['id'] == $main_organization_id) {
                    $info['organizations'] = array($value);
                }
            }
        } else {
            $info['organizations'][] = self::getOrg($organizations_list, $organization_id);
        }

        $organization_all = OrganizationModel::getAllFiledOrganizations($main_organization_id)->toArray();
        $organization = [];
        foreach ($organization_all as $key => $value) {
            $organization[$key]['id'] = $value['id'];
            $calculate_year = CarbonCulateModel::getCompleteCarbonCulate($value['id'])->toArray();
            $organization[$key]['name'] = $value['name'];
            $organization[$key]['contrast_year'] = $calculate_year;
        }
        $info['organization_all'] = $organization;

        $data['code'] = 200;
        $data['data'] = $info;

        return json($data);
    }

    /**
     * editSee 新增核算组织树回显
     *
     * @author wuyinghua
     * @return void
     */
    public function editSee() {
        $data_redis = $this->request->middleware('data_redis');
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
        $calculate_id = isset($_GET['id']) ? $_GET['id'] : '';
        $calculate_data = CarbonCulateModel::getCarbonCulate($calculate_id);
        $info['organization_id'] = $calculate_data['organization_id'];
        $info['organization_name'] = $calculate_data['organization_name'];
        $info['calculate_year'] = $calculate_data['calculate_year'];
        $info['contrast_year'] = $calculate_data['contrast_year'];

        $organization_all = OrganizationModel::getAllFiledOrganizations($main_organization_id)->toArray();
        $organization = [];
        foreach ($organization_all as $key => $value) {
            $organization[$key]['id'] = $value['id'];
            $organization[$key]['name'] = $value['name'];
        }
        $info['organization_all'] = $organization;

        $data['code'] = 200;
        $data['data'] = $info;

        return json($data);
    }

    /**
     * add 核算添加
     * 
     * @author wuyinghua
	 * @return void
     */
    public function add() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');

        $data['main_organization_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
        $data['organization_id'] = $params_payload_arr['organization_id'];
        $data['choice_organization'] = $params_payload_arr['choice_organization'];
        $organization_id_arr = explode(',', $data['organization_id']);

        // 判断是否存在组织没有核算模型
        foreach ($organization_id_arr as $key => $value) {

            $calculate_data = CalculateExampleModel::getCalculateExampleByOrg($value);
            if ($calculate_data == NULL) {
                $organization = OrganizationModel::getOrganization($value);
                return json(['code'=>201, 'message'=>$organization['name'] . '没有核算模型无法进行核算']);
            }
        }

        $data['calculate_year'] = $params_payload_arr['calculate_year'];
        $data['contrast_year'] = $params_payload_arr['contrast_year']; // 非必填，填写需要判断当前组织对比年下是否有核算

        $organization_data = OrganizationModel::getOrganization($data['choice_organization']);
        $data['calculate_name'] = $organization_data['name'] . $data['calculate_year'] . '年碳排放核算';
        $data['userid'] = $data_redis['userid'];

        // 一个组织一年只能有一次核算
        $calculate_data = CarbonCulateModel::getAllCarbonCulateByOrg($data['choice_organization'], $data['calculate_year'])->toArray();
        if ($calculate_data != NULL) {
            return json(['code'=>201, 'message'=>'该组织该年度已存在核算']);
        } else {
            $add = CarbonCulateModel::addCarbonCulate($data);
        }

        if ($add) {
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '添加核算：' . $data['calculate_name'];
            OperationModel::addOperation($data_log);
            return json(['code'=>200, 'message'=>"添加成功"]);
        } else {
            return json(['code'=>404, 'message'=>"添加失败"]);
        }
    }

    /**
     * edit 核算编辑 todo 允许编辑组织范围
     * 
     * @author wuyinghua
	 * @return void
     */
    public function edit() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');
        $data['id'] = $params_payload_arr['id'];
        $data['calculate_year'] = $params_payload_arr['calculate_year'];
        $data['contrast_year'] = $params_payload_arr['contrast_year']; // 非必填，填写需要判断当前组织对比年下是否有核算
        $data['main_organization_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
        $data['organization_id'] = $params_payload_arr['organization_id'];
        $data['choice_organization'] = $params_payload_arr['choice_organization'];
        $data['userid'] = $data_redis['userid'];

        $calculate_data = CarbonCulateModel::getCarbonCulate($data['id']);
        $data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
        $data['modify_time'] = date('Y-m-d H:i:s');
        $calculate_data = CarbonCulateModel::getCarbonCulate($data['id']);
        $edit = CarbonCulateModel::editCarbonCulate($data);
        if ($edit) {
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '编辑核算：' . $calculate_data['calculate_name'];
            OperationModel::addOperation($data_log);

            return json(['code'=>200, 'message'=>"编辑成功"]);
        } else {
            return json(['code'=>201, 'message'=>"编辑失败"]);
        }
    }

    /**
     * collect 发起收集任务
     *
     * @author wuyinghua
     * @return void
     */
    public function collect() {
        if (request()->isPost()) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $this->request->middleware('data_redis');
            $data['userid'] = $data_redis['userid'];
            $data['state'] = ParamModel::ORGDETAIL_STATE_SUBMIT;
            $data['organization_calculate_id'] = isset($params_payload_arr['organization_calculate_id']) ? $params_payload_arr['organization_calculate_id'] : '';
            $data['calculate_month'] = isset($params_payload_arr['calculate_month']) ? $params_payload_arr['calculate_month'] : '';
            $calculate_data = CarbonCulateModel::getCarbonCulate($data['organization_calculate_id']);
            if (isset($params_payload_arr['id'])) {
                $data['id'] = $params_payload_arr['id'];
                $data_log['log'] = '对' . $calculate_data['calculate_name'] . $calculate_data['organization_name'] . $data['calculate_month'] . '月的数据发起收集';
                $result = CarbonCulateModel::collect($data);
            } else {
                $data_log['log'] = '对' . $calculate_data['calculate_name'] . '的所有数据全部发起收集';
                $result = CarbonCulateModel::collect($data, true);
            }

            if ($result) {
                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '组织碳核算';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['url'] = $this->request->pathinfo();
                OperationModel::addOperation($data_log);
                return json(['code'=>200, 'message'=>"发起成功"]);
            } else {
                return json(['code'=>404, 'message'=>"发起失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>"操作失败"]);
        }
    }

    /**
     * getOrg 获取组织
     * 
     * @author wuyinghua
     * @param $data
     * @param $id
	 * @return $org
     */
    public static function getOrg($data, $id) {
        $org = [];
        foreach($data as $k => $v) {
            if($v['pid'] == $id) {
                $org['children'][] = $v;
            } else {
                $v['children'] = [];
                $org = $v;
            }
        }

        return $org;
    }

    /**
     * getOrgTree 获取组织父子集
     * 
     * @author wuyinghua
     * @param $data
     * @param $pid
	 * @return $data
     */
    public static function getOrgTree($data, $pid) {
        $tree = [];
        foreach($data as $k => $v) {
            if($v['pid'] == $pid) {
                $v['children'] = self::getOrgTree($data, $v['organization_id']);
                $tree[] = $v;
                unset($data[$k]);
            }
        }

        return $tree;
    }

}
