<?php
declare (strict_types = 1);

namespace app\controller\organization;

use app\BaseController;
use app\model\organization\FacilityModel;
use app\model\system\OrganizationModel;
use app\validate\FacilityValidate;
use app\model\system\OperationModel;
use think\exception\ValidateException;
use app\model\data\ParamModel;

class Facility extends BaseController
{
    /**
     * 设备管理列表
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function index() {
        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : ParamModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : ParamModel::pageIndex;
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';

        // 过滤条件：设备名称、设备编号
        $filters = [
            'facility_name' => isset($_GET['facility_name']) ? $_GET['facility_name'] : '',
            'facility_no' => isset($_GET['facility_no']) ? $_GET['facility_no'] : '',
            'main_organization_id' => $main_organization_id
        ];

        $list = FacilityModel::getFacilities($page_size, $page_index, $filters)->toArray();
        $organization_all = OrganizationModel::getAllFiledOrganizations($main_organization_id)->toArray();
        $organization = [];
        foreach ($organization_all as $key => $value) {
            $organization[$key]['id'] = $value['id'];
            $organization[$key]['name'] = $value['name'];
        }

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['total'] = $list['total'];
        $data['data']['organization'] = $organization;

        return json($data);
    }

    /**
     * add 设备添加
     * 
     * @author wuyinghua
	 * @return void
     */
    public function add() {

        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $this->request->middleware('data_redis');

            $data['main_organization_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
            $data['facility_name'] = $params_payload_arr['facility_name'];
            $data['facility_no'] = $params_payload_arr['facility_no'];
            $data['organization_id'] = $params_payload_arr['organization_id'];
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 设备编码唯一
            $facility_data = FacilityModel::getFacilityByNo($data['facility_no']);
            if ($facility_data != NULL) {
                return json(['code'=>201, 'message'=>"设备编码已存在"]);
            }

            $add = FacilityModel::addFacility($data);
            if ($add) {
                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '组织碳核算';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['url'] = $this->request->pathinfo();
                $data_log['log'] = '添加设备：' . $data['facility_name'];
                OperationModel::addOperation($data_log);

                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>404, 'message'=>"添加失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * edit 设备编辑
     * 
     * @author wuyinghua
	 * @return void
     */
    public function edit() {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $this->request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $data['facility_name'] = $params_payload_arr['facility_name'];
            $data['facility_no'] = $params_payload_arr['facility_no'];
            $data['organization_id'] = $params_payload_arr['organization_id'];
            $data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['modify_time'] = date('Y-m-d H:i:s');

            $facility_data = FacilityModel::getFacilityByNo($data['facility_no']);
            if ($facility_data != NULL) {
                return json(['code'=>201, 'message'=>"设备编码已存在"]);
            }

            $edit = FacilityModel::editFacility($data);
            if ($edit) {
                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '组织碳核算';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['url'] = $this->request->pathinfo();
                $data_log['log'] = '编辑设备：' . $data['facility_name'];
                OperationModel::addOperation($data_log);

                return json(['code'=>200, 'message'=>"编辑成功"]);
            } else {
                return json(['code'=>201, 'message'=>"编辑失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * see 设备查看
     * 
     * @author wuyinghua
     * @return void
     */
    public function see() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $info = FacilityModel::getFacility($id);
        if ($info == null) {
            return json(['code'=>201, 'message'=>"未查询到设备相关信息"]);
        }

        $data['code'] = 200;
        $data['data'] = $info;

        return json($data);
    }

    /**
     * del 删除设备
     * 
     * @author wuyinghua
	 * @return void
     */
    public function del() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');
        $data['id'] = $params_payload_arr['id'];
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $facility_data = FacilityModel::getFacility($data['id']);
        $del = FacilityModel::delFacility($data['id']);
        if ($del) {
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '删除设备：' . $facility_data['facility_name'];
            OperationModel::addOperation($data_log);

            return json(['code'=>200, 'message'=>"删除成功"]);
        } else {
            return json(['code'=>201, 'message'=>"删除失败"]);
        }
    }

    /**
     * validateForm 验证
     * 
     * @author wuyinghua
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(FacilityValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}
