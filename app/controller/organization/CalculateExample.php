<?php
declare (strict_types = 1);

namespace app\controller\organization;

use app\BaseController;
use app\model\organization\CalculateExampleModel;
use app\model\system\OrganizationModel;
use app\validate\CalculateExampleValidate;
use app\model\organization\FacilityModel;
use app\model\system\OperationModel;
use think\exception\ValidateException;
use app\model\data\ParamModel;

/**
 * CalculateExample
 */
class CalculateExample extends BaseController
{
    /**
     * 核算模型管理列表
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function index() {
        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : ParamModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : ParamModel::pageIndex;
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';

        // 过滤条件：组织名称、模型名称
        $filters = [
            'organization_id' => isset($_GET['organization_id']) ? $_GET['organization_id'] : '',
            'model_name' => isset($_GET['model_name']) ? $_GET['model_name'] : '',
            'main_organization_id' => $main_organization_id
        ];

        $list = CalculateExampleModel::getCalculateExamples($page_size, $page_index, $filters)->toArray();
        $organization_all = OrganizationModel::getAllFiledOrganizations($main_organization_id)->toArray();
        $gwp_all = CalculateExampleModel::getAllGwps()->toArray();
        $organization = [];
        foreach ($organization_all as $key => $value) {
            $organization[$key]['id'] = $value['id'];
            $organization[$key]['name'] = $value['name'];
        }

        $gwp = [];
        foreach ($gwp_all as $key => $value) {
            $gwp[$key]['gwp_id'] = $value['id'];
            $gwp[$key]['gwp_name'] = $value['name'];
        }

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['total'] = $list['total'];
        $data['data']['organization_all'] = $organization;
        array_unshift($organization, ['id'=>0,'name'=>'全部']);
        $data['data']['organization_map'] = $organization;
        $data['data']['gwp_all'] = $gwp;

        return json($data);
    }

    /**
     * add 核算模型添加
     * 
     * @author wuyinghua
	 * @return void
     */
    public function add() {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $this->request->middleware('data_redis');

            $data['main_organization_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
            $data['model_name'] = $params_payload_arr['model_name'];
            $data['gwp_id'] = $params_payload_arr['gwp_id'];
            $data['organization_id'] = $params_payload_arr['organization_id'];
            $data['description'] = $params_payload_arr['description'];
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 一个组织只能有一个核算模型
            $model_list = CalculateExampleModel::getCalculateExampleByOrg($data['organization_id']);
            if ($model_list != NULL) {
                return json(['code'=>201, 'message'=>"该组织已存在核算模型"]);
            } else {
                $add = CalculateExampleModel::addCalculateExample($data);
            }

            if ($add) {
                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '组织碳核算';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['url'] = $this->request->pathinfo();
                $data_log['log'] = '添加核算模型：' . $data['model_name'];
                OperationModel::addOperation($data_log);
                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>404, 'message'=>"添加失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * edit 核算模型编辑
     * 
     * @author wuyinghua
	 * @return void
     */
    public function edit() {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $this->request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $data['model_name'] = $params_payload_arr['model_name'];
            $data['gwp_id'] = $params_payload_arr['gwp_id'];
            $data['organization_id'] = $params_payload_arr['organization_id'];
            $data['description'] = $params_payload_arr['description'];
            $data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['modify_time'] = date('Y-m-d H:i:s');
            $edit = CalculateExampleModel::editCalculateExample($data);
            if ($edit) {
                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '组织碳核算';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['url'] = $this->request->pathinfo();
                $data_log['log'] = '编辑核算模型：' . $data['model_name'];
                OperationModel::addOperation($data_log);

                return json(['code'=>200, 'message'=>"编辑成功"]);
            } else {
                return json(['code'=>201, 'message'=>"编辑失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * see 核算模型查看
     * 
     * @author wuyinghua
     * @return void
     */
    public function see() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $info = CalculateExampleModel::getCalculateExample($id);
        if ($info == null) {
            return json(['code'=>201, 'message'=>"未查询到设备相关信息"]);
        }

        $data['code'] = 200;
        $data['data'] = $info;

        return json($data);
    }

    /**
     * del 删除核算模型
     * 
     * @author wuyinghua
	 * @return void
     */
    public function del() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');
        $data['id'] = $params_payload_arr['id'];
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $calculate_data = CalculateExampleModel::getCalculateExample($data['id']);
        $del = CalculateExampleModel::delCalculateExample($data['id']);
        if ($del) {
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '删除核算模型：' . $calculate_data['model_name'];
            OperationModel::addOperation($data_log);

            return json(['code'=>200, 'message'=>"删除成功"]);
        } else {
            return json(['code'=>201, 'message'=>"删除失败"]);
        }
    }

    /**
     * copy 复制核算模型
     * 
     * @author wuyinghua
	 * @return void
     */
    public function copy() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');
        $data['id'] = $params_payload_arr['id'];
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $data['model_name'] = $params_payload_arr['model_name'];
        $data['gwp_id'] = $params_payload_arr['gwp_id'];
        $data['organization_id'] = $params_payload_arr['organization_id'];
        $data['description'] = $params_payload_arr['description'];
        $calculate_data = CalculateExampleModel::getCalculateExample($data['id']);
        // 一个组织只能有一个核算模型
        $model_list = CalculateExampleModel::getCalculateExampleByOrg($data['organization_id']);
        if ($model_list != NULL) {
            return json(['code'=>201, 'message'=>"该组织已存在核算模型"]);
        } else {
            $copy = CalculateExampleModel::copyCalculateExample($data, $data_redis['userid']);
        }
        
        if ($copy) {
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '复制核算模型：' . $calculate_data['model_name'];
            OperationModel::addOperation($data_log);

            return json(['code'=>200, 'message'=>"复制成功"]);
        } else {
            return json(['code'=>201, 'message'=>"复制失败"]);
        }
    }

    /**
     * 排放源管理列表
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function emissionList() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : ParamModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : ParamModel::pageIndex;
        $id = isset($_GET['id']) ? $_GET['id'] : '';

        // 过滤条件：核算模型ID、...
        $filters = ['calculate_model_id' => $id];
        $list = CalculateExampleModel::getEmissions($page_size, $page_index, $filters)->toArray();
        $model_data = CalculateExampleModel::getCalculateExample($id);
        $model_name = $model_data['model_name'];
        $organization_name = $model_data['organization_name'];

        // 获取当前核算模型关联的设备
        foreach ($list['data'] as $key => $value) {
            $facility_list = CalculateExampleModel::getFacilityByEmissionId($value['id'])->toArray();
            $list['data'][$key]['count'] = count($facility_list);
        }

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['model_name'] = $model_name;
        $data['data']['calculate_model_id'] = $id;
        $data['data']['organization_name'] = $organization_name;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * emissionChoice 选择排放源
     * 
     * @author wuyinghua
	 * @return void
     */
    public function emissionChoice() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');
        $data['userid'] = $data_redis['userid'];
        $data['ids'] = explode(',', $params_payload_arr['ids']); // 可以多选

        if ($data['ids'] == '' || $data['ids'] == null || $data['ids'] == 0) {
            return json(['code'=>201, 'message'=>"参数ids错误"]);
        }

        $data['calculate_model_id'] = $params_payload_arr['calculate_model_id'];
        if ($data['calculate_model_id'] == '' || $data['calculate_model_id'] == null || $data['calculate_model_id'] == 0) {
            return json(['code'=>201, 'message'=>"参数calculate_model_id错误"]);
        }

        // 核算模型选择的排放源名称
        $emission_name = '';
        foreach ($data['ids'] as $key => $id) {
            $emission_data = CalculateExampleModel::getEmissionSource($id);
            if ($emission_data == null) {
                return json(['code'=>201, 'message'=>"参数id错误"]);
            }

            // 无法重复选择同一个排放源，根据排放源ID唯一
            $emission_source_data = CalculateExampleModel::getEmissionBySource($emission_data['id'], $data['calculate_model_id']);
            if ($emission_source_data != NULL) {
                return json(['code'=>201, 'message'=>"排放源已存在，无法重复选择"]);
            }

            if ($key > 0) {
                $emission_name = $emission_name . ',' . $emission_data['name'];
            } else {
                $emission_name = $emission_data['name'];
            }
        }

        $choice = CalculateExampleModel::emissionChoice($data);
        if ($choice) {
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '核算模型选择排放源：' . $emission_name;
            OperationModel::addOperation($data_log);

            return json(['code'=>200, 'message'=>"选择排放源成功"]);
        } else {
            return json(['code'=>201, 'message'=>"选择排放源失败"]);
        }
    }

    /**
     * emissionSee 排放源查看
     * 
     * @author wuyinghua
     * @return void
     */
    public function emissionSee() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $info = CalculateExampleModel::getEmissionModelSource($id);
        $emission_data = CalculateExampleModel::getEmissionSource($info['emission_id']);
        if ($emission_data == null) {
            return json(['code'=>201, 'message'=>"未查询到因子相关信息"]);
        }

        $list = CalculateExampleModel::getInfoById($info['emission_id']);
        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }

    /**
     * emissionDel 排放源删除
     * 
     * @author wuyinghua
	 * @return void
     */
    public function emissionDel() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');
        $data['id'] = $params_payload_arr['id'];
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $info = CalculateExampleModel::getEmissionModelSource($data['id']);
        $emission_data = CalculateExampleModel::getEmissionSource($info['emission_id']);
        $del = CalculateExampleModel::delEmissionSource($data['id']);
        if ($del) {
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '删除核算模型-排放源：' . $emission_data['name'];
            OperationModel::addOperation($data_log);

            return json(['code'=>200, 'message'=>"删除成功"]);
        } else {
            return json(['code'=>201, 'message'=>"删除失败"]);
        }
    }

    /**
     * 排放源关联设备列表
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function facilityList() {
        $data_redis = $this->request->middleware('data_redis');
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $info = CalculateExampleModel::getEmissionModelSource($id);
        $emission_data = CalculateExampleModel::getEmissionSource($info['emission_id']);
        $model_data = CalculateExampleModel::getCalculateExample($info['calculate_model_id']);
        $model_name = $model_data['model_name'];
        $organization_name = $model_data['organization_name'];
        $emisson_name = $emission_data['name'];

        // 过滤条件：核算模型关联排放源ID、...
        $filters = ['calculate_model_emission_id' => $id];
        $list = CalculateExampleModel::getFacilities($filters)->toArray();
        $list_new = [];
        foreach ($list as $key => $value) {
            array_push($list_new, $value['id']);
        }
        $facilities = FacilityModel::getAllFacilities($main_organization_id)->toArray();

        $data['code'] = 200;
        $data['data']['list_new'] = $list_new;
        $data['data']['list'] = $list;
        $data['data']['model_name'] = $model_name;
        $data['data']['organization_name'] = $organization_name;
        $data['data']['calculate_model_emission_id'] = $id;
        $data['data']['emisson_name'] = $emisson_name;
        $data['data']['facilities'] = $facilities;

        return json($data);
    }

    /**
     * facilityAdd 关联设备添加
     * 
     * @author wuyinghua
	 * @return void
     */
    public function facilityAdd() {

        if (request()->isPost()) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $this->request->middleware('data_redis');
            $data['calculate_model_emission_id'] = $params_payload_arr['calculate_model_emission_id'];
            if ($data['calculate_model_emission_id'] == '' || $data['calculate_model_emission_id'] == null || $data['calculate_model_emission_id'] == 0) {
                return json(['code'=>201, 'message'=>"参数calculate_model_emission_id错误"]);
            }

            $data['ids'] = $params_payload_arr['ids']; // 设备编码
            if ($data['ids'] == '' || $data['ids'] == null || $data['ids'] == 0) {
                return json(['code'=>201, 'message'=>"参数ids错误"]);
            }

            // 排放源不可重复关联同一个设备
            if (count($data['ids']) != count(array_unique($data['ids']))) {
                return json(['code'=>201, 'message'=>"不可重复关联同一个设备"]);
            }

            $data['userid'] = $data_redis['userid'];
            $add = CalculateExampleModel::addEmissionSourceFacility($data);
            if ($add) {
                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '组织碳核算';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['url'] = $this->request->pathinfo();
                $data_log['log'] = '添加核算模型-排放源关联的设备';
                OperationModel::addOperation($data_log);
                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>404, 'message'=>"添加失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * facilityDel 删除关联设备
     * 
     * @author wuyinghua
	 * @return void
     */
    public function facilityDel() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');
        $data['id'] = $params_payload_arr['id'];
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $del = CalculateExampleModel::delEmissionSourceFacility($data['id']);
        if ($del) {
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '删除核算模型-排放源关联设备';
            OperationModel::addOperation($data_log);

            return json(['code'=>200, 'message'=>"删除成功"]);
        } else {
            return json(['code'=>201, 'message'=>"删除失败"]);
        }
    }

    /**
     * validateForm 验证
     * 
     * @author wuyinghua
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(CalculateExampleValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}
