<?php
declare (strict_types = 1);

namespace app\controller\organization;

use app\BaseController;
use app\model\organization\CalculateReportModel;
use app\model\system\OrganizationModel;
use app\model\system\OperationModel;
use app\model\admin\UnitModel;
use app\model\quality\QualityModel;
use app\model\datum\DatumModel;
use app\model\organization\CarbonCulateModel;
use app\model\system\FileModel;
use app\validate\OrgCalculateReportValidate;
use think\exception\ValidateException;
use app\model\data\ParamModel;
use think\facade\Config;
use think\facade\Db;

class CalculateReport extends BaseController
{
    /**
     * 核算报告列表
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function index() {

        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : ParamModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : ParamModel::pageIndex;
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';

        // 过滤条件：组织、报告名称、核算年
        $filters = [
            'organization_id' => isset($_GET['organization_id']) ? $_GET['organization_id'] : '',
            'report_name' => isset($_GET['report_name']) ? $_GET['report_name'] : '',
            'calculate_year' => isset($_GET['calculate_year']) ? $_GET['calculate_year'] : '',
            'main_organization_id' => $main_organization_id
        ];

        $list = CalculateReportModel::getCalculateReports($page_size, $page_index, $filters)->toArray();
        $organization_all = OrganizationModel::getAllFiledOrganizations($main_organization_id)->toArray();
        $organization = [];
        foreach ($organization_all as $key => $value) {
            $organization[$key]['id'] = $value['id'];
            $organization[$key]['name'] = $value['name'];
        }

        $organization_new = [];
        foreach ($organization as $key => $value) {
            $organization_new[$key] = $value;
            $organization_new[$key]['calculate_all'] = CalculateReportModel::getAllCalculate($value['id']);
            $organization_new[$key]['data_quality_all'] = CalculateReportModel::getAllDataQuality($value['id']);

            $datum_data = CalculateReportModel::getAllDatum($value['id']);
            foreach ($datum_data as $k => $v) {
                $organization_new[$key]['year_id'] =  $v['id'];
                if ($v['end_year'] == '' || $v['end_year'] == null || $v['end_year'] == 0) {
                    $organization_new[$key]['year'] =  $v['start_year'];
                } else {
                    $organization_new[$key]['year'] =  $v['start_year'] . '-' . $v['end_year'];
                }
            }
        }

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['total'] = $list['total'];
        $data['data']['organization_all'] = $organization_new;
        array_unshift($organization, ['id'=>0,'name'=>'全部']);
        $data['data']['organization_map'] = $organization;

        return json($data);
    }

    /**
     * add 核算报告添加
     * 
     * @author wuyinghua
	 * @return void
     */
    public function add() {
        // 1、所属组织枚举值为当前用户所属组织。
        // 2、核算名称为碳核算管理列表数据，根据所属组织展示数据范围。
        // 3、核算年份根据所选择核算名称自动带出。
        // 4、数据质量控制计划版本枚举值为控制计划管理中数据，根据所属组织展示数据范围。
        // 5、点击选择，弹出选择减排场景弹窗，确定后在列表中展示。
        // 6、基准线：根据所属组织选项自动选择选择基准年，展示内容为基准年份，如：2019年或2019-2023年；如果未配置基准，展示：未配置基准。

        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $this->request->middleware('data_redis');

            $data['virtual_id'] = empty($params_payload_arr['virtual_id']) ? '' : $params_payload_arr['virtual_id']; // 虚拟id
            $data['main_organization_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : ''; // 母公司id
            $data['report_name'] = $params_payload_arr['report_name']; //报告名称
            $data['organization_id'] = $params_payload_arr['organization_id']; // 组织id
            $data['organization_calculate_id'] = $params_payload_arr['organization_calculate_id']; // 组织核算id
            $data['data_quality_id'] = $params_payload_arr['data_quality_id']; // 数据质量控制计划版本id
            $data['datum_id'] = $params_payload_arr['datum_id']; // 基准id
            $data['username'] = $params_payload_arr['username']; // 报告负责人姓名
            $data['telephone'] = $params_payload_arr['telephone']; // 报告负责人电话
            $data['email'] = $params_payload_arr['email']; // 报告负责人邮件
            $data['exempte_description'] = $params_payload_arr['exempte_description']; // 排放源免除说明
            $data['change_description'] = $params_payload_arr['change_description']; // 量化方法变更说明
            $data['discharge_description'] = $params_payload_arr['discharge_description']; // 生物质相关排放说明
            $data['other_description'] = $params_payload_arr['other_description']; // 其他情况说明
            $data['userid'] = $data_redis['userid'];

            $add = CalculateReportModel::addCalculateReport($data);
            if ($add) {
                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '组织碳核算';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['url'] = $this->request->pathinfo();
                $data_log['log'] = '新增报告：' . $data['report_name'];
                OperationModel::addOperation($data_log);

                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>404, 'message'=>"添加失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * edit 核算报告编辑
     * 
     * @author wuyinghua
	 * @return void
     */
    public function edit() {
        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $this->request->middleware('data_redis');
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data['id'] = $params_payload_arr['id'];
            $report_data = CalculateReportModel::getCalculateReport($data['id']);
            if ($report_data == null) {
                return json(['code'=>201, 'message'=>"未查询到核算报告相关信息"]);
            }

            $data['main_organization_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : ''; // 母公司id
            $data['report_name'] = $params_payload_arr['report_name']; //报告名称
            $data['organization_id'] = $params_payload_arr['organization_id']; // 组织id
            $data['datum_id'] = $params_payload_arr['datum_id']; // 基准id
            $data['organization_calculate_id'] = $params_payload_arr['organization_calculate_id']; // 组织核算id
            $data['data_quality_id'] = $params_payload_arr['data_quality_id']; // 数据质量控制计划版本id
            $data['username'] = $params_payload_arr['username']; // 报告负责人姓名
            $data['telephone'] = $params_payload_arr['telephone']; // 报告负责人电话
            $data['email'] = $params_payload_arr['email']; // 报告负责人邮件
            $data['exempte_description'] = $params_payload_arr['exempte_description']; // 排放源免除说明
            $data['change_description'] = $params_payload_arr['change_description']; // 量化方法变更说明
            $data['discharge_description'] = $params_payload_arr['discharge_description']; // 生物质相关排放说明
            $data['other_description'] = $params_payload_arr['other_description']; // 其他情况说明
            $data['userid'] = $data_redis['userid'];

            $edit = CalculateReportModel::editCalculateReport($data);
            if ($edit) {
                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '组织碳核算';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['url'] = $this->request->pathinfo();
                $data_log['log'] = '编辑核算报告：' . $data['report_name'];
                OperationModel::addOperation($data_log);

                return json(['code'=>200, 'message'=>"编辑成功"]);
            } else {
                return json(['code'=>201, 'message'=>"编辑失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * see 核算报告查看
     * 
     * @author wuyinghua
     * @return void
     */
    public function see() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $info = CalculateReportModel::getCalculateReport($id);
        if ($info == null) {
            return json(['code'=>201, 'message'=>"未查询到核算报告相关信息"]);
        }

        // 核算名称回显
        $organization_calculate = CarbonCulateModel::getCarbonCulate($info['organization_calculate_id']);
        $info['organization_calculate']['id'] = $info['organization_calculate_id'];
        $info['organization_calculate']['name'] = $organization_calculate['calculate_name'];

        // 数据质量控制计划版本回显
        $data_quality = QualityModel::getInfoById($info['data_quality_id']);
        if ($data_quality != NULL) {
            $info['data_quality']['id'] = $info['data_quality_id'];
            $info['data_quality']['name'] = $data_quality['version'];
        } else {
            $info['data_quality']['name'] = '';
        }

        // 排放基准回显
        $datum = DatumModel::getInfoById($info['datum_id']);
        $info['datum']['id'] = $info['datum_id'];
        if ($datum['end_year'] == '' || $datum['end_year'] == null || $datum['end_year'] == 0) {
            $info['datum']['name'] =  $datum['start_year'];
        } else {
            $info['datum']['name'] =  $datum['start_year'] . '-' . $datum['end_year'];
        }

        // 减排场景
        $reduction_data = CalculateReportModel::getReduction($id)->toArray();
        foreach ($reduction_data as $key => $value) {
            $info['reduction'][$key]['id'] = $value['id'];
            $info['reduction'][$key]['organization_name'] = $value['organization_name'];
            $info['reduction'][$key]['name'] = $value['name'];
            $info['reduction'][$key]['desc'] = $value['desc'];
        }
        $data['code'] = 200;
        $data['data'] = $info;

        return json($data);
    }

    /**
     * del 删除核算报告
     * 
     * @author wuyinghua
	 * @return void
     */
    public function del() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');
        $data['id'] = $params_payload_arr['id'];
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }
        $report_data = CalculateReportModel::getCalculateReport($data['id']);
        $del = CalculateReportModel::delCalculateReport($data['id']);
        if ($del) {
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '删除核算报告：' . $report_data['report_name'];
            OperationModel::addOperation($data_log);

            return json(['code'=>200, 'message'=>"删除成功"]);
        } else {
            return json(['code'=>201, 'message'=>"删除失败"]);
        }
    }

    /**
     * delReduction 删除减排场景
     * 
     * @author wuyinghua
	 * @return void
     */
    public function delReduction() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');
        $data['id'] = $params_payload_arr['id'];
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $reduction_data = CalculateReportModel::getReductionById($data['id']);
        $del = CalculateReportModel::delReduction($data['id']);
        if ($del) {
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '删除核算报告减排场景' . $reduction_data['name'];
            OperationModel::addOperation($data_log);

            return json(['code'=>200, 'message'=>"删除成功"]);
        } else {
            return json(['code'=>201, 'message'=>"删除失败"]);
        }
    }

    /**
     * 减排场景列表
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function reductionList() {
        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : ParamModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : ParamModel::pageIndex;
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
        $organization_id = isset($_GET['organization_id']) ? $_GET['organization_id'] : '';

        if (isset($_GET['virtual_id'])) {
            if (isset($_GET['report_id'])) {
                $reduction_data = CalculateReportModel::getReduction($_GET['report_id'])->toArray();
            } else {
                $reduction_data = CalculateReportModel::getReductionByVirtual($_GET['virtual_id'], $organization_id)->toArray();
            }

            if ($reduction_data != null) {
                foreach ($reduction_data as $key => &$value) {

                    $value['id'] = $value['id'];
                    $value['organization_name'] = $value['organization_name'];
                    $value['name'] = $value['name'];
                    $value['desc'] = $value['desc'];
        
                    // 类型为总减排量
                    if (strrpos($value['type'], '1') !== FALSE) {
                        $total_unit_data = UnitModel::findUnit($value['total_unit']);
                        $value['total_unit'] = $total_unit_data['name'];
                    }

                    // 类型为单位减排量
                    if (strrpos($value['type'], '2') !== FALSE) {
                        $value['molecule'] = Config::get('emission.molecule')[$value['molecule']-1]['name'];
                        $denominator = explode(',', $value['denominator']);
                        $denominator_data = UnitModel::findUnit($denominator[1]);
                        $value['denominator'] = $denominator_data['name'];
                    }

                    // 选择减排场景 总减排量和单位减排量
                    /**
                       *unit_low为0 type=1 总减排量
                       *    total_high为0 确定值  XX --
                       *    不为0         范围值 XX-XX --
                       *
                       *total_low 为0 type =2 单位减排量 确定值
                       *    unit_high为0 确定值 -- XX
                       *    不为0        范围值 -- XX-XX
                       *
                       *都不为0 的情况 type = 1,2
                       *    total_hig为0 && unit_hig为0 XX XX
                       *    total_hig为0 XX XX-XX
                       *    unit_hig为0 XX-XX XX
                     */
                    if ($value['type'] == '1') {
                        if ($value['total_reduction_high'] == '' || $value['total_reduction_high'] == null || $value['total_reduction_high'] == 0) {
                            $value['total_description'] = '总减排量描述内容：' . $value['total_reduction_low'] . $value['total_unit'];
                        } else {
                            $value['total_description'] = '总减排量描述内容：' . $value['total_reduction_low'] . '-' . $value['total_reduction_high'] . $value['total_unit'];
                        }
                        $value['unit_description'] = '--';
                    } elseif ($value['type'] == '2') {
                        if ($value['unit_high'] == '' || $value['unit_high'] == null || $value['unit_high'] == 0) {
                            $value['unit_description'] = '单位减排量描述内容：' . $value['unit_low'] . $value['molecule'] . '/' . $value['denominator'];
                        } else {
                            $value['unit_description'] = '单位减排量描述内容：' . $value['unit_low'] . '-' . $value['unit_high'] . $value['molecule'] . '/' . $value['denominator'];
                        }
                        $value['total_description'] = '--';
                    } else {
                        if (($value['total_reduction_high'] == '' || $value['total_reduction_high'] == null || $value['total_reduction_high'] == 0) && ($value['unit_high'] == '' || $value['unit_high'] == null || $value['unit_high'] == 0)) {
                            $value['total_description'] = '总减排量描述内容：' . $value['total_reduction_low'] . $value['total_unit'];
                            $value['unit_description'] = '单位减排量描述内容：' . $value['unit_low'] . $value['molecule'] . '/' . $value['denominator'];
                        } elseif ($value['total_reduction_high'] == '' || $value['total_reduction_high'] == null || $value['total_reduction_high'] == 0) {
                            $value['total_description'] = '总减排量描述内容：' . $value['total_reduction_low'] . $value['total_unit'];
                            $value['unit_description'] = '单位减排量描述内容：' . $value['unit_low'] . '-' . $value['unit_high'] . $value['molecule'] . '/' . $value['denominator'];
                        } elseif ($value['unit_high'] == '' || $value['unit_high'] == null || $value['unit_high'] == 0) {
                            $value['total_description'] = '总减排量描述内容：' . $value['total_reduction_low'] . '-' . $value['total_reduction_high'] . $value['total_unit'];
                            $value['unit_description'] = '单位减排量描述内容：' . $value['unit_low'] . $value['molecule'] . '/' . $value['denominator'];
                        } else {
                            $value['total_description'] = '总减排量描述内容：' . $value['total_reduction_low'] . '-' . $value['total_reduction_high'] . $value['total_unit'];
                            $value['unit_description'] = '单位减排量描述内容：' . $value['unit_low'] . '-' . $value['unit_high'] . $value['molecule'] . '/' . $value['denominator'];
                        }
                    }
                }
            }

            $data['code'] = 200;
            $data['data'] = $reduction_data;
        } else {
            // 过滤条件：组织、减排场景名称
            $filters = [
                'reduction_name'       => isset($_GET['reduction_name']) ? $_GET['reduction_name'] : '',
                'main_organization_id' => $main_organization_id,
                'organization_id'      => $organization_id
            ];

            $list = CalculateReportModel::getReductions($page_size, $page_index, $filters)->toArray();

            $data['code'] = 200;
            $data['data']['list'] = $list['data'];
            $data['data']['total'] = $list['total'];
        }

        return json($data);
    }

    /**
     * choice 选择减排场景
     * 
     * @author wuyinghua
	 * @return void
     */
    public function choice() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');
        $data['userid'] = $data_redis['userid'];
        $data['virtual_id'] = $params_payload_arr['virtual_id'];
        $data['ids'] = explode(',', $params_payload_arr['ids']); // 可以多选

        if ($data['ids'] == '' || $data['ids'] == null || $data['ids'] == 0) {
            return json(['code'=>201, 'message'=>"参数ids错误"]);
        }

        $data['report_id'] = isset($params_payload_arr['report_id']) ? $params_payload_arr['report_id'] : NULL;
        foreach ($data['ids'] as $id) {
            $report_reduction = CalculateReportModel::getReductionByReportId($id, $data['report_id'], $data['virtual_id']);
            if ($report_reduction != NULL) {
                return json(['code'=>201, 'message'=>"减排场景无法重复选择"]);
            }
        }

        $data['virtual_id'] = empty($params_payload_arr['virtual_id']) ? '' : $params_payload_arr['virtual_id']; // 虚拟id
        $choice = CalculateReportModel::reductionChoice($data);
        if ($choice) {
            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '组织碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '核算报告选择减排场景';
            OperationModel::addOperation($data_log);

            return json(['code'=>200, 'message'=>"选择减排场景成功"]);
        } else {
            return json(['code'=>201, 'message'=>"选择减排场景失败"]);
        }
    }

    /**
     * ghgWord 生成核算报告GHG-WORD
     * 
     * @author wuyinghua
     * @param $id
	 * @return $data
     */
    public function ghgWord() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');
        $data['userid'] = $data_redis['userid'];
        $data['main_organization_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : ''; // 母公司id
        $data['id'] = $params_payload_arr['id'];
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $data['organization_id'] = $params_payload_arr['organization_id'];
        if ($data['organization_id'] == '' || $data['organization_id'] == null || $data['organization_id'] == 0) {
            return json(['code'=>201, 'message'=>"参数organization_id错误"]);
        }

        $calculate_report_data = $calculate_report_data = CalculateReportModel::getCalculateReport($data['id']);
        $source_name = $calculate_report_data['organization_name'] . $calculate_report_data['calculate_year'] . '企业温室气体排放报告（GHG版本）' . '.docx';

        $data_file['module'] = '组织碳核算';
        $data_file['main_organization_id'] = $data['main_organization_id'];
        $data_file['organization_id'] = $data['organization_id'];
        $data_file['source_name'] = $source_name;
        $data_file['type'] = 2; // 类型为GHG或ISO文件
        $data_file['create_by'] = $data['userid'];
        $data_file['modify_by'] = $data['userid'];
        $data_file['create_time'] = date('Y-m-d H:i:s');
        $data_file['modify_time'] = date('Y-m-d H:i:s');
        $data_file['id'] = guid();
        $data_file_id = $data_file['id'];
        FileModel::addFile($data_file);

        Db::startTrans();
        try {
            $data_file = $this->createGhgWord($data['id']);

            // 添加操作日志
            $data_log['main_organization_id'] = $data['main_organization_id'];
            $data_log['user_id'] = $data['userid'];
            $data_log['module'] = '企业碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '创建：' . $source_name;

            FileModel::updateFile($data_file, $data_file_id);
            OperationModel::addOperation($data_log);

            Db::commit();
            return json(['code'=>200, 'message'=>"GHG核算报告生成成功"]);
        } catch (\Exception $e) {
            Db::rollback();
            return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
        }
    }

    /**
     * testword 用于测试图表生成
     * 
     * @author wuyinghua
     * @param $id
	 * @return $data
     */
    public function testword() {
        $templatePath = "static/test.docx";
        // 判断目录是否存在
        $filePath= './storage/uploads/'. date('Ymd', time());
        if (!is_dir($filePath)) {
            mkdir($filePath, 0777);
        }
        $filePathNew = $filePath . '/' . time() . '.docx';
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templatePath);

        // 图表类别'pie', 'doughnut', 'line', 'bar', 'stacked_bar', 'percent_stacked_bar', 'column'：柱状图文字在外面, 'stacked_column'：柱状图文字在里面, 'percent_stacked_column', 'area', 'radar', 'scatter'
        // 添加基准线排放量与实际排放量图表数据
        $datum_compare_categories = ['基准线排放量', '实际排放量'];
        $datum_compare_series = [1200, 1500];

        // 图表4-1基准线排放量与实际排放量（单位：tCO2e）
        $style = array(
            'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::THAI_DISTRIBUTE,
            'spaceAfter' => 100,
            'spacing' => 2,
            'spacing' => 2,
            'indentation' => array('left' => 100, 'right' => 100),
            'font' => array('name' => 'Arial', 'size' => 88, 'color' => '000000', 'bold' => true),
        );
        $datum_compare_chart = new \PhpOffice\PhpWord\Element\Chart('stacked_column', $datum_compare_categories, $datum_compare_series,$style);

        $dataLabel=['showCatName'=>false];
        $datum_compare_chart->getStyle()
            // ->setTitle('图表示例')
            ->setShowAxisLabels()
            ->setWidth(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(5))
            ->setHeight(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(3))
            ->setShowGridY()
            ->setColors('FF0000')
            ->setDataLabelOptions($dataLabel);
        $templateProcessor->setChart('datum_compare_chart', $datum_compare_chart);

        // 添加整体排放情况对比图图表数据
        $overall_categories = ['范围一', '范围二', '范围三'];
        $overall_series = [50, 30, 20];

        // 图表4-1整体排放情况对比图
        $style = array(
            'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::THAI_DISTRIBUTE,
            'spaceAfter' => 100,
            'spacing' => 2,
            'indentation' => array('left' => 100, 'right' => 100),
            'font' => array('name' => 'Arial', 'size' => 88, 'color' => '000000', 'bold' => true),
        );
        $overall_chart = new \PhpOffice\PhpWord\Element\Chart('pie', $overall_categories, $overall_series,$style);

        $dataLabel=[
            'showVal' => false, // value
            'showCatName' => false, // category name
            'showLegendKey' => false, //show the cart legend
            'showSerName' => false, // series name
            'showPercent' => true,
            'showLeaderLines' => false,
            'showBubbleSize' => false,
        ];
        $overall_chart->getStyle()
            // ->setTitle('图表示例')
            // ->setShowAxisLabels()
            ->setWidth(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(5))
            ->setHeight(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(3))
            // ->setShowGridY()
            ->setLegendPosition('r')
            ->setShowLegend(true)
            ->setDataLabelOptions($dataLabel);
        $templateProcessor->setChart('overall_chart', $overall_chart);

        // 添加温室气体种类排放对比图图表数据
        $gas_categories = ['CO₂', 'CH₄', 'N₂O', 'HFCs', 'PFCs', 'SF₆', 'NF₃', '混合'];
        $gas_series = [30, 20, 50, 30, 10, 60, 90, 30];

        // 图表4-3温室气体种类排放对比图
        $gas_chart = new \PhpOffice\PhpWord\Element\Chart('pie', $gas_categories, $gas_series);

        $dataLabel=[
            'showVal' => false, // value
            'showCatName' => false, // category name
            'showLegendKey' => false, //show the cart legend
            'showSerName' => false, // series name
            'showPercent' => true,
            'showLeaderLines' => false,
            'showBubbleSize' => false,
        ];
        $gas_chart->getStyle()
            // ->setTitle('图表示例')
            // ->setShowAxisLabels()
            ->setWidth(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(5))
            ->setHeight(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(3))
            // ->setShowGridY()
            ->setLegendPosition('r')
            ->setShowLegend(true)
            ->setDataLabelOptions($dataLabel);
        $templateProcessor->setChart('gas_chart', $gas_chart);

        // 添加温室气体排放对比图图表数据
        $contrast_categories = ['范围一', '范围二', '范围三'];
        $contrast_series = [50, 20, 30]; // 核算年 calculate_year 例：2021

        // 图表4-4温室气体排放对比图
        $contrast_chart = new \PhpOffice\PhpWord\Element\Chart('column', $contrast_categories, $contrast_series,[],2021);
        $contrast_chart->addSeries(['范围一', '范围二', '范围三'], [30, 20, 50], 2022); // contrast_year 对比年 例：2022
        $dataLabel=[
            'showVal' => true, // value
            'showCatName' => false, // category name
            'showLegendKey' => false, //show the cart legend
            'showSerName' => false, // series name
            'showPercent' => false,
            'showLeaderLines' => false,
            'showBubbleSize' => false,
        ];

        $contrast_chart->getStyle()
            // ->setTitle('图表示例')
            ->setShowAxisLabels(true)
            ->setWidth(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(5))
            ->setHeight(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(3))
            ->setShowGridY()
            ->setShowLegend(true)
            ->setLegendPosition('b')
            ->setDataLabelOptions($dataLabel);
        $templateProcessor->setChart('contrast_chart', $contrast_chart);
        $templateProcessor->saveAs($filePathNew);
    }

    /**
     * createGhgWord 生成核算报告GHG-WORD
     * 
     * @author wuyinghua
     * @param $id
	 * @return $data
     */
    public function createGhgWord($id) {
        ini_set('max_execution_time', '999999999');
        // 核算报告数据
        $calculate_report_data = CalculateReportModel::getCalculateReport($id);

        // 碳核算- 排放量计算数据
        $count_gas_data = CalculateReportModel::getCalculateCountGas($calculate_report_data['organization_calculate_id'])->toArray();
        $gas_name_arr = [];
        foreach ($count_gas_data as $gas_key => $gas_value) {
            $gas_name_arr = array_merge_recursive($gas_name_arr, explode(',', $gas_value['gases']));
        }
        $gas_name_arr = array_filter(array_unique($gas_name_arr));
        $gas_name_str = implode(',', $gas_name_arr);
        

        $templatePath = "static/企业温室气体排放报告（GHG版本）.docx";
        // 判断目录是否存在
        $filePath= './storage/uploads/'. date('Ymd', time());
        if (!is_dir($filePath)) {
            mkdir($filePath, 0777);
        }
        $filePathNew = $filePath . '/' . time() . '.docx';
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templatePath);

        // GHG列表
        $ghg_list = CalculateReportModel::ghgList($calculate_report_data['data_quality_id']);
        $ghg_arr = [];
        foreach ($ghg_list as $ghg_key => $ghg_value) {
            foreach ($ghg_value['children'] as $k => $v) {
                $ghg_arr[] = $v;
            }
        }
        // return json($ghg_arr);
        foreach ($ghg_arr as $key => $value) {
            $num = $key + 1;
            $templateProcessor->setValues([
                'active_desc#' . $num     => $value['active_desc'], // 活动描述/排除的说明
                'collection_instructions#' . $num     => $value['collection_instructions'], // 数据收集说明
                'calculation#' . $num     => '', // 数计算方法 todo
                'calculation_description#' . $num    => $value['calculation_description'], // 数计算方法描述
                'data_description#' . $num     => $value['data_description'], // 数据存储说明
            ]);
        }

        // 核算年base data
        $range_data = $this->getGhgRange($calculate_report_data['organization_calculate_id']);//return $range_data;
        $calculate_org_data = CalculateReportModel::getCalculateByContrastYear($calculate_report_data['organization_id'], $calculate_report_data['contrast_year']);

        // 根据组织ID和是否有对比年 判断是否有对比年base data
        if ($calculate_org_data != NULL) {
            $contrast_range_data = $this->getGhgRange($calculate_org_data['id']);
        }

        if ($calculate_report_data['contrast_year'] != NULL && $calculate_org_data != NULL && $contrast_range_data != NULL) {
            $contrast_one_change = abs($range_data['one_total'] - $contrast_range_data['one_total']); // 对比范围一变化量
            $contrast_two_change = abs($range_data['two_total'] - $contrast_range_data['two_total']); // 对比范围二变化量
            $contrast_three_change = abs($range_data['three_total'] - $contrast_range_data['three_total']); // 对比范围三变化量
            $contrast_all_change = abs($range_data['direct_all'] - $contrast_range_data['direct_all']); // 对比整体变化量
            $contrast_one_change_per = $range_data['one_total'] ? 100 * $contrast_one_change/$range_data['one_total'] : 0; // 对比范围一变化量百分比
            $contrast_two_change_per = $range_data['two_total'] ? 100 * $contrast_two_change/$range_data['two_total'] : 0; // 对比范围二变化量百分比
            $contrast_three_change_per = $range_data['three_total'] ? 100 * $contrast_three_change/$range_data['three_total'] : 0; // 对比范围三变化量百分比
            $contrast_all_change_per = $range_data['direct_all'] ? 100 * $contrast_all_change/$range_data['direct_all'] : 0; // 对比整体变化量百分比

        } else {
            $contrast_range_data['one_total'] = 0;
            $contrast_range_data['two_total'] = 0;
            $contrast_range_data['three_total'] = 0;
            $contrast_range_data['direct_all'] = 0;
            $contrast_range_data['contrast_one_total'] = 0;
            $contrast_range_data['contrast_two_total'] = 0;
            $contrast_range_data['contrast_three_total'] = 0;
            $contrast_range_data['contrast_direct_all'] = 0;
            $contrast_one_change = 0;
            $contrast_two_change = 0;
            $contrast_three_change = 0;
            $contrast_all_change = 0;
            $contrast_one_change_per = 0;
            $contrast_two_change_per = 0;
            $contrast_three_change_per = 0;
            $contrast_all_change_per = 0;
        }

        // 温室气体管理小组架构，图片id以逗号分隔
        $file_data = FileModel::getFileByIds($calculate_report_data['manage_build'])->toArray();
        $manage_build_str = '';
        foreach ($file_data as $key => $value) {
            $num = $key + 1;
            $manage_build_str .= '${manage_build#' . $num . '}';
        }
        $templateProcessor->setValue('manage_build', $manage_build_str);
        foreach ($file_data as $key => $value) {
            $num = $key +1;
            $templateProcessor->setImageValue('manage_build#' . $num, [
                'path' => 'http://47.122.18.241:81' . $value['file_path'],  // 路径，上传到81端口的admin_cooperate_uploads中
                'width' => '100px', //宽度
                'height' => '100px', //高度
            ]);
        }

        // 组织平面示意图，图片id以逗号分隔
        $file_data = FileModel::getFileByIds($calculate_report_data['organization_file'])->toArray();
        $organization_file_str = '';
        foreach ($file_data as $key => $value) {
            $num = $key + 1;
            $organization_file_str .= '${organization_file#' . $num . '}';
        }
        $templateProcessor->setValue('organization_file', $organization_file_str);
        foreach ($file_data as $key => $value) {
            $num = $key +1;
            $templateProcessor->setImageValue('organization_file#' . $num, [
                'path' => 'http://47.122.18.241:81' . $value['file_path'],  // 路径，上传到81端口的admin_cooperate_uploads中
                'width' => '100px', //宽度
                'height' => '100px', //高度
            ]);
        }

        // 表2-2 温室气体清单-clone field : ghg_name
        $templateProcessor->cloneRowAndSetValues('ghg_name', $range_data['range_arr']);

        // 表3-1 活动数据收集表 -clone field : active_type_name
        $templateProcessor->cloneRowAndSetValues('active_type_name', $range_data['range_arr']);

        // 表3-2 活动数据收集表 -clone field : all_factor_data
        $templateProcessor->cloneRowAndSetValues('all_factor_data', $range_data['range_arr']);

        $templateProcessor->setValues([
            // 基本信息
            'organization_name'        => $calculate_report_data['organization_name'], // 组织名称
            'calculate_year'           => $calculate_report_data['calculate_year'], // 核算年度
            'organization_description' => $calculate_report_data['business_desc'], // 企业介绍
            'contrast_year'            => $calculate_report_data['contrast_year'], // 对比年度
            'create_time'              => $calculate_report_data['create_time'], // 报告创建时间

            // 管理体系 jy_organization_info todo
            'manage_build_desc'        => $calculate_report_data['manage_build_desc'], // 温室气体管理小组架构描述
            'organization_file_desc'   => $calculate_report_data['organization_file_desc'], // 组织平面示意图描述

            // 组织边界 jy_organizational_boundaries
            'organization_setting'     => $calculate_report_data['organization_setting'], // 组织边界设定方法
            'organization_desc'        => $calculate_report_data['organization_desc'], // 组织边界描述
            'organization_change'      => $calculate_report_data['organization_change'], // 组织边界变动说明

            // 温室气体
            'gases'                    => $gas_name_str, // 温室气体种类列表，多个使用逗号分隔

            // 核算结果
            'direct_all'               => $range_data['direct_all'], // 排放总量
            'one_total'                => $range_data['one_total'], // 范围一总量
            'two_total'                => $range_data['two_total'], // 范围二总量
            'three_total'              => $range_data['three_total'], // 范围三总量
            'one_total_per'            => $range_data['one_total_per'], // 范围一排放量占比
            'two_total_per'            => $range_data['two_total_per'], // 范围二排放量占比
            'three_total_per'          => $range_data['three_total_per'], // 范围三排放量占比

            // 基准排放
            'datum_type'               => $calculate_report_data['datum_type'] == 1 ? '单一年份' : '多年平均', // 设定类型:设定类型1单一年份2多年平均
            'datum_year'               => $calculate_report_data['datum_type'] == 1 ? $calculate_report_data['start_year'] : $calculate_report_data['start_year'] . '-' . $calculate_report_data['end_year'], // 基准年
            'datum_policy'             => $calculate_report_data['datum_policy'], // 基准线排放量重算政策
            'datum_total'              => $calculate_report_data['datum_total'], // 基准线排放量
            'datum_compare'            => $range_data['direct_all'] >= $calculate_report_data['datum_total'] ? '增加' : '减少', // 比较基准排放量增加或减少
            'datum_compare_per'        => $calculate_report_data['datum_total'] ? number_format(100 * abs($range_data['direct_all'] - $calculate_report_data['datum_total'])/$calculate_report_data['datum_total'], 4) : 100, // 比较基准排放量增加或减少百分比

            // 整体排放情况
            'max_total_name'           => $range_data['max_total_name'], // 占比最大的排放分类

            // 温室气体种类排放
            'co2_total'                => $range_data['co2_total'], // CO₂排放总量
            'ch4_total'                => $range_data['ch4_total'], // CH₄排放总量
            'n2o_total'                => $range_data['n2o_total'], // N₂O排放总量
            'hfcs_total'               => $range_data['hfcs_total'], // HFCs排放总量
            'pfcs_total'               => $range_data['pfcs_total'], // PFCs排放总量
            'sf6_total'                => $range_data['sf6_total'], // SF₆排放总量
            'nf3_total'                => $range_data['nf3_total'], // NF₃排放总量
            'co2e_total'               => $range_data['co2e_total'], // 混合排放总量
            'co2_total_per'            => $range_data['co2_total_per'], // CO₂排放总量排放占比
            'ch4_total_per'            => $range_data['ch4_total_per'], // CH₄排放总量排放占比
            'n2o_total_per'            => $range_data['n2o_total_per'], // N₂O排放总量排放占比
            'hfcs_total_per'           => $range_data['hfcs_total_per'], // HFCs排放总量排放占比
            'pfcs_total_per'           => $range_data['pfcs_total_per'], // PFCs排放总量排放占比
            'sf6_total_per'            => $range_data['sf6_total_per'], // SF₆排放总量排放占比
            'nf3_total_per'            => $range_data['nf3_total_per'], // NF₃排放总量排放占比
            'co2e_total_per'           => $range_data['co2e_total_per'], // 混合排放总量排放占比

            // 占比最高气体
            'max_gas_name'             => $range_data['max_gas_name'], // 占比最高的温室气体种类
            'max_gas'                  => $range_data['max_gas'], // 占比最高的温室气体种类的排放量
            'max_gas_per'              => $range_data['max_gas_per'], // 占比最高的温室气体种类的排放占比

            // 排放量对比分析
            'calculate_one_total'      => $range_data['one_total'], // 核算年范围一总排放
            'calculate_two_total'      => $range_data['two_total'], // 核算年范围二总排放
            'calculate_three_total'    => $range_data['three_total'], // 核算年范围三总排放
            'calculate_direct_all'     => $range_data['direct_all'], // 核算年整体总排放
            'contrast_one_total'       => $contrast_range_data['one_total'], // 对比年范围一总排放
            'contrast_two_total'       => $contrast_range_data['two_total'], // 对比年范围二总排放
            'contrast_three_total'     => $contrast_range_data['three_total'], // 对比年范围三总排放
            'contrast_direct_all'      => $contrast_range_data['direct_all'], // 对比年整体总排放
            'contrast_one_change'      => $contrast_one_change, // 对比范围一变化量
            'contrast_two_change'      => $contrast_two_change, // 对比范围二变化量
            'contrast_three_change'    => $contrast_three_change, // 对比范围三变化量
            'contrast_all_change'      => $contrast_all_change, // 对比整体变化量
            'contrast_one_change_per'  => number_format($contrast_one_change_per, 4), // 对比范围一变化量百分比
            'contrast_two_change_per'  => number_format($contrast_two_change_per, 4), // 对比范围二变化量百分比
            'contrast_three_change_per'=> number_format($contrast_three_change_per, 4), // 对比范围三变化量百分比
            'contrast_all_change_per'  => number_format($contrast_all_change_per, 4), // 对比整体变化量百分比
            'contrast_compare'         => $range_data['direct_all'] >= $contrast_range_data['direct_all'] ? '增加' : '减少', // 比较对比年度排放量增加或减少

            // 不确定性分析
            'emi_cate_all'             => $range_data['emi_cate_all'], // 总加权平均分
            'grade_all'                => $range_data['grade_all'], // 加权平均评分总计
            'grade_all_des'            => $range_data['grade_all_des'], // 加权平均评分等级和说明
            'average_score_total'      => $range_data['average_score_total'], // 整体排放加权平均评分合计

            // 报告补充说明
            'exempte_description'      => $calculate_report_data['exempte_description'], // 排放源免除说明
            'change_description'       => $calculate_report_data['change_description'], // 量化方法变更说明
            'discharge_description'    => $calculate_report_data['discharge_description'], // 生物质相关排放
            'other_description'        => $calculate_report_data['other_description'], // 其他情况说明

        ]);

        // 图表类别'pie', 'doughnut', 'line', 'bar', 'stacked_bar', 'percent_stacked_bar', 'column'：柱状图文字在外面, 'stacked_column'：柱状图文字在里面, 'percent_stacked_column', 'area', 'radar', 'scatter'
        // 添加基准线排放量与实际排放量图表数据
        $datum_compare_categories = ['基准线排放量', '实际排放量'];
        $datum_compare_series = [$calculate_report_data['datum_total'], $range_data['direct_all']];

        // 图表4-1基准线排放量与实际排放量（单位：tCO2e）
        $datum_compare_chart = new \PhpOffice\PhpWord\Element\Chart('stacked_column', $datum_compare_categories, $datum_compare_series);

        $dataLabel=['showCatName'=>false];
        $datum_compare_chart->getStyle()
            // ->setTitle('图表示例')
            ->setShowAxisLabels()
            ->setWidth(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(5))
            ->setHeight(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(3))
            ->setShowGridY()
            ->setDataLabelOptions($dataLabel);
        $templateProcessor->setChart('datum_compare_chart', $datum_compare_chart);

        // 添加整体排放情况对比图图表数据
        $overall_categories = [];
        $overall_series = [];
        if ($range_data['one_total'] != 0) {
            array_push($overall_categories, '范围一');
            array_push($overall_series, abs($range_data['one_total']));
        }
        if ($range_data['two_total'] != 0) {
            array_push($overall_categories, '范围二');
            array_push($overall_series, abs($range_data['two_total']));
        }
        if ($range_data['three_total'] != 0) {
            array_push($overall_categories, '范围三');
            array_push($overall_series, abs($range_data['three_total']));
        }

        // 图表4-1整体排放情况对比图
        $overall_chart = new \PhpOffice\PhpWord\Element\Chart('pie', $overall_categories, $overall_series);

        $dataLabel = [
            'showVal' => false, // value
            'showCatName' => false, // category name
            'showLegendKey' => false, //show the cart legend
            'showSerName' => false, // series name
            'showPercent' => true,
            'showLeaderLines' => false,
            'showBubbleSize' => false,
        ];
        $overall_chart->getStyle()
            // ->setTitle('图表示例')
            ->setShowAxisLabels()
            ->setWidth(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(5))
            ->setHeight(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(3))
            // ->setShowGridY()
            ->setLegendPosition('r')
            ->setShowLegend(true)
            ->setDataLabelOptions($dataLabel);
        $templateProcessor->setChart('overall_chart', $overall_chart);

        // 添加温室气体种类排放对比图图表数据
        $gas_categories = [];
        $gas_series = [];
        if ($range_data['co2_total'] != 0) {
            array_push($gas_categories, 'CO₂');
            array_push($gas_series, abs($range_data['co2_total']));
        }
        if ($range_data['ch4_total'] != 0) {
            array_push($gas_categories, 'CH₄');
            array_push($gas_series, abs($range_data['ch4_total']));
        }
        if ($range_data['n2o_total'] != 0) {
            array_push($gas_categories, 'N₂O');
            array_push($gas_series, abs($range_data['n2o_total']));
        }
        if ($range_data['hfcs_total'] != 0) {
            array_push($gas_categories, 'HFCs');
            array_push($gas_series, abs($range_data['hfcs_total']));
        }
        if ($range_data['pfcs_total'] != 0) {
            array_push($gas_categories, 'PFCs');
            array_push($gas_series, abs($range_data['pfcs_total']));
        }
        if ($range_data['sf6_total'] != 0) {
            array_push($gas_categories, 'SF₆');
            array_push($gas_series, abs($range_data['sf6_total']));
        }
        if ($range_data['nf3_total'] != 0) {
            array_push($gas_categories, 'NF₃');
            array_push($gas_series, abs($range_data['nf3_total']));
        }
        if ($range_data['co2e_total'] != 0) {
            array_push($gas_categories, '混合');
            array_push($gas_series, abs($range_data['co2e_total']));
        }

        // 图表4-3温室气体种类排放对比图
        $gas_chart = new \PhpOffice\PhpWord\Element\Chart('pie', $gas_categories, $gas_series);

        $dataLabel = [
            'showVal' => false, // value
            'showCatName' => false, // category name
            'showLegendKey' => false, //show the cart legend
            'showSerName' => false, // series name
            'showPercent' => true,
            'showLeaderLines' => false,
            'showBubbleSize' => false,
        ];
        $gas_chart->getStyle()
            // ->setTitle('图表示例')
            ->setShowAxisLabels()
            ->setWidth(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(5))
            ->setHeight(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(3))
            // ->setShowGridY()
            ->setLegendPosition('r')
            ->setShowLegend(true)
            ->setDataLabelOptions($dataLabel);
        $templateProcessor->setChart('gas_chart', $gas_chart);

        // 添加温室气体排放对比图图表数据
        $contrast_categories = [];
        $range_data_cons = [];
        $contrast_range_data_cons = [];
        if ($range_data['one_total'] != 0 || $contrast_range_data['one_total'] != 0) {
            array_push($contrast_categories, '范围一');
            array_push($range_data_cons, abs($range_data['one_total']));
            array_push($contrast_range_data_cons, abs($contrast_range_data['one_total']));
        }
        if ($range_data['two_total'] != 0 || $contrast_range_data['two_total'] != 0) {
            array_push($contrast_categories, '范围二');
            array_push($range_data_cons, abs($range_data['two_total']));
            array_push($contrast_range_data_cons, abs($contrast_range_data['two_total']));
        }
        if ($range_data['three_total'] != 0 || $contrast_range_data['three_total'] != 0) {
            array_push($contrast_categories, '范围三');
            array_push($range_data_cons, abs($range_data['three_total']));
            array_push($contrast_range_data_cons, abs($contrast_range_data['three_total']));
        }

        // 图表4-4温室气体排放对比图
        $contrast_chart = new \PhpOffice\PhpWord\Element\Chart('column', $contrast_categories, $range_data_cons, [], $calculate_report_data['calculate_year']);
        $contrast_chart->addSeries($contrast_categories, $contrast_range_data_cons, $calculate_report_data['contrast_year']); // contrast_year 对比年 例：2022
        $dataLabel=[
            'showVal' => true, // value
            'showCatName' => false, // category name
            'showLegendKey' => false, //show the cart legend
            'showSerName' => false, // series name
            'showPercent' => false,
            'showLeaderLines' => false,
            'showBubbleSize' => false,
        ];

        $contrast_chart->getStyle()
            // ->setTitle('图表示例')
            ->setShowAxisLabels(true)
            ->setWidth(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(5))
            ->setHeight(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(3))
            ->setShowGridY()
            ->setShowLegend(true)
            ->setLegendPosition('b')
            ->setDataLabelOptions($dataLabel);
        $templateProcessor->setChart('contrast_chart', $contrast_chart);


        // 排放量对比分析区域数据block判断
        $contrast_block = $calculate_report_data['contrast_year'] != NULL ? true : false;
        $templateProcessor->cloneBlock('contrast_block', $contrast_block ? 1 : 0);
        $templateProcessor->cloneBlock('contrast_prompt_block', $contrast_block ? 0 : 1); // 暂未设置对比数据提示

        // 表4-5数据质量评分表 -clone field : average_score
        $templateProcessor->cloneRowAndSetValues('average_score', $range_data['range_arr']);

        // 减排场景 -clone field : desc
        $reduction_data = CalculateReportModel::getReduction($id)->toArray();
        $templateProcessor->cloneRowAndSetValues('desc', $reduction_data);

        // 附录1：活动数据汇总表 -clone field : active_department
        $templateProcessor->cloneRowAndSetValues('active_department', $range_data['range_arr']);

        // 附录2：排放因子选择表 -clone field : gases_choice
        $templateProcessor->cloneRowAndSetValues('gases_choice', $range_data['range_arr']);

        // 附录3：全球增温潜势值 TODO $calculate_report_data['en_short_name']
        $gwp_data = CalculateReportModel::getGases()->toArray();
        $gwp_data_new = [];
        foreach ($gwp_data as $gwp_key => $gwp_value) {
            $gwp_data_new[$gwp_key ]['id'] = $gwp_value['id'];
            $gwp_data_new[$gwp_key ]['gwp_name'] = $gwp_value['name'];
            $gwp_data_new[$gwp_key ]['gwp_choice'] = $gwp_value[$calculate_report_data['en_short_name']];
        }
        $templateProcessor->cloneRowAndSetValues('gwp_choice', $gwp_data_new);
        $templateProcessor->saveAs($filePathNew);
        $data_file['file_path'] = substr($filePathNew, 1);

        return $data_file;
    }


    /**
     * getGhgRange 生成核算报告GHG-WORD
     * 
     * @author wuyinghua
     * @param $organization_calculate_id
	 * @return $data
     */
    public function getGhgRange($organization_calculate_id){
        $emission = Db::table('jy_organization_calculate_detail_emission n')
            ->field('n.id,n.ghg_cate,n.calculate_model_id,n.emission_name,n.emissions,0 + CAST(abs(n.active_value) AS CHAR) active_value,n.emissions,n.active,n.ghg_cate,n.ghg_type,n.active_unit,
            n.active_department,n.active_type,n.active_score,n.factor_score,n.factor_source,gh.name ghg_cate_name,
            g.name ghg_name,n.active_data,n.factor_type,n.active_value_standard,
            c.molecule,c.gases,c.co2e_factor_value,c.tco2e,c.co2_factor_value,c.co2_quality,c.co2_gwp,c.co2_tco2e,
            c.ch4_factor_value,c.ch4_quality,c.ch4_gwp,c.ch4_tco2e,c.n2o_factor_value,c.n2o_quality,c.n2o_gwp,
            c.n2o_tco2e,c.hfcs_factor_value,c.hfcs_quality,c.hfcs_gwp,c.hfcs_tco2e,c.pfcs_factor_value,
            c.pfcs_quality,c.pfcs_gwp,c.pfcs_tco2e,c.sf6_factor_value,c.sf6_quality,c.sf6_gwp,
            c.sf6_tco2e,c.nf3_factor_value,c.nf3_quality,c.nf3_gwp,c.nf3_tco2e')
            ->leftJoin('jy_ghg_template g', 'g.id = n.ghg_type')
            ->leftJoin('jy_ghg_template gh', 'gh.id = n.ghg_cate')
            ->leftJoin('jy_organization_calculate_count_gas c', 'c.emi_id = n.id')
            ->where(['n.organization_calculate_id'=>(int)$organization_calculate_id, 'n.is_del'=>ParamModel::IS_DEL_NO])
            ->select()
            ->toArray();

        $emission_one = [];
        $emission_two = [];
        $emission_three = [];
        if ($emission) {
            foreach ($emission as $v) {

                if ($v['ghg_cate_name'] == '范围一') {
                    $emission_one[] = $v;
                };
                if ($v['ghg_cate_name'] == '范围二') {
                    $emission_two[] = $v;
                };
                if ($v['ghg_cate_name'] == '范围三') {
                    $emission_three[] = $v;
                };
            }
        }

        //单个分类总量
        $emi_one = 0;
        $emi_two = 0;
        $emi_three = 0;

        if ($emission_one) {
            foreach ($emission_one as $v) {
                $emi_one += $v['emissions'];
            }
        }
        if ($emission_two) {
            foreach ($emission_two as $v) {
                $emi_two += $v['emissions'];
            }
        }
        if ($emission_three) {
            foreach ($emission_three as $v) {
                $emi_three += $v['emissions'];
            }
        }

        //总排放量
        $emi_all = $emi_one + $emi_two + $emi_three;
        //单个分类加权平均评分
        $emi_cate_one = 0;
        $emi_cate_two = 0;
        $emi_cate_three = 0;
        $average_score_total = 0;
        $range_arr = [];
        foreach ($emission as $k => $v) {
            $unit_two = Db::table('jy_unit')->field('name')->where('id', $v['active_unit'][2])->find();
            $active_type_name = Config::get('emission.active_type')[$v['active_type'] - 1]['name'];
            $factor_type_name = Config::get('emission.factor_type')[$v['factor_type'] - 1]['name'];
            if($v['molecule']){
                $molecule = Config::get('emission.molecule')[$v['molecule']-1]['name'];
            }else{
                $molecule = "";
            }

            $v['unit_name'] = $unit_two['name'];
            $v['active_type_name'] = $active_type_name;
            $v['factor_type_name'] = $factor_type_name;
            $all_factor_data = $v['co2e_factor_value'] + 
                               $v['co2_factor_value'] + 
                               $v['ch4_factor_value'] + 
                               $v['n2o_factor_value'] + 
                               $v['hfcs_factor_value'] + 
                               $v['pfcs_factor_value'] + 
                               $v['sf6_factor_value'] + 
                               $v['nf3_factor_value'];

            $v['all_factor_data'] = number_format($all_factor_data, 6) . $molecule; // 当前排放源的排放因子名称 + 单位


            $v['score_result'] = $v['factor_score'] * $v['active_score'];// 评分结果：排放因子评分*活动数据评分

            //占排放分类排放量的比例（%)
            $emi_pro = 0;
            //排放分类加权平均评分
            $emi_cate = 0;
            if ($v['ghg_cate_name'] == '范围一') {
                $emi_pro = $emi_one ? $v['emissions'] / $emi_one : 0;
                $emi_cate = $v['factor_score'] * $v['active_score'] * $emi_pro;
                $emi_cate_one += $emi_cate;
            } elseif ($v['ghg_cate_name'] == '范围二') {
                $emi_pro = $emi_two ? $v['emissions'] / $emi_two : 0;
                $emi_cate = $v['factor_score'] * $v['active_score'] * $emi_pro;
                $emi_cate_two +=$emi_cate;
            } elseif ($v['ghg_cate_name'] == '范围三') {
                $emi_pro = $emi_three ? $v['emissions'] / $emi_three : 0;
                $emi_cate = $v['factor_score'] * $v['active_score'] * $emi_pro;
                $emi_cate_three += $emi_cate;
            }
            $emi_all_pro = 0;
            //占总排放量的比例（%）
            $emi_all_pro =  $emi_all ? abs($v['emissions'])/$emi_all : 0;
            $v['emi_pro'] = number_format($emi_all_pro * 100, 4) . '%'; // 占总排放量的比例（%）
            $v['average_score'] = number_format($v['factor_score'] * $v['active_score'] * $emi_all_pro, 4); // 整体排放加权平均评分
            $average_score_total += $v['average_score']; // 整体排放加权平均评分合计
            $v['gases_choice'] = $v['gases']; // 排放温室气体种类，用于附录2：排放因子选择表
            $range_arr[] = $v;
        }

        //直接排放
        $direct_total = 0;
        //间接排放
        $indirect_total = 0;
        //范围一总量
        $one_total = 0;
        //范围二总量
        $two_total = 0;
        //范围三总量
        $three_total = 0;
        //混合气体
        $co2e_one = 0;
        $co2e_two = 0;
        $co2e_three = 0;
        //二氧化碳
        $co2_one = 0;
        $co2_two = 0;
        $co2_three = 0;
        $ch4_one = 0;
        $ch4_two = 0;
        $ch4_three = 0;
        $n2o_one = 0;
        $n2o_two = 0;
        $n2o_three = 0;
        $hfcs_one = 0;
        $hfcs_two = 0;
        $hfcs_three = 0;
        $pfcs_one = 0;
        $pfcs_two = 0;
        $pfcs_three = 0;
        $sf6_one = 0;
        $sf6_two = 0;
        $sf6_three = 0;
        $nf3_one = 0;
        $nf3_two = 0;
        $nf3_three = 0;

        if ($emission_one) {
            foreach ($emission_one as $k => $v) {
                    $co2e_one += $v['co2e_factor_value'] * $v['active_value_standard'];
                    $co2_one += $v['co2_factor_value'] * $v['active_value_standard'] * $v['co2_gwp'];
                    $ch4_one += $v['ch4_factor_value'] * $v['active_value_standard'] * $v['ch4_gwp'];
                    $n2o_one += $v['n2o_factor_value'] * $v['active_value_standard'] * $v['n2o_gwp'];
                    $hfcs_one += $v['hfcs_factor_value'] * $v['active_value_standard'] * $v['hfcs_gwp'];
                    $pfcs_one += $v['pfcs_factor_value'] * $v['active_value_standard'] * $v['pfcs_gwp'];
                    $sf6_one += $v['sf6_factor_value'] * $v['active_value_standard'] * $v['sf6_gwp'];
                    $nf3_one += $v['nf3_factor_value'] * $v['active_value_standard'] * $v['nf3_gwp'];
            }
        }
        if ($emission_two) {
            foreach ($emission_two as $v) {
                $co2e_two += $v['co2e_factor_value'] * $v['active_value_standard'];
                $co2_two += $v['co2_factor_value'] * $v['active_value_standard'] * $v['co2_gwp'];
                $ch4_two += $v['ch4_factor_value'] * $v['active_value_standard'] * $v['ch4_gwp'];
                $n2o_two += $v['n2o_factor_value'] * $v['active_value_standard'] * $v['n2o_gwp'];
                $hfcs_two += $v['hfcs_factor_value'] * $v['active_value_standard'] * $v['hfcs_gwp'];
                $pfcs_two += $v['pfcs_factor_value'] * $v['active_value_standard'] * $v['pfcs_gwp'];
                $sf6_two += $v['sf6_factor_value'] * $v['active_value_standard'] * $v['sf6_gwp'];
                $nf3_two += $v['nf3_factor_value'] * $v['active_value_standard'] * $v['nf3_gwp'];
            }
        }
        if ($emission_three) {
            foreach ($emission_three as $v) {
                $co2e_three += $v['co2e_factor_value'] * $v['active_value_standard'];
                $co2_three += $v['co2_factor_value'] * $v['active_value_standard'] * $v['co2_gwp'];
                $ch4_three += $v['ch4_factor_value'] * $v['active_value_standard'] * $v['ch4_gwp'];
                $n2o_three += $v['n2o_factor_value'] * $v['active_value_standard'] * $v['n2o_gwp'];
                $hfcs_three += $v['hfcs_factor_value'] * $v['active_value_standard'] * $v['hfcs_gwp'];
                $pfcs_three += $v['pfcs_factor_value'] * $v['active_value_standard'] * $v['pfcs_gwp'];
                $sf6_three += $v['sf6_factor_value'] * $v['active_value_standard'] * $v['sf6_gwp'];
                $nf3_three += $v['nf3_factor_value'] * $v['active_value_standard'] * $v['nf3_gwp'];
            }
        }

        //范围一总量
        $one_total =  $co2_one + $ch4_one + $n2o_one + $hfcs_one + $pfcs_one + $sf6_one + $nf3_one + $co2e_one;
        //范围二总量
        $two_total =  $co2_two + $ch4_two + $n2o_two + $hfcs_two + $pfcs_two + $sf6_two + $nf3_two + $co2e_two;
        //范围三总量
        $three_total = $co2_three + $ch4_three + $n2o_three + $hfcs_three + $pfcs_three + $sf6_three + $nf3_three + $co2e_three;
        //直接排放
        $direct_total = $one_total;
        //间接排放
        $indirect_total = $two_total + $three_total;
        //排放总量
        $direct_all = $direct_total + $indirect_total;
        // CO₂排放总量
        $co2_total = $co2_one + $co2_two + $co2_three;
        // CH₄排放总量
        $ch4_total = $ch4_one + $ch4_two + $ch4_three;
        // N₂O排放总量
        $n2o_total = $n2o_one + $n2o_two + $n2o_three;
        // HFCs排放总量
        $hfcs_total = $hfcs_one + $hfcs_two + $hfcs_three;
        // PFCs排放总量
        $pfcs_total = $pfcs_one + $pfcs_two + $pfcs_three;
        // SF₆排放总量
        $sf6_total = $sf6_one + $sf6_two + $sf6_three;
        // NF₃排放总量
        $nf3_total = $nf3_one + $nf3_two + $nf3_three;
        // 混合排放总量
        $co2e_total = $co2e_one + $co2e_two + $co2e_three;
        // CO₂排放总量排放占比
        $co2_total_per = $direct_all ? 100 * $co2_total/$direct_all : 0;
        // CH₄排放总量排放占比
        $ch4_total_per = $direct_all ? 100 * $ch4_total/$direct_all : 0;
        // N₂O排放总量排放占比
        $n2o_total_per = $direct_all ? 100 * $n2o_total/$direct_all : 0;
        // HFCs排放总量排放占比
        $hfcs_total_per = $direct_all ? 100 * $hfcs_total/$direct_all : 0;
        // PFCs排放总量排放占比
        $pfcs_total_per = $direct_all ? 100 * $pfcs_total/$direct_all : 0;
        // SF₆排放总量排放占比
        $sf6_total_per = $direct_all ? 100 * $sf6_total/$direct_all : 0;
        // NF₃排放总量排放占比
        $nf3_total_per = $direct_all ? 100 * $nf3_total/$direct_all : 0;
        // 混合排放总量排放占比
        $co2e_total_per = $direct_all ? 100 * $co2e_total/$direct_all : 0;
        // 加权平均评分总计
        $emi_cate_all = $emi_cate_one + $emi_cate_two + $emi_cate_three;

        $one_total_per = $direct_all ? 100 * $one_total/$direct_all : 0; // 范围一排放量占比
        $two_total_per = $direct_all ? 100 * $two_total/$direct_all : 0; // 范围二排放量占比
        $three_total_per = $direct_all ? 100 * $three_total/$direct_all : 0; // 范围三排放量占比
        $max_total = ($three_total >= ($one_total >= $two_total ? $one_total : $two_total) ? $three_total : ($one_total >= $two_total ? $one_total : $two_total )); // 最大的占比
        if ($max_total == $one_total) {
            $max_total_name = '范围一';
        } elseif ($max_total == $two_total) {
            $max_total_name = '范围二';
        } else {
            $max_total_name = '范围三';
        }

        // 排放量最大的温室气体
        $max_gas = max([$co2_total, $ch4_total, $n2o_total, $hfcs_total, $pfcs_total, $sf6_total, $nf3_total, $co2e_total]);
        if ($max_gas == $co2_total) {
            $max_gas_name = 'CO₂';
            $max_gas_per = $direct_all ? 100 * $co2_total/$direct_all : 0;

        } elseif ($max_gas == $ch4_total) {
            $max_gas_name = 'CH₄';
            $max_gas_per = $direct_all ? 100 * $ch4_total/$direct_all : 0;

        } elseif ($max_gas == $n2o_total) {
            $max_gas_name = 'N₂O';
            $max_gas_per = $direct_all ? 100 * $n2o_total/$direct_all : 0;

        } elseif ($max_gas == $hfcs_total) {
            $max_gas_name = 'HFCs';
            $max_gas_per = $direct_all ? 100 * $hfcs_total/$direct_all : 0;

        } elseif ($max_gas == $pfcs_total) {
            $max_gas_name = 'PFCs';
            $max_gas_per = $direct_all ? 100 * $pfcs_total/$direct_all : 0;

        } elseif ($max_gas == $sf6_total) {
            $max_gas_name = 'SF₆';
            $max_gas_per = $direct_all ? 100 * $sf6_total/$direct_all : 0;

        } elseif ($max_gas == $nf3_total) {
            $max_gas_name = 'NF₃';
            $max_gas_per = $direct_all ? 100 * $nf3_total/$direct_all : 0;

        } else {
            $max_gas_name = '混合';
            $max_gas_per = $direct_all ? 100 * $co2e_total/$direct_all : 0;

        }

        // 不确定性分析，数据质量等级及等级说明
        $grade_all = '';
        $grade_all_des = '';
        if($emi_cate_all >= 0 && $emi_cate_all < 7){
            $grade_all = '6级';
            $grade_all_des = '不确定性极高，数据品质极差';

        } elseif ($emi_cate_all >= 7 && $emi_cate_all < 13){
            $grade_all ='5级';
            $grade_all_des = '不确定性高，数据品质差';

        } elseif ($emi_cate_all >= 13 && $emi_cate_all < 19){
            $grade_all = '4级';
            $grade_all_des = '不确定性略高，数据品质较差';

        } elseif ($emi_cate_all >= 19 && $emi_cate_all < 25){
            $grade_all ='3级';
            $grade_all_des = '不确定性偏高，数据品质不佳';

        } elseif ($emi_cate_all >= 25 && $emi_cate_all < 31){
            $grade_all = '2级';
            $grade_all_des = '不确定性低，数据品质佳';

        // } elseif ($emi_cate_all >= 31 && $emi_cate_all <= 36){ TODO 由于加权平均评分总计会大于36，先放开
        } elseif ($emi_cate_all >= 31){
            $grade_all = '1级';
            $grade_all_des = '不确定性极低，数据品质极佳';

        }

        $data['range_arr'] = $range_arr;
        $data['direct_all'] = $direct_all;  // 排放总量
        $data['one_total'] = $one_total; // 范围一总量
        $data['two_total'] = $two_total; // 范围二总量
        $data['three_total'] = $three_total; // 范围三总量
        $data['one_total_per'] = number_format($one_total_per, 4); // 范围一排放量占比
        $data['two_total_per'] = number_format($two_total_per, 4); // 范围二排放量占比
        $data['three_total_per'] = number_format($three_total_per, 4); // 范围三排放量占比
        $data['max_total_name'] = $max_total_name; // 最大的占比名称，例：范围一

        $data['co2_total'] = $co2_total; // CO₂排放总量
        $data['ch4_total'] = $ch4_total; // CH₄排放总量
        $data['n2o_total'] = $n2o_total; // N₂O排放总量
        $data['hfcs_total'] = $hfcs_total; // HFCs排放总量
        $data['pfcs_total'] = $pfcs_total; // PFCs排放总量
        $data['sf6_total'] = $sf6_total; // SF₆排放总量
        $data['nf3_total'] = $nf3_total; // NF₃排放总量
        $data['co2e_total'] = $co2e_total; // 混合排放总量

        $data['co2_total_per'] = number_format($co2_total_per, 4); // CO₂排放总量排放占比
        $data['ch4_total_per'] = number_format($ch4_total_per, 4); // CH₄排放总量排放占比
        $data['n2o_total_per'] = number_format($n2o_total_per, 4); // N₂O排放总量排放占比
        $data['hfcs_total_per'] = number_format($hfcs_total_per, 4); // HFCs排放总量排放占比
        $data['pfcs_total_per'] = number_format($pfcs_total_per, 4); // PFCs排放总量排放占比
        $data['sf6_total_per'] = number_format($sf6_total_per, 4); // SF₆排放总量排放占比
        $data['nf3_total_per'] = number_format($nf3_total_per, 4); // NF₃排放总量排放占比
        $data['co2e_total_per'] = number_format($co2e_total_per, 4); // 混合排放总量排放占比

        $data['max_gas_name'] = $max_gas_name; // 排放量最大的温室气体名称
        $data['max_gas_per'] = number_format($max_gas_per, 4); // 排放量最大的温室气体百分比
        $data['max_gas'] = $max_gas; // 占比最高的温室气体种类的排放量

        $data['emi_cate_all'] = $emi_cate_all;// 总加权平均分
        $data['grade_all'] = $grade_all;// 加权平均评分总计
        $data['grade_all_des'] = $grade_all_des;// 加权平均评分等级和说明
        $data['average_score_total'] = $average_score_total;// 整体排放加权平均评分合计

        return $data;
    }

    /**
     * isoWord 生成核算报告ISO-WORD
     * 
     * @author wuyinghua
	 * @return $data
     */
    public function isoWord() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data_redis = $this->request->middleware('data_redis');
        $data['userid'] = $data_redis['userid'];
        $data['main_organization_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : ''; // 母公司id
        $data['id'] = $params_payload_arr['id'];
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $data['organization_id'] = $params_payload_arr['organization_id'];
        if ($data['organization_id'] == '' || $data['organization_id'] == null || $data['organization_id'] == 0) {
            return json(['code'=>201, 'message'=>"参数organization_id错误"]);
        }

        $calculate_report_data = $calculate_report_data = CalculateReportModel::getCalculateReport($data['id']);
        $source_name = $calculate_report_data['organization_name'] . $calculate_report_data['calculate_year'] . '企业温室气体排放报告（ISO版本）' . '.docx';

        $data_file['module'] = '组织碳核算';
        $data_file['main_organization_id'] = $data['main_organization_id'];
        $data_file['organization_id'] = $data['organization_id'];
        $data_file['source_name'] = $source_name;
        $data_file['type'] = 2; // 类型为GHG或ISO文件
        $data_file['create_by'] = $data['userid'];
        $data_file['modify_by'] = $data['userid'];
        $data_file['create_time'] = date('Y-m-d H:i:s');
        $data_file['modify_time'] = date('Y-m-d H:i:s');
        $data_file['id'] = guid();
        $data_file_id = $data_file['id'];
        FileModel::addFile($data_file);

        Db::startTrans();
        try {
            $data_file = $this->createIsoWord($data['id']);

            // 添加操作日志
            $data_log['main_organization_id'] = $data['main_organization_id'];
            $data_log['user_id'] = $data['userid'];
            $data_log['module'] = '企业碳核算';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '创建：' . $source_name;

            FileModel::updateFile($data_file, $data_file_id);
            OperationModel::addOperation($data_log);

            Db::commit();
            return json(['code'=>200, 'message'=>"ISO核算报告生成成功"]);
        } catch (\Exception $e) {
            Db::rollback();
            return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
        }
    }

    /**
     * checkOrgFile 定时查找并生成`jy_file`表中`组织碳核算生成中`的文件
     * 
     * @author wuyinghua
	 * @return $data
     */
    public function checkOrgFile() {
        $update = FileModel::updateFile();

        if ($update >= 0) {
            return json(['code'=>200, 'message'=>"执行成功，变更状态" . $update . "条"]);
        } else {
            return json(['code'=>400, 'message'=>"执行失败"]);
        }
    }

    /**
     * createIsoWord 生成核算报告ISO-WORD
     * 
     * @author wuyinghua
     * @param $id
	 * @return $data
     */
    public function createIsoWord($id) {
        ini_set('max_execution_time', '999999999');
        // 核算报告数据
        $calculate_report_data = CalculateReportModel::getCalculateReport($id);

        // 碳核算- 排放量计算数据
        $count_gas_data = CalculateReportModel::getCalculateCountGas($calculate_report_data['organization_calculate_id'])->toArray();
        $gas_name_arr = [];
        foreach ($count_gas_data as $gas_key => $gas_value) {
            $gas_name_arr = array_merge_recursive($gas_name_arr, explode(',', $gas_value['gases']));
        }
        $gas_name_arr = array_filter(array_unique($gas_name_arr));
        $gas_name_str = implode(',', $gas_name_arr);
        

        $templatePath = "static/企业温室气体排放报告（ISO版本）.docx";
        // 判断目录是否存在
        $filePath= './storage/uploads/'. date('Ymd', time());
        if (!is_dir($filePath)) {
            mkdir($filePath, 0777);
        }
        $filePathNew = $filePath . '/' . time() . '.docx';
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templatePath);

        // ISO列表
        $iso_list = CalculateReportModel::isoList($calculate_report_data['data_quality_id']);
        $iso_arr = [];
        foreach ($iso_list as $iso_key => $iso_value) {
            foreach ($iso_value['children'] as $k => $v) {
                $iso_arr[] = $v;
            }
        }
        // return json($iso_arr);
        foreach ($iso_arr as $key => $value) {
            $num = $key + 1;
            $templateProcessor->setValues([
                'active_desc#' . $num     => $value['active_desc'], // 活动描述/排除的说明
                'collection_instructions#' . $num     => $value['collection_instructions'], // 数据收集说明
                'calculation#' . $num     => '', // 数计算方法 todo
                'calculation_description#' . $num    => $value['calculation_description'], // 数计算方法描述
                'data_description#' . $num     => $value['data_description'], // 数据存储说明
            ]);
        }

        // 核算年base data
        $range_data = $this->getISORange($calculate_report_data['organization_calculate_id']);
        $calculate_org_data = CalculateReportModel::getCalculateByContrastYear($calculate_report_data['organization_id'], $calculate_report_data['contrast_year']);
        // 根据组织ID和是否有对比年 判断是否有对比年base data
        if ($calculate_org_data != NULL) {
            $contrast_range_data = $this->getISORange($calculate_org_data['id']);
        }

        if ($calculate_report_data['contrast_year'] != NULL && $calculate_org_data != NULL && $contrast_range_data != NULL) {
            $contrast_one_change = abs($range_data['one_total'] - $contrast_range_data['one_total']); // 对比直接排放或清除变化量
            $contrast_two_change = abs($range_data['two_total'] - $contrast_range_data['two_total']); // 对比能源间接排放变化量
            $contrast_three_change = abs($range_data['three_total'] - $contrast_range_data['three_total']); // 对比运输间接排放变化量
            $contrast_four_change = abs($range_data['four_total'] - $contrast_range_data['four_total']); // 对比外购产品或服务间接排放变化量
            $contrast_five_change = abs($range_data['five_total'] - $contrast_range_data['five_total']); // 对比供应链下游排放变化量
            $contrast_six_change = abs($range_data['six_total'] - $contrast_range_data['six_total']); // 对比其他间接排放变化量
            $contrast_all_change = abs($range_data['direct_all'] - $contrast_range_data['direct_all']); // 对比整体变化量
            $contrast_one_change_per = $range_data['one_total'] ? 100 * $contrast_one_change/$range_data['one_total'] : 0; // 对比直接排放或清除变化量百分比
            $contrast_two_change_per = $range_data['two_total'] ? 100 * $contrast_two_change/$range_data['two_total'] : 0; // 对比能源间接排放变化量百分比
            $contrast_three_change_per = $range_data['three_total'] ? 100 * $contrast_three_change/$range_data['three_total'] : 0; // 对比运输间接排放变化量百分比
            $contrast_four_change_per = $range_data['four_total'] ? 100 * $contrast_four_change/$range_data['four_total'] : 0; // 对比外购产品或服务间接排放变化量百分比
            $contrast_five_change_per = $range_data['five_total'] ? 100 * $contrast_five_change/$range_data['five_total'] : 0; // 对比供应链下游排放变化量百分比
            $contrast_six_change_per = $range_data['six_total'] ? 100 * $contrast_six_change/$range_data['six_total'] : 0; // 对比其他间接排放变化量百分比
            $contrast_all_change_per = $range_data['direct_all'] ? 100 * $contrast_all_change/$range_data['direct_all'] : 0; // 对比整体变化量百分比

        } else {
            $contrast_range_data['one_total'] = 0;
            $contrast_range_data['two_total'] = 0;
            $contrast_range_data['three_total'] = 0;
            $contrast_range_data['four_total'] = 0;
            $contrast_range_data['five_total'] = 0;
            $contrast_range_data['six_total'] = 0;
            $contrast_range_data['direct_all'] = 0;
            $contrast_range_data['contrast_one_total'] = 0;
            $contrast_range_data['contrast_two_total'] = 0;
            $contrast_range_data['contrast_three_total'] = 0;
            $contrast_range_data['contrast_four_total'] = 0;
            $contrast_range_data['contrast_five_total'] = 0;
            $contrast_range_data['contrast_six_total'] = 0;
            $contrast_range_data['contrast_direct_all'] = 0;
            $contrast_one_change = 0;
            $contrast_two_change = 0;
            $contrast_three_change = 0;
            $contrast_four_change = 0;
            $contrast_five_change = 0;
            $contrast_six_change = 0;
            $contrast_all_change = 0;
            $contrast_one_change_per = 0;
            $contrast_two_change_per = 0;
            $contrast_three_change_per = 0;
            $contrast_four_change_per = 0;
            $contrast_five_change_per = 0;
            $contrast_six_change_per = 0;
            $contrast_all_change_per = 0;
        }

        // 温室气体管理小组架构，图片id以逗号分隔
        $file_data = FileModel::getFileByIds($calculate_report_data['manage_build'])->toArray();
        $manage_build_str = '';
        foreach ($file_data as $key => $value) {
            $num = $key + 1;
            $manage_build_str .= '${manage_build#' . $num . '}';
        }
        $templateProcessor->setValue('manage_build', $manage_build_str);
        foreach ($file_data as $key => $value) {
            $num = $key + 1;
            $templateProcessor->setImageValue('manage_build#' . $num, [
                'path' => 'http://47.122.18.241:81' . $value['file_path'],  // 路径，上传到81端口的admin_cooperate_uploads中
                'width' => '100px', //宽度
                'height' => '100px', //高度
            ]);
        }

        // 组织平面示意图，图片id以逗号分隔
        $file_data = FileModel::getFileByIds($calculate_report_data['organization_file'])->toArray();
        $organization_file_str = '';
        foreach ($file_data as $key => $value) {
            $num = $key + 1;
            $organization_file_str .= '${organization_file#' . $num . '}';
        }
        $templateProcessor->setValue('organization_file', $organization_file_str);
        foreach ($file_data as $key => $value) {
            $num = $key +1;
            $templateProcessor->setImageValue('organization_file#' . $num, [
                'path' => 'http://47.122.18.241:81' . $value['file_path'],  // 路径，上传到81端口的admin_cooperate_uploads中
                'width' => '100px', //宽度
                'height' => '100px', //高度
            ]);
        }

        // 表2-2 温室气体清单-clone field : iso_name
        $templateProcessor->cloneRowAndSetValues('iso_name', $range_data['range_arr']);

        // 表3-1 活动数据收集表 -clone field : active_type_name
        $templateProcessor->cloneRowAndSetValues('active_type_name', $range_data['range_arr']);

        // 表3-2 活动数据收集表 -clone field : all_factor_data
        $templateProcessor->cloneRowAndSetValues('all_factor_data', $range_data['range_arr']);

        $templateProcessor->setValues([
            // 基本信息
            'organization_name'        => $calculate_report_data['organization_name'], // 组织名称
            'calculate_year'           => $calculate_report_data['calculate_year'], // 核算年度
            'organization_description' => $calculate_report_data['business_desc'], // 企业介绍
            'contrast_year'            => $calculate_report_data['contrast_year'], // 对比年度
            'create_time'              => $calculate_report_data['create_time'], // 报告创建时间

            // 管理体系 jy_organization_info todo
            'manage_build_desc'        => $calculate_report_data['manage_build_desc'], // 温室气体管理小组架构描述
            'organization_file_desc'   => $calculate_report_data['organization_file_desc'], // 组织平面示意图描述

            // 组织边界 jy_organizational_boundaries
            'organization_setting'     => $calculate_report_data['organization_setting'], // 组织边界设定方法
            'organization_desc'        => $calculate_report_data['organization_desc'], // 组织边界描述
            'organization_change'      => $calculate_report_data['organization_change'], // 组织边界变动说明

            // 温室气体
            'gases'                    => $gas_name_str, // 温室气体种类列表，多个使用逗号分隔

            // 核算结果
            'direct_all'               => $range_data['direct_all'], // 排放总量
            'one_total'                => $range_data['one_total'], // 直接排放或清除总量
            'two_total'                => $range_data['two_total'], // 能源间接排放总量
            'three_total'              => $range_data['three_total'], // 运输间接排放总量
            'four_total'               => $range_data['four_total'], // 外购产品或服务间接排放总量
            'five_total'               => $range_data['five_total'], // 供应链下游排放总量
            'six_total'                => $range_data['six_total'], // 其他间接排放总量
            'one_total_per'            => $range_data['one_total_per'], // 直接排放或清除排放量占比
            'two_total_per'            => $range_data['two_total_per'], // 能源间接排放排放量占比
            'three_total_per'          => $range_data['three_total_per'], // 运输间接排放排放量占比
            'four_total_per'           => $range_data['four_total_per'], // 外购产品或服务间接排放排放量占比
            'five_total_per'           => $range_data['five_total_per'], // 供应链下游排放排放量占比
            'six_total_per'            => $range_data['six_total_per'], // 其他间接排放排放量占比

            // 基准排放
            'datum_type'               => $calculate_report_data['datum_type'] == 1 ? '单一年份' : '多年平均', // 设定类型:设定类型1单一年份2多年平均
            'datum_year'               => $calculate_report_data['datum_type'] == 1 ? $calculate_report_data['start_year'] : $calculate_report_data['start_year'] . '-' . $calculate_report_data['end_year'], // 基准年
            'datum_policy'             => $calculate_report_data['datum_policy'], // 基准线排放量重算政策
            'datum_total'              => $calculate_report_data['datum_total'], // 基准线排放量
            'datum_compare'            => $range_data['direct_all'] >= $calculate_report_data['datum_total'] ? '增加' : '减少', // 比较基准排放量增加或减少
            'datum_compare_per'        => $calculate_report_data['datum_total'] ? number_format(100 * abs($range_data['direct_all'] - $calculate_report_data['datum_total'])/$calculate_report_data['datum_total'], 4) : 100, // 比较基准排放量增加或减少百分比

            // 整体排放情况
            'max_total_name'           => $range_data['max_total_name'], // 占比最大的排放分类

            // 温室气体种类排放
            'co2_total'                => $range_data['co2_total'], // CO₂排放总量
            'ch4_total'                => $range_data['ch4_total'], // CH₄排放总量
            'n2o_total'                => $range_data['n2o_total'], // N₂O排放总量
            'hfcs_total'               => $range_data['hfcs_total'], // HFCs排放总量
            'pfcs_total'               => $range_data['pfcs_total'], // PFCs排放总量
            'sf6_total'                => $range_data['sf6_total'], // SF₆排放总量
            'nf3_total'                => $range_data['nf3_total'], // NF₃排放总量
            'co2e_total'               => $range_data['co2e_total'], // 混合排放总量
            'co2_total_per'            => $range_data['co2_total_per'], // CO₂排放总量排放占比
            'ch4_total_per'            => $range_data['ch4_total_per'], // CH₄排放总量排放占比
            'n2o_total_per'            => $range_data['n2o_total_per'], // N₂O排放总量排放占比
            'hfcs_total_per'           => $range_data['hfcs_total_per'], // HFCs排放总量排放占比
            'pfcs_total_per'           => $range_data['pfcs_total_per'], // PFCs排放总量排放占比
            'sf6_total_per'            => $range_data['sf6_total_per'], // SF₆排放总量排放占比
            'nf3_total_per'            => $range_data['nf3_total_per'], // NF₃排放总量排放占比
            'co2e_total_per'           => $range_data['co2e_total_per'], // 混合排放总量排放占比

            // 占比最高气体
            'max_gas_name'             => $range_data['max_gas_name'], // 占比最高的温室气体种类
            'max_gas'                  => $range_data['max_gas'], // 占比最高的温室气体种类的排放量
            'max_gas_per'              => $range_data['max_gas_per'], // 占比最高的温室气体种类的排放占比

            // 排放量对比分析
            'calculate_one_total'      => $range_data['one_total'], // 核算年直接排放或清除总排放
            'calculate_two_total'      => $range_data['two_total'], // 核算年能源间接排放总排放
            'calculate_three_total'    => $range_data['three_total'], // 核算年运输间接排放总排放
            'calculate_four_total'     => $range_data['four_total'], // 核算年外购产品或服务间接排放总排放
            'calculate_five_total'     => $range_data['five_total'], // 核算年供应链下游排放总排放
            'calculate_six_total'      => $range_data['six_total'], // 核算年其他间接排放总排放
            'calculate_direct_all'     => $range_data['direct_all'], // 核算年整体总排放
            'contrast_one_total'       => $contrast_range_data['one_total'], // 对比年直接排放或清除总排放
            'contrast_two_total'       => $contrast_range_data['two_total'], // 对比年能源间接排放总排放
            'contrast_three_total'     => $contrast_range_data['three_total'], // 对比年运输间接排放总排放
            'contrast_four_total'      => $contrast_range_data['four_total'], // 对比年外购产品或服务间接排放总排放
            'contrast_five_total'      => $contrast_range_data['five_total'], // 对比年供应链下游排放总排放
            'contrast_six_total'       => $contrast_range_data['six_total'], // 对比年其他间接排放总排放
            'contrast_direct_all'      => $contrast_range_data['direct_all'], // 对比年整体总排放
            'contrast_one_change'      => $contrast_one_change, // 对比直接排放或清除变化量
            'contrast_two_change'      => $contrast_two_change, // 对比能源间接排放变化量
            'contrast_three_change'    => $contrast_three_change, // 对比运输间接排放变化量
            'contrast_four_change'     => $contrast_four_change, // 对比外购产品或服务间接排放变化量
            'contrast_five_change'     => $contrast_five_change, // 对比供应链下游排放变化量
            'contrast_six_change'      => $contrast_six_change, // 对比其他间接排放变化量
            'contrast_all_change'      => $contrast_all_change, // 对比整体变化量
            'contrast_one_change_per'  => number_format($contrast_one_change_per, 4), // 对比直接排放或清除变化量百分比
            'contrast_two_change_per'  => number_format($contrast_two_change_per, 4), // 对比能源间接排放变化量百分比
            'contrast_three_change_per'=> number_format($contrast_three_change_per, 4), // 对比运输间接排放变化量百分比
            'contrast_four_change_per' => number_format($contrast_four_change_per, 4), // 对比外购产品或服务间接排放变化量百分比
            'contrast_five_change_per' => number_format($contrast_five_change_per, 4), // 对比供应链下游排放变化量百分比
            'contrast_six_change_per'  => number_format($contrast_six_change_per, 4), // 对比其他间接排放变化量百分比
            'contrast_all_change_per'  => number_format($contrast_all_change_per, 4), // 对比整体变化量百分比
            'contrast_compare'         => $range_data['direct_all'] >= $contrast_range_data['direct_all'] ? '增加' : '减少', // 比较对比年度排放量增加或减少

            // 不确定性分析
            'emi_cate_all'             => $range_data['emi_cate_all'], // 总加权平均分
            'grade_all'                => $range_data['grade_all'], // 加权平均评分总计
            'grade_all_des'            => $range_data['grade_all_des'], // 加权平均评分等级和说明
            'average_score_total'      => $range_data['average_score_total'], // 整体排放加权平均评分合计

            // 报告补充说明
            'exempte_description'      => $calculate_report_data['exempte_description'], // 排放源免除说明
            'change_description'       => $calculate_report_data['change_description'], // 量化方法变更说明
            'discharge_description'    => $calculate_report_data['discharge_description'], // 生物质相关排放
            'other_description'        => $calculate_report_data['other_description'], // 其他情况说明

        ]);

        // 图表类别'pie', 'doughnut', 'line', 'bar', 'stacked_bar', 'percent_stacked_bar', 'column'：柱状图文字在外面, 'stacked_column'：柱状图文字在里面, 'percent_stacked_column', 'area', 'radar', 'scatter'
        // 添加基准线排放量与实际排放量图表数据
        $datum_compare_categories = ['基准线排放量', '实际排放量'];
        $datum_compare_series = [$calculate_report_data['datum_total'], $range_data['direct_all']];

        // 图表4-1基准线排放量与实际排放量（单位：tCO2e）
        $datum_compare_chart = new \PhpOffice\PhpWord\Element\Chart('stacked_column', $datum_compare_categories, $datum_compare_series);

        $dataLabel=['showCatName'=>false];
        $datum_compare_chart->getStyle()
            // ->setTitle('图表示例')
            ->setShowAxisLabels()
            ->setWidth(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(5))
            ->setHeight(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(3))
            ->setShowGridY()
            ->setDataLabelOptions($dataLabel);
        $templateProcessor->setChart('datum_compare_chart', $datum_compare_chart);

        // 添加整体排放情况对比图图表数据
        $overall_categories = [];
        $overall_series = [];
        if ($range_data['one_total'] != 0) {
            array_push($overall_categories, '直接排放或清除');
            array_push($overall_series, abs($range_data['one_total']));
        }
        if ($range_data['two_total'] != 0) {
            array_push($overall_categories, '能源间接排放');
            array_push($overall_series, abs($range_data['two_total']));
        }
        if ($range_data['three_total'] != 0) {
            array_push($overall_categories, '运输间接排放');
            array_push($overall_series, abs($range_data['three_total']));
        }
        if ($range_data['four_total'] != 0) {
            array_push($overall_categories, '外购产品或服务间接排放');
            array_push($overall_series, abs($range_data['four_total']));
        }
        if ($range_data['five_total'] != 0) {
            array_push($overall_categories, '供应链下游排放');
            array_push($overall_series, abs($range_data['five_total']));
        }
        if ($range_data['six_total'] != 0) {
            array_push($overall_categories, '其他间接排放');
            array_push($overall_series, abs($range_data['six_total']));
        }

        // 图表4-1整体排放情况对比图
        $overall_chart = new \PhpOffice\PhpWord\Element\Chart('pie', $overall_categories, $overall_series);

        $dataLabel=[
            'showVal' => false, // value
            'showCatName' => false, // category name
            'showLegendKey' => false, //show the cart legend
            'showSerName' => false, // series name
            'showPercent' => true,
            'showLeaderLines' => false,
            'showBubbleSize' => false,
        ];
        $overall_chart->getStyle()
            // ->setTitle('图表示例')
            ->setShowAxisLabels()
            ->setWidth(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(5))
            ->setHeight(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(3))
            // ->setShowGridY()
            ->setLegendPosition('r')
            ->setShowLegend(true)
            ->setDataLabelOptions($dataLabel);
        $templateProcessor->setChart('overall_chart', $overall_chart);

        // 添加温室气体种类排放对比图图表数据
        $gas_categories = [];
        $gas_series = [];
        if ($range_data['co2_total'] != 0) {
            array_push($gas_categories, 'CO₂');
            array_push($gas_series, abs($range_data['co2_total']));
        }
        if ($range_data['ch4_total'] != 0) {
            array_push($gas_categories, 'CH₄');
            array_push($gas_series, abs($range_data['ch4_total']));
        }
        if ($range_data['n2o_total'] != 0) {
            array_push($gas_categories, 'N₂O');
            array_push($gas_series, abs($range_data['n2o_total']));
        }
        if ($range_data['hfcs_total'] != 0) {
            array_push($gas_categories, 'HFCs');
            array_push($gas_series, abs($range_data['hfcs_total']));
        }
        if ($range_data['pfcs_total'] != 0) {
            array_push($gas_categories, 'PFCs');
            array_push($gas_series, abs($range_data['pfcs_total']));
        }
        if ($range_data['sf6_total'] != 0) {
            array_push($gas_categories, 'SF₆');
            array_push($gas_series, abs($range_data['sf6_total']));
        }
        if ($range_data['nf3_total'] != 0) {
            array_push($gas_categories, 'NF₃');
            array_push($gas_series, abs($range_data['nf3_total']));
        }
        if ($range_data['co2e_total'] != 0) {
            array_push($gas_categories, '混合');
            array_push($gas_series, abs($range_data['co2e_total']));
        }

        // 图表4-3温室气体种类排放对比图
        $gas_chart = new \PhpOffice\PhpWord\Element\Chart('pie', $gas_categories, $gas_series);

        $dataLabel=[
            'showVal' => false, // value
            'showCatName' => false, // category name
            'showLegendKey' => false, //show the cart legend
            'showSerName' => false, // series name
            'showPercent' => true,
            'showLeaderLines' => false,
            'showBubbleSize' => false,
        ];
        $gas_chart->getStyle()
            // ->setTitle('图表示例')
            ->setShowAxisLabels()
            ->setWidth(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(5))
            ->setHeight(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(3))
            // ->setShowGridY()
            ->setLegendPosition('r')
            ->setShowLegend(true)
            ->setDataLabelOptions($dataLabel);
        $templateProcessor->setChart('gas_chart', $gas_chart);

        // 添加温室气体排放对比图图表数据
        $contrast_categories = [];
        $range_data_cons = [];
        $contrast_range_data_cons = [];
        if ($range_data['one_total'] != 0 || $contrast_range_data['one_total'] != 0) {
            array_push($contrast_categories, '直接排放或清除');
            array_push($range_data_cons, abs($range_data['one_total']));
            array_push($contrast_range_data_cons, abs($contrast_range_data['one_total']));
        }
        if ($range_data['two_total'] != 0 || $contrast_range_data['two_total'] != 0) {
            array_push($contrast_categories, '能源间接排放');
            array_push($range_data_cons, abs($range_data['two_total']));
            array_push($contrast_range_data_cons, abs($contrast_range_data['two_total']));
        }
        if ($range_data['three_total'] != 0 || $contrast_range_data['three_total'] != 0) {
            array_push($contrast_categories, '运输间接排放');
            array_push($range_data_cons, abs($range_data['three_total']));
            array_push($contrast_range_data_cons, abs($contrast_range_data['three_total']));
        }
        if ($range_data['four_total'] != 0 || $contrast_range_data['four_total'] != 0) {
            array_push($contrast_categories, '外购产品或服务间接排放');
            array_push($range_data_cons, abs($range_data['four_total']));
            array_push($contrast_range_data_cons, abs($contrast_range_data['four_total']));
        }
        if ($range_data['five_total'] != 0 || $contrast_range_data['five_total'] != 0) {
            array_push($contrast_categories, '供应链下游排放');
            array_push($range_data_cons, abs($range_data['five_total']));
            array_push($contrast_range_data_cons, abs($contrast_range_data['five_total']));
        }
        if ($range_data['six_total'] != 0 || $contrast_range_data['six_total'] != 0) {
            array_push($contrast_categories, '其他间接排放');
            array_push($range_data_cons, abs($range_data['six_total']));
            array_push($contrast_range_data_cons, abs($contrast_range_data['six_total']));
        }

        // 图表4-4温室气体排放对比图
        $contrast_chart = new \PhpOffice\PhpWord\Element\Chart('column', $contrast_categories, $range_data_cons, [], $calculate_report_data['calculate_year']);
        $contrast_chart->addSeries($contrast_categories, $contrast_range_data_cons, $calculate_report_data['contrast_year']); // contrast_year 对比年 例：2022

        $dataLabel=[
            'showVal' => true, // value
            'showCatName' => false, // category name
            'showLegendKey' => false, //show the cart legend
            'showSerName' => false, // series name
            'showPercent' => false,
            'showLeaderLines' => false,
            'showBubbleSize' => false,
        ];

        $contrast_chart->getStyle()
            // ->setTitle('图表示例')
            ->setShowAxisLabels(true)
            ->setWidth(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(5))
            ->setHeight(\PhpOffice\PhpWord\Shared\Converter::inchToEmu(3))
            ->setShowGridY()
            ->setShowLegend(true)
            ->setLegendPosition('b')
            ->setDataLabelOptions($dataLabel);
        $templateProcessor->setChart('contrast_chart', $contrast_chart);


        // 排放量对比分析区域数据block判断
        $contrast_block = $calculate_report_data['contrast_year'] != NULL ? true : false;
        $templateProcessor->cloneBlock('contrast_block', $contrast_block ? 1 : 0);
        $templateProcessor->cloneBlock('contrast_prompt_block', $contrast_block ? 0 : 1); // 暂未设置对比数据提示

        // 表4-5数据质量评分表 -clone field : average_score
        $templateProcessor->cloneRowAndSetValues('average_score', $range_data['range_arr']);

        // 减排场景 -clone field : desc
        $reduction_data = CalculateReportModel::getReduction($id)->toArray();
        $templateProcessor->cloneRowAndSetValues('desc', $reduction_data);

        // 附录1：活动数据汇总表 -clone field : active_department
        $templateProcessor->cloneRowAndSetValues('active_department', $range_data['range_arr']);

        // 附录2：排放因子选择表 -clone field : gases_choice
        $templateProcessor->cloneRowAndSetValues('gases_choice', $range_data['range_arr']);

        // 附录3：全球增温潜势值
        $gwp_data = CalculateReportModel::getGases()->toArray();
        $gwp_data_new = [];
        foreach ($gwp_data as $gwp_key => $gwp_value) {
            $gwp_data_new[$gwp_key ]['id'] = $gwp_value['id'];
            $gwp_data_new[$gwp_key ]['gwp_name'] = $gwp_value['name'];
            $gwp_data_new[$gwp_key ]['gwp_choice'] = $gwp_value[$calculate_report_data['en_short_name']];
        }
        $templateProcessor->cloneRowAndSetValues('gwp_choice', $gwp_data_new);
        $templateProcessor->saveAs($filePathNew);
        $data_file['file_path'] = substr($filePathNew, 1);

        return $data_file;
    }

    /**
     * getIsoRange 生成核算报告ISO-WORD
     * 
     * @author wuyinghua
     * @param $organization_calculate_id
	 * @return $data
     */
    public function getIsoRange($organization_calculate_id){
        $emission = Db::table('jy_organization_calculate_detail_emission n')
            ->field('n.id,n.ghg_cate,n.calculate_model_id,n.emission_name,n.emissions,0 + CAST(abs(n.active_value) AS CHAR) active_value,n.emissions,n.active,n.ghg_cate,n.ghg_type,n.active_unit,
            n.active_department,n.active_type,n.active_score,n.factor_score,n.factor_source,gh.name iso_cate_name,
            g.name iso_name,n.active_data,n.factor_type,n.active_value_standard,
            c.molecule,c.gases,c.co2e_factor_value,c.tco2e,c.co2_factor_value,c.co2_quality,c.co2_gwp,c.co2_tco2e,
            c.ch4_factor_value,c.ch4_quality,c.ch4_gwp,c.ch4_tco2e,c.n2o_factor_value,c.n2o_quality,c.n2o_gwp,
            c.n2o_tco2e,c.hfcs_factor_value,c.hfcs_quality,c.hfcs_gwp,c.hfcs_tco2e,c.pfcs_factor_value,
            c.pfcs_quality,c.pfcs_gwp,c.pfcs_tco2e,c.sf6_factor_value,c.sf6_quality,c.sf6_gwp,
            c.sf6_tco2e,c.nf3_factor_value,c.nf3_quality,c.nf3_gwp,c.nf3_tco2e')
            ->leftJoin('jy_ghg_template g', 'g.id = n.iso_type')
            ->leftJoin('jy_ghg_template gh', 'gh.id = n.iso_cate')
            ->leftJoin('jy_organization_calculate_count_gas c', 'c.emi_id = n.id')
            ->where(['n.organization_calculate_id'=>(int)$organization_calculate_id, 'n.is_del'=>ParamModel::IS_DEL_NO])
            ->select()
            ->toArray();

            $emission_one = [];
            $emission_two = [];
            $emission_three = [];
            $emission_four = [];
            $emission_five = [];
            $emission_six = [];
        if ($emission) {
            foreach ($emission as $v) {

                if ($v['iso_cate_name'] == '直接排放或清除') {
                    $emission_one[] = $v;
                };
                if ($v['iso_cate_name'] == '能源间接排放') {
                    $emission_two[] = $v;
                };
                if ($v['iso_cate_name'] == '运输间接排放') {
                    $emission_three[] = $v;
                };
                if ($v['iso_cate_name'] == '外购产品或服务间接排放') {
                    $emission_four[] = $v;
                };
                if ($v['iso_cate_name'] == '供应链下游排放') {
                    $emission_five[] = $v;
                };
                if ($v['iso_cate_name'] == '其他间接排放') {
                    $emission_six[] = $v;
                };
            }
        }

        //单个分类总量
        $emi_one = 0;
        $emi_two = 0;
        $emi_three = 0;
        $emi_four = 0;
        $emi_five = 0;
        $emi_six = 0;

        if ($emission_one) {
            foreach ($emission_one as $v) {
                $emi_one += $v['emissions'];
            }
        }
        if ($emission_two) {
            foreach ($emission_two as $v) {
                $emi_two += $v['emissions'];
            }
        }
        if ($emission_three) {
            foreach ($emission_three as $v) {
                $emi_three += $v['emissions'];
            }
        }
        if ($emission_four) {
            foreach ($emission_four as $v) {
                $emi_four += $v['emissions'];
            }
        }
        if ($emission_five) {
            foreach ($emission_five as $v) {
                $emi_five += $v['emissions'];
            }
        }
        if ($emission_six) {
            foreach ($emission_six as $v) {
                $emi_six += $v['emissions'];
            }
        }

        //总排放量
        $emi_all = $emi_one + $emi_two + $emi_three + $emi_four + $emi_five + $emi_six;
        //单个分类加权平均评分
        $emi_cate_one=0;
        $emi_cate_two=0;
        $emi_cate_three=0;
        $emi_cate_four=0;
        $emi_cate_five=0;
        $emi_cate_six=0;
        $average_score_total = 0;
        $range_arr = [];
        foreach ($emission as $k => $v) {
            $unit_two = Db::table('jy_unit')->field('name')->where('id', $v['active_unit'][2])->find();
            $active_type_name = Config::get('emission.active_type')[$v['active_type'] - 1]['name'];
            $factor_type_name = Config::get('emission.factor_type')[$v['factor_type'] - 1]['name'];
            if($v['molecule']){
                $molecule = Config::get('emission.molecule')[$v['molecule']-1]['name'];
            }else{
                $molecule = "";
            }

            $v['unit_name'] = $unit_two['name'];
            $v['active_type_name'] = $active_type_name;
            $v['factor_type_name'] = $factor_type_name;
            $all_factor_data = $v['co2e_factor_value'] + 
                               $v['co2_factor_value'] + 
                               $v['ch4_factor_value'] + 
                               $v['n2o_factor_value'] + 
                               $v['hfcs_factor_value'] + 
                               $v['pfcs_factor_value'] + 
                               $v['sf6_factor_value'] + 
                               $v['nf3_factor_value'];

            $v['all_factor_data'] = number_format($all_factor_data, 6) . $molecule; // 当前排放源的排放因子名称 + 单位


            $v['score_result'] = $v['factor_score'] * $v['active_score'];// 评分结果：排放因子评分*活动数据评分

            //占排放分类排放量的比例（%)
            $emi_pro = 0;
            //排放分类加权平均评分
            $emi_cate = 0;
            if($v['iso_cate_name'] == '直接排放或清除') {
                $emi_pro = $emi_one ? $v['emissions'] / $emi_one : 0;
                $emi_cate = $v['factor_score'] * $v['active_score'] * $emi_pro;
                $emi_cate_one += $emi_cate;
            }elseif($v['iso_cate_name'] == '能源间接排放') {
                $emi_pro = $emi_two ? $v['emissions'] / $emi_two : 0;
                $emi_cate = $v['factor_score'] * $v['active_score'] * $emi_pro;
                $emi_cate_two += $emi_cate;
            }elseif($v['iso_cate_name'] == '运输间接排放') {
                $emi_pro = $emi_three ? $v['emissions'] / $emi_three : 0;
                $emi_cate = $v['factor_score'] * $v['active_score'] * $emi_pro;
                $emi_cate_three += $emi_cate;
            }elseif($v['iso_cate_name'] == '外购产品或服务间接排放') {
                $emi_pro = $emi_four ? $v['emissions'] / $emi_four : 0;
                $emi_cate = $v['factor_score'] * $v['active_score'] * $emi_pro;
                $emi_cate_four += $emi_cate;
            }elseif($v['iso_cate_name'] == '供应链下游排放') {
                $emi_pro = $emi_five ? $v['emissions'] / $emi_five : 0;
                $emi_cate = $v['factor_score'] * $v['active_score'] * $emi_pro;
                $emi_cate_five += $emi_cate;
            }elseif($v['iso_cate_name'] == '其他间接排放') {
                $emi_pro = $emi_six ? $v['emissions'] / $emi_six : 0;
                $emi_cate = $v['factor_score'] * $v['active_score'] * $emi_pro;
                $emi_cate_six += $emi_cate;
            }
            $emi_all_pro = 0;
            //占总排放量的比例（%）
            $emi_all_pro =  $emi_all ? abs($v['emissions']) / $emi_all : 0;
            $v['emi_pro'] = number_format($emi_all_pro * 100, 4) . '%'; // 占总排放量的比例（%）
            $v['average_score'] = number_format($v['factor_score'] * $v['active_score'] * $emi_all_pro, 4); // 整体排放加权平均评分
            $average_score_total += $v['average_score']; // 整体排放加权平均评分合计
            $v['gases_choice'] = $v['gases']; // 排放温室气体种类，用于附录2：排放因子选择表
            $range_arr[] = $v;
        }

        //直接排放
        $direct_total = 0;
        //间接排放
        $indirect_total = 0;
        //运输间接排放
        $one_total = 0;
        //能源间接排放总量
        $two_total = 0;
        //运输间接排放总量
        $three_total = 0;
        //外购产品或服务间接排放排放总量
        $four_total = 0;
        //供应链下游排放排放总量
        $five_total = 0;
        //其他间接排放排放总量
        $six_total = 0;

        //混合气体
        $co2e_one = 0;
        $co2e_two = 0;
        $co2e_three = 0;
        $co2e_four = 0;
        $co2e_five = 0;
        $co2e_six = 0;
        //二氧化碳
        $co2_one = 0;
        $co2_two = 0;
        $co2_three = 0;
        $co2_four = 0;
        $co2_five = 0;
        $co2_six = 0;

        //ch4
        $ch4_one = 0;
        $ch4_two = 0;
        $ch4_three = 0;
        $ch4_four = 0;
        $ch4_five = 0;
        $ch4_six = 0;

        //n2o
        $n2o_one = 0;
        $n2o_two = 0;
        $n2o_three = 0;
        $n2o_four = 0;
        $n2o_five = 0;
        $n2o_six = 0;

        //hfcs
        $hfcs_one = 0;
        $hfcs_two = 0;
        $hfcs_three = 0;
        $hfcs_four = 0;
        $hfcs_five = 0;
        $hfcs_six = 0;

        //pfcs
        $pfcs_one = 0;
        $pfcs_two = 0;
        $pfcs_three = 0;
        $pfcs_four = 0;
        $pfcs_five = 0;
        $pfcs_six = 0;

        //sf6
        $sf6_one = 0;
        $sf6_two = 0;
        $sf6_three = 0;
        $sf6_four = 0;
        $sf6_five = 0;
        $sf6_six = 0;

        //nf3
        $nf3_one = 0;
        $nf3_two = 0;
        $nf3_three = 0;
        $nf3_four = 0;
        $nf3_five = 0;
        $nf3_six = 0;

        if ($emission_one) {
            foreach ($emission_one as $k => $v) {
                    $co2e_one += $v['co2e_factor_value'] * $v['active_value_standard'];
                    $co2_one += $v['co2_factor_value'] * $v['active_value_standard'] * $v['co2_gwp'];
                    $ch4_one += $v['ch4_factor_value'] * $v['active_value_standard'] * $v['ch4_gwp'];
                    $n2o_one += $v['n2o_factor_value'] * $v['active_value_standard'] * $v['n2o_gwp'];
                    $hfcs_one += $v['hfcs_factor_value'] * $v['active_value_standard'] * $v['hfcs_gwp'];
                    $pfcs_one += $v['pfcs_factor_value'] * $v['active_value_standard'] * $v['pfcs_gwp'];
                    $sf6_one += $v['sf6_factor_value'] * $v['active_value_standard'] * $v['sf6_gwp'];
                    $nf3_one += $v['nf3_factor_value'] * $v['active_value_standard'] * $v['nf3_gwp'];
            }
        }
        if ($emission_two) {
            foreach ($emission_two as $v) {
                $co2e_two += $v['co2e_factor_value'] * $v['active_value_standard'];
                $co2_two += $v['co2_factor_value'] * $v['active_value_standard'] * $v['co2_gwp'];
                $ch4_two += $v['ch4_factor_value'] * $v['active_value_standard'] * $v['ch4_gwp'];
                $n2o_two += $v['n2o_factor_value'] * $v['active_value_standard'] * $v['n2o_gwp'];
                $hfcs_two += $v['hfcs_factor_value'] * $v['active_value_standard'] * $v['hfcs_gwp'];
                $pfcs_two += $v['pfcs_factor_value'] * $v['active_value_standard'] * $v['pfcs_gwp'];
                $sf6_two += $v['sf6_factor_value'] * $v['active_value_standard'] * $v['sf6_gwp'];
                $nf3_two += $v['nf3_factor_value'] * $v['active_value_standard'] * $v['nf3_gwp'];
            }
        }
        if ($emission_three) {
            foreach ($emission_three as $v) {
                $co2e_three += $v['co2e_factor_value'] * $v['active_value_standard'];
                $co2_three += $v['co2_factor_value'] * $v['active_value_standard'] * $v['co2_gwp'];
                $ch4_three += $v['ch4_factor_value'] * $v['active_value_standard'] * $v['ch4_gwp'];
                $n2o_three += $v['n2o_factor_value'] * $v['active_value_standard'] * $v['n2o_gwp'];
                $hfcs_three += $v['hfcs_factor_value'] * $v['active_value_standard'] * $v['hfcs_gwp'];
                $pfcs_three += $v['pfcs_factor_value'] * $v['active_value_standard'] * $v['pfcs_gwp'];
                $sf6_three += $v['sf6_factor_value'] * $v['active_value_standard'] * $v['sf6_gwp'];
                $nf3_three += $v['nf3_factor_value'] * $v['active_value_standard'] * $v['nf3_gwp'];
            }
        }
        if ($emission_four) {
            foreach ($emission_four as $v) {
                $co2e_four += $v['co2e_factor_value'] * $v['active_value_standard'];
                $co2_four += $v['co2_factor_value'] * $v['active_value_standard'] * $v['co2_gwp'];
                $ch4_four += $v['ch4_factor_value'] * $v['active_value_standard'] * $v['ch4_gwp'];
                $n2o_four += $v['n2o_factor_value'] * $v['active_value_standard'] * $v['n2o_gwp'];
                $hfcs_four += $v['hfcs_factor_value'] * $v['active_value_standard'] * $v['hfcs_gwp'];
                $pfcs_four += $v['pfcs_factor_value'] * $v['active_value_standard'] * $v['pfcs_gwp'];
                $sf6_four += $v['sf6_factor_value'] * $v['active_value_standard'] * $v['sf6_gwp'];
                $nf3_four += $v['nf3_factor_value'] * $v['active_value_standard'] * $v['nf3_gwp'];
            }
        }
        if ($emission_five) {
            foreach ($emission_five as $v) {
                $co2e_five += $v['co2e_factor_value'] * $v['active_value_standard'];
                $co2_five += $v['co2_factor_value'] * $v['active_value_standard'] * $v['co2_gwp'];
                $ch4_five += $v['ch4_factor_value'] * $v['active_value_standard'] * $v['ch4_gwp'];
                $n2o_five += $v['n2o_factor_value'] * $v['active_value_standard'] * $v['n2o_gwp'];
                $hfcs_five += $v['hfcs_factor_value'] * $v['active_value_standard'] * $v['hfcs_gwp'];
                $pfcs_five += $v['pfcs_factor_value'] * $v['active_value_standard'] * $v['pfcs_gwp'];
                $sf6_five += $v['sf6_factor_value'] * $v['active_value_standard'] * $v['sf6_gwp'];
                $nf3_five += $v['nf3_factor_value'] * $v['active_value_standard'] * $v['nf3_gwp'];
            }
        }
        if ($emission_six) {
            foreach ($emission_six as $v) {
                $co2e_six += $v['co2e_factor_value'] * $v['active_value_standard'];
                $co2_six += $v['co2_factor_value'] * $v['active_value_standard'] * $v['co2_gwp'];
                $ch4_six += $v['ch4_factor_value'] * $v['active_value_standard'] * $v['ch4_gwp'];
                $n2o_six += $v['n2o_factor_value'] * $v['active_value_standard'] * $v['n2o_gwp'];
                $hfcs_six += $v['hfcs_factor_value'] * $v['active_value_standard'] * $v['hfcs_gwp'];
                $pfcs_six += $v['pfcs_factor_value'] * $v['active_value_standard'] * $v['pfcs_gwp'];
                $sf6_six += $v['sf6_factor_value'] * $v['active_value_standard'] * $v['sf6_gwp'];
                $nf3_six += $v['nf3_factor_value'] * $v['active_value_standard'] * $v['nf3_gwp'];
            }
        }

        //直接排放或清除总量
        $one_total =  $co2_one + $ch4_one + $n2o_one + $hfcs_one + $pfcs_one + $sf6_one + $nf3_one + $co2e_one;
        //能源间接排放总量
        $two_total =  $co2_two + $ch4_two + $n2o_two + $hfcs_two + $pfcs_two + $sf6_two + $nf3_two + $co2e_two;
        //运输间接总量
        $three_total = $co2_three + $ch4_three + $n2o_three + $hfcs_three + $pfcs_three + $sf6_three + $nf3_three + $co2e_three;
        //外购产品或服务间接排放总量
        $four_total = $co2_four + $ch4_four + $n2o_four + $hfcs_four + $pfcs_four + $sf6_four + $nf3_four + $co2e_four;
        //供应链下游间接排放总量
        $five_total = $co2_five + $ch4_five + $n2o_five + $hfcs_five + $pfcs_five + $sf6_five + $nf3_five + $co2e_five;
        //其他间接排放总量
        $six_total = $co2_six + $ch4_six + $n2o_six + $hfcs_six + $pfcs_six + $sf6_six + $nf3_six + $co2e_six;
        //直接排放
        $direct_total = $one_total;
        //间接排放
        $indirect_total = $two_total + $three_total;
        //排放总量
        $direct_all = $one_total + $two_total + $three_total + $four_total + $five_total + $six_total;
        // CO₂排放总量
        $co2_total = $co2_one + $co2_two + $co2_three + $co2_four + $co2_five + $co2_six;
        // CH₄排放总量
        $ch4_total = $ch4_one + $ch4_two + $ch4_three + $ch4_four + $ch4_five + $ch4_six;
        // N₂O排放总量
        $n2o_total = $n2o_one + $n2o_two + $n2o_three + $n2o_four + $n2o_five + $n2o_six;
        // HFCs排放总量
        $hfcs_total= $hfcs_one + $hfcs_two + $hfcs_three + $hfcs_four + $hfcs_five + $hfcs_six;
        // PFCs排放总量
        $pfcs_total = $pfcs_one + $pfcs_two + $pfcs_three + $pfcs_four + $pfcs_five + $pfcs_six;
        // SF₆排放总量
        $sf6_total= $sf6_one + $sf6_two + $sf6_three + $sf6_four + $sf6_five + $sf6_six;
        // NF₃排放总量
        $nf3_total= $nf3_one + $nf3_two + $nf3_three + $nf3_four + $nf3_five + $nf3_six;
        // 混合排放总量
        $co2e_total= $co2e_one + $co2e_two + $co2e_three + $co2e_four + $co2e_five + $co2e_six;

        // CO₂排放总量排放占比
        $co2_total_per = $direct_all ? 100 * $co2_total/$direct_all : 0;
        // CH₄排放总量排放占比
        $ch4_total_per = $direct_all ? 100 * $ch4_total/$direct_all : 0;
        // N₂O排放总量排放占比
        $n2o_total_per = $direct_all ? 100 * $n2o_total/$direct_all : 0;
        // HFCs排放总量排放占比
        $hfcs_total_per = $direct_all ? 100 * $hfcs_total/$direct_all : 0;
        // PFCs排放总量排放占比
        $pfcs_total_per = $direct_all ? 100 * $pfcs_total/$direct_all : 0;
        // SF₆排放总量排放占比
        $sf6_total_per = $direct_all ? 100 * $sf6_total/$direct_all : 0;
        // NF₃排放总量排放占比
        $nf3_total_per = $direct_all ? 100 * $nf3_total/$direct_all : 0;
        // 混合排放总量排放占比
        $co2e_total_per = $direct_all ? 100 * $co2e_total/$direct_all : 0;
        // 加权平均评分总计
        $emi_cate_all = $emi_cate_one + $emi_cate_two + $emi_cate_three + $emi_cate_four + $emi_cate_five + $emi_cate_six;

        $one_total_per = $direct_all ? 100 * $one_total/$direct_all : 0; // 范围一排放量占比
        $two_total_per = $direct_all ? 100 * $two_total/$direct_all : 0; // 范围二排放量占比
        $three_total_per = $direct_all ? 100 * $three_total/$direct_all : 0; // 范围三排放量占比
        $four_total_per = $direct_all ? 100 * $four_total/$direct_all : 0; // 范围三排放量占比
        $five_total_per = $direct_all ? 100 * $five_total/$direct_all : 0; // 范围三排放量占比
        $six_total_per = $direct_all ? 100 * $six_total/$direct_all : 0; // 范围三排放量占比
        $max_total = max([$one_total, $two_total, $three_total, $four_total, $five_total, $six_total]);
        if ($max_total == $one_total) {
            $max_total_name = '直接排放或清除';
        } elseif ($max_total == $two_total) {
            $max_total_name = '能源间接排放';
        } elseif ($max_total == $three_total) {
            $max_total_name = '运输间接排放';
        } elseif ($max_total == $four_total) {
            $max_total_name = '外购产品或服务间接排放';
        } elseif ($max_total == $five_total) {
            $max_total_name = '供应链下游排放';
        } else {
            $max_total_name = '其他间接排放';
        }

        // 排放量最大的温室气体
        $max_gas = max([$co2_total, $ch4_total, $n2o_total, $hfcs_total, $pfcs_total, $sf6_total, $nf3_total, $co2e_total]);
        if ($max_gas == $co2_total) {
            $max_gas_name = 'CO₂';
            $max_gas_per = $direct_all ? 100 * $co2_total/$direct_all : 0;

        } elseif ($max_gas == $ch4_total) {
            $max_gas_name = 'CH₄';
            $max_gas_per = $direct_all ? 100 * $ch4_total/$direct_all : 0;

        } elseif ($max_gas == $n2o_total) {
            $max_gas_name = 'N₂O';
            $max_gas_per = $direct_all ? 100 * $n2o_total/$direct_all : 0;

        } elseif ($max_gas == $hfcs_total) {
            $max_gas_name = 'HFCs';
            $max_gas_per = $direct_all ? 100 * $hfcs_total/$direct_all : 0;

        } elseif ($max_gas == $pfcs_total) {
            $max_gas_name = 'PFCs';
            $max_gas_per = $direct_all ? 100 * $pfcs_total/$direct_all : 0;

        } elseif ($max_gas == $sf6_total) {
            $max_gas_name = 'SF₆';
            $max_gas_per = $direct_all ? 100 * $sf6_total/$direct_all : 0;

        } elseif ($max_gas == $nf3_total) {
            $max_gas_name = 'NF₃';
            $max_gas_per = $direct_all ? 100 * $nf3_total/$direct_all : 0;

        } else {
            $max_gas_name = '混合';
            $max_gas_per = $direct_all ? 100 * $co2e_total/$direct_all : 0;

        }

        // 不确定性分析，数据质量等级及等级说明
        $grade_all = '';
        $grade_all_des = '';
        if($emi_cate_all >= 0 && $emi_cate_all < 7){
            $grade_all = '6级';
            $grade_all_des = '不确定性极高，数据品质极差';

        } elseif ($emi_cate_all >= 7 && $emi_cate_all < 13){
            $grade_all ='5级';
            $grade_all_des = '不确定性高，数据品质差';

        } elseif ($emi_cate_all >= 13 && $emi_cate_all < 19){
            $grade_all = '4级';
            $grade_all_des = '不确定性略高，数据品质较差';

        } elseif ($emi_cate_all >= 19 && $emi_cate_all < 25){
            $grade_all ='3级';
            $grade_all_des = '不确定性偏高，数据品质不佳';

        } elseif ($emi_cate_all >= 25 && $emi_cate_all < 31){
            $grade_all = '2级';
            $grade_all_des = '不确定性低，数据品质佳';

        // } elseif ($emi_cate_all >= 31 && $emi_cate_all <= 36){ TODO 由于加权平均评分总计会大于36，先放开
        } elseif ($emi_cate_all >= 31){
            $grade_all = '1级';
            $grade_all_des = '不确定性极低，数据品质极佳';

        }

        $data['range_arr'] = $range_arr;
        $data['direct_all'] = $direct_all;  // 排放总量
        $data['one_total'] = $one_total; // 直接排放或清除总量
        $data['two_total'] = $two_total; // 能源间接排放总量
        $data['three_total'] = $three_total; // 运输间接排放总量
        $data['four_total'] = $four_total; // 外购产品或服务间接排放总量
        $data['five_total'] = $five_total; // 供应链下游排放总量
        $data['six_total'] = $six_total; // 其他间接排放总量
        $data['one_total_per'] = number_format($one_total_per, 4); // 直接排放或清除排放量占比
        $data['two_total_per'] = number_format($two_total_per, 4); // 能源间接排放排放量占比
        $data['three_total_per'] = number_format($three_total_per, 4); // 运输间接排放排放量占比
        $data['four_total_per'] = number_format($four_total_per, 4); // 外购产品或服务间接排放排放量占比
        $data['five_total_per'] = number_format($five_total_per, 4); // 供应链下游排放排放量占比
        $data['six_total_per'] = number_format($six_total_per, 4); // 其他间接排放排放量占比
        $data['max_total_name'] = $max_total_name; // 最大的占比名称，例：直接排放或清除

        $data['co2_total'] = $co2_total; // CO₂排放总量
        $data['ch4_total'] = $ch4_total; // CH₄排放总量
        $data['n2o_total'] = $n2o_total; // N₂O排放总量
        $data['hfcs_total'] = $hfcs_total; // HFCs排放总量
        $data['pfcs_total'] = $pfcs_total; // PFCs排放总量
        $data['sf6_total'] = $sf6_total; // SF₆排放总量
        $data['nf3_total'] = $nf3_total; // NF₃排放总量
        $data['co2e_total'] = $co2e_total; // 混合排放总量

        $data['co2_total_per'] = number_format($co2_total_per, 4); // CO₂排放总量排放占比
        $data['ch4_total_per'] = number_format($ch4_total_per, 4); // CH₄排放总量排放占比
        $data['n2o_total_per'] = number_format($n2o_total_per, 4); // N₂O排放总量排放占比
        $data['hfcs_total_per'] = number_format($hfcs_total_per, 4); // HFCs排放总量排放占比
        $data['pfcs_total_per'] = number_format($pfcs_total_per, 4); // PFCs排放总量排放占比
        $data['sf6_total_per'] = number_format($sf6_total_per, 4); // SF₆排放总量排放占比
        $data['nf3_total_per'] = number_format($nf3_total_per, 4); // NF₃排放总量排放占比
        $data['co2e_total_per'] = number_format($co2e_total_per, 4); // 混合排放总量排放占比

        $data['max_gas_name'] = $max_gas_name; // 排放量最大的温室气体名称
        $data['max_gas_per'] = number_format($max_gas_per, 4); // 排放量最大的温室气体百分比
        $data['max_gas'] = $max_gas; // 占比最高的温室气体种类的排放量

        $data['emi_cate_all'] = $emi_cate_all;// 总加权平均分
        $data['grade_all'] = $grade_all;// 加权平均评分总计
        $data['grade_all_des'] = $grade_all_des;// 加权平均评分等级和说明
        $data['average_score_total'] = $average_score_total;// 整体排放加权平均评分合计

        return $data;
    }

    /**
     * validateForm 验证
     * 
     * @author wuyinghua
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(OrgCalculateReportValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}
