<?php
namespace app\controller\home;

use app\BaseController;
use app\model\home\IndexModel;
use app\model\system\UserModel;
use app\model\product\ApprovalModel;
use app\model\organization\DataManagementModel;
use app\model\supply\CustomerProductModel;
use app\model\attestation\AttestationModel;
use think\facade\Db;

class Index extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 首页列表
     * 
     * @author wuyinghua
     * @param $request
	 * @return void
     */
    public function index() {
        // 常用功能显示数量
        $num_configs = 14;
        // 最新资讯显示数量
        $num_news = 10;
        // 通知公告显示数量
        $num_notices = 10;
        $list = [];
        $data_redis = $this->request->middleware('data_redis');
        $list['list_configs'] = IndexModel::getConfigs($num_configs, $data_redis['userid'])->toArray();

        foreach ($list['list_configs'] as $key => $value) {
            $list_menu = IndexModel::getMenus($value['pid']);
            $list['list_configs'][$key]['p_title'] = $list_menu['title'];
            $list['list_configs'][$key]['path'] = $list_menu['path'] . '/' . $value['path'];
        }

        $user_data = UserModel::seeUser($data_redis['userid']);


        $list['role'] = '';
        $role = UserModel::getRole($user_data['role_id']);
        if ($role) {
            $list['role'] = $role['title'];
        }

        $list['list_todo'] = [];
        // 产品核算审批
        $product_approvals = ApprovalModel::getAllApproval($data_redis['main_organization_id'])->toArray();
        $product_approvals_arr = [];
        if (count($product_approvals) > 0) {
            $product_approvals_arr = array_slice($product_approvals, 0, 5);
            foreach ($product_approvals_arr as $key => $value) {
                $new_arr['id'] = $value['id'];
                $new_arr['title'] = $value['product_name'] . '产品核算数据审批';
                $new_arr['create_time'] = $value['modify_time'];
                $new_arr['username'] = $value['username'];
                $new_arr['type'] = 'product_approval';
    
                array_push($list['list_todo'], $new_arr);
            }
        }

        // 组织核算审批
        $organization_approvals = DataManagementModel::getAllOrganizationCalculate($data_redis['main_organization_id'])->toArray();
        $organization_approvals_arr = [];
        if (count($organization_approvals) > 0) {
            $organization_approvals_arr = array_slice($organization_approvals, 0, 5);
            foreach ($organization_approvals_arr as $key => $value) {
                $new_arr['id'] = $value['id'];
                $new_arr['title'] = $value['calculate_name'] . $value['calculate_month'] . '月数据审批';
                $new_arr['create_time'] = $value['modify_time'];
                $new_arr['username'] = $value['username'];
                $new_arr['type'] = 'organization_approval';
    
                array_push($list['list_todo'], $new_arr);
            }
        }

        // 供应链货品报送
        $supply_fill_approvals = CustomerProductModel::getNotVerifyListBySupplerId($data_redis['main_organization_id'], '', '');
        $supply_fill_approvals_arr = [];
        if (count($supply_fill_approvals) > 0) {
            $supply_fill_approvals_arr = array_slice($supply_fill_approvals, 0, 5);
            foreach ($supply_fill_approvals_arr as $key => $value) {
                $new_arr['id'] = $value['customer_product_id'];
                $new_arr['title'] = $value['title'] . $value['product_name'] . '信息填报';
                $new_arr['create_time'] = $value['modify_time'];
                $new_arr['username'] = $value['username'];
                $new_arr['type'] = 'supply_fill_approval';
    
                array_push($list['list_todo'], $new_arr);
            }
        }

        // 供应链报送审批
        $supply_approvals = CustomerProductModel::getAllAuditByCustomer($data_redis['main_organization_id'])->toArray();
        $supply_approvals_arr = [];
        if (count($supply_approvals) > 0) {
            $supply_approvals_arr = array_slice($supply_approvals, 0, 5);
            foreach ($supply_approvals_arr as $key => $value) {
                $new_arr['id'] = $value['id'];
                $new_arr['title'] = $value['title'] . $value['product_name'] . '报送数据审批';
                $new_arr['create_time'] = $value['modify_time'];
                $new_arr['username'] = $value['username'];
                $new_arr['type'] = 'supply_approval';
    
                array_push($list['list_todo'], $new_arr); 
            }
        }

        // 认证审批
        $certification_approvals = AttestationModel::getAllApproval($data_redis['main_organization_id'])->toArray();
        $certification_approvals_arr = [];
        if (count($certification_approvals) > 0) {
            $certification_approvals_arr = array_slice($certification_approvals, 0, 5);
            foreach ($certification_approvals_arr as $key => $value) {
                $new_arr['id'] = $value['id'];
                if ($value['status'] == 2) {
                    $new_arr['title'] = '订单确定付款: 申请已通过，需前往认证';
                } elseif ($value['status'] == 5) {
                    $new_arr['title'] = '驳回: ' . $value['product_name'] . '认证已驳回，需重新提交';
                } else {
                    $new_arr['title'] = '证书上传: ' . $value['product_name']  . '证书已派发，可前往下载';
                }
    
                $value['username'] = ''; 
                if ($value['data_from'] == 1) {
                    $find_name = Db::table('jy_user')
                        ->field('username')
                        ->where('id', $value['modify_by'])
                        ->find();
                    if($find_name){
                        $value['username'] = $find_name['username'];
                    }
                } elseif ($value['data_from'] == 2) {
                    $find_name = Db::table('jy_mange_user')
                        ->field('username')
                        ->where('id', $value['modify_by'])
                        ->find();
                    if($find_name){
                        $value['username'] = $find_name['username'];
                    }
                } elseif ($value['data_from'] == 3) {
                    $find_name = Db::table('jy_cooperate_user')
                        ->field('username')
                        ->where('id', $value['modify_by'])
                        ->find();
                    if($find_name){
                        $value['username'] = $find_name['username'];
                    }
                }
    
                $new_arr['create_time'] = $value['modify_time'];
                $new_arr['username'] = $value['username'];
                $new_arr['type'] = 'certification_approval';
    
                array_push($list['list_todo'], $new_arr);
            }
        }

        $list['list_notices'] = IndexModel::getNotices($num_notices)->toArray();
        $list['list_news'] = IndexModel::getNews($num_news)->toArray();
        foreach ($list['list_news'] as &$value) {
            $value['content'] = strip_tags($value['content']);
            $value['content'] = mb_substr($value['content'], 0, 30);
        }

        $list['username'] = $user_data['username'];
        $list['organization_name'] = $user_data['organization_name'];
        $list['last_time'] = $user_data['last_time'];
        $list['todo_count'] = count($product_approvals) + count($organization_approvals) + count($supply_fill_approvals) + count($supply_approvals) + count($certification_approvals);

        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }
}
