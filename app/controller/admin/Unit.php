<?php
declare (strict_types = 1);

namespace app\controller\admin;

use app\BaseController;
use app\validate\UnitValidate;
use think\exception\ValidateException;
use app\model\admin\UnitModel;
use app\model\system\OperationModel;
use think\Request;

class Unit extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 单位列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function index() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        // 过滤条件：单位分类、单位名称
        $filters = [
            'filter_unit_type' => isset($_GET['filterUnitType']) ? $_GET['filterUnitType'] : '',
            'filter_unit_name' => isset($_GET['filterUnitName']) ? $_GET['filterUnitName'] : ''
        ];

        $list = UnitModel::getUnits($page_size, $page_index, $filters)->toArray();
        $unit_types = UnitModel::getUnitType()->toArray();

        foreach ($list['data'] as $key => $value) {
            $unit = UnitModel::findUnit($value['id']);
            $list['data'][$key]['base_unit'] = $unit['base_unit'];
        }

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['unit_types'] = $unit_types;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * see 单位数组
     * 
     * @author wuyinghua
	 * @return void
     */
    public function see() {
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $list = UnitModel::getUnit($type)->toArray();

        $array = array();
        $dictionaries = array();

        foreach ($list as $key => $value) {
            if (!in_array($value['type_id'], $array)) {
                array_push($array, $value['type_id']);
                $dictionaries[$key]['id'] = $value['type_id'];
                $dictionaries[$key]['unit_type'] = $value['type'];
                $dictionaries[$key]['units'] = $this->initDatas($list, $value['type_id']);
            }
        }

        $data['code'] = 200;
        $data['data']['list'] = array_values($dictionaries);

        return json($data);
    }

    /**
     * add 添加单位
     * 
     * @author wuyinghua
	 * @return void
     */
    public function add(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['name'] = $params_payload_arr['name'];
            $data['type_id'] = $params_payload_arr['type_id'];
            $data['is_base'] = 2; // 基准单位不可新增，默认为非基准单位
            $data['conversion_ratio'] = $params_payload_arr['conversion_ratio'];
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '单位管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = $data['create_time'];
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '新建单位：' . $data['name'];

            OperationModel::addOperation($data_log);
            $add = UnitModel::addUnit($data);

            if ($add) {
                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>404, 'message'=>"添加失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * edit 修改单位
     * 
     * @author wuyinghua
	 * @return void
     */
    public function edit(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $data['conversion_ratio'] = $params_payload_arr['conversion_ratio'];
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');
            $unit_info = UnitModel::findUnit($data['id']);

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '单位管理';
            $data_log['type'] = '系统操作';
            $data_log['time'] = $data['modify_time'];
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑单位：' . $unit_info['name'];

            OperationModel::addOperation($data_log);
            $edit = UnitModel::editUnit($data);

            if ($edit) {
                return json(['code'=>200, 'message'=>"修改成功"]);
            } else {
                return json(['code'=>404, 'message'=>"修改失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * del 删除单位
     * 
     * @author wuyinghua
	 * @return void
     */
    public function del(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $id = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($id == '' || $id == null || $id == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $unit_info = UnitModel::findUnit($id);

        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '单位管理';
        $data_log['type'] = '系统操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $request->pathinfo();
        $data_log['log'] = '删除单位：' . $unit_info['name'];

        OperationModel::addOperation($data_log);
        $del = UnitModel::delUnit($id);

        if($del){
            return json(['code'=>200, 'message'=>"删除成功"]);
        }else{
            return json(['code'=>404, 'message'=>"删除失败"]);
        }
    }

    /**
     * find 查看单位详情
     * 
     * @author wuyinghua
	 * @return void
     */
    public function find() {
        $id = $_GET['id'];
        $list = UnitModel::findUnit($id);

        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }

    /**
     * initDatas
     * 
     * @author wuyinghua
     * @param $list
     * @param $type_id
	 * @return $array
     */
    private function initDatas($list, $type_id = '') {
        foreach ($list as $value) {
            if ($type_id == $value['type_id']) {
                $array[] = [
                    'unit_id'             => $value['id'],
                    'name'                => $value['name'],
                    'conversion_ratio'    => $value['conversion_ratio']
                ];
            }
        }

        return $array;
    }

	//======================================================================
	// PROTECTED FUNCTIONS
	//======================================================================

    /**
     * validateForm 验证
     * 
     * @author wuyinghua
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(UnitValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}
