<?php
namespace app\controller\product;

use app\BaseController;
use app\validate\ProductDataValidate;
use app\validate\ProductDataTransportValidate;
use think\exception\ValidateException;
use app\model\product\ProductCalculateModel;
use app\model\product\ProductDataModel;
use app\model\product\ProductMaterialModel;
use app\model\data\ParamModel;
use app\model\product\FactorModel;
use app\model\supply\CustomerProductModel;
use app\model\system\OperationModel;
use app\model\admin\UnitModel;
use think\Request;

/**
 * ProductData
 */
class ProductData extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 产品核算数据管理列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function index(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $main_id = $data_redis['main_organization_id'];
        $id = $_GET['id']; //产品核算id
        $product_id = $_GET['product_id'];
        $data_stage = $_GET['data_stage'];
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';
        $transport_ids = [8,12,14,16,18];

        // 数据管理（结果预览）
        if ($data_stage == '0') {
            $list = ProductCalculateModel::getCalculate($id);
            $list_stage = explode(',', $list['stage']);
            $list_all = ProductCalculateModel::seeCalculateData($id);

            $array = array();
            foreach ($list_stage as $list_key => $list_value) {
                $name = ProductCalculateModel::getStageName($list_key + 1);
                $array[$list_key]['data_stage'] = $name['name'];
                $array[$list_key]['total_emission'] = 0;
                $array[$list_key]['datas'] = [];
                foreach ($list_all as $key => $value) {
                    if ($value['data_stage'] == $list_value) {
                        if ($value['factor_molecule'] != NULL && $value['factor_denominator'] != NULL) {
                            $value['match_unit'] = $value['factor_molecule'] . '/' . $value['factor_denominator'];
                        }

                        if (in_array($value['category_id'], $transport_ids)) {
                            $value['unit_str'] = $value['unit_str'] . 'km';
                        }

                        $array[$list_key]['total_emission'] += $value['emissions'];
                        $array[$list_key]['datas'][] = $value;
                    }
                }
            }

            $data['code'] = 200;
            $data['data']['list'] = $array;
            $data['data']['total_emissions'] = $list['emissions'];
        } else {
            $list = ProductDataModel::getDataManagements($id, $product_id, $data_stage, $page_size, $page_index)->toArray();

            foreach ($list['data'] as $key => $value) {
                $list['data'][$key]['match_name'] = $value['factor_title'];
                $list['data'][$key]['match_unit'] = $value['factor_molecule'] . '/' . $value['factor_denominator'];

                $category = ProductDataModel::getStageName($value['category']);
                $list['data'][$key]['category_id'] = $value['category'];
                $list['data'][$key]['category'] = $category['name'];

                if (in_array($value['category'], $transport_ids)) {
                    $list['data'][$key]['unit_str'] = $value['unit_str'] . 'km';
                }

                $array = array();

                if (!empty($value['materials'])) {
                    $materials = explode(',', $value['materials']);
                    foreach ($materials as $materials_key => $materials_value) {
                        $array[$materials_key]['name'] = $materials_value;
                    }

                    $numbers = explode(',', $value['numbers']);
                    foreach ($numbers as $numbers_key => $numbers_value) {
                        $array[$numbers_key]['number'] = $numbers_value;
                    }

                    $units = explode(',', $value['units']);
                    foreach ($units as $units_key => $units_value) {
                        $array[$units_key]['unit'] = $units_value;
                    }

                    $list['data'][$key]['materials'] = $array;
                }
            }

            // 获取阶段下的类别
            $data_categories = ProductDataModel::getCategoryName($data_stage)->toArray();

            // 获取阶段下的材料
            $data_materials = ProductDataModel::getDataManagementByStage($data_stage, $id)->toArray();
            $data_product = ProductDataModel::seeProduct($product_id);

            // 剔除材料空值
            foreach ($data_materials as $key => $value) {
                if (empty($value['material']) || in_array($value['material'], $data_materials)) {
                    unset($data_materials[$key]);
                }
            }

            array_unshift($data_materials, $data_product);

            // 获取供应商
            $data_suppliers = ProductMaterialModel::getSuppliers($main_id)->toArray();

            $list_stage = ProductCalculateModel::getCalculate($id);
            $stage_list = explode(',', $list_stage['stage']);

            // 获取阶段tab
            $data_stages = array();
            foreach ($stage_list as $key => $value) {
                $name = ProductCalculateModel::getStageName($value);
                $data_stages[$value]['id'] = $value;
                $data_stages[$value]['name'] = $name['name'];
            }

            // 按升序排列阶段
            ksort($data_stages);

            array_push($data_stages, ['id' => '0', 'name' => '结果预览']);

            $data['code'] = 200;
            $data['data']['list'] = $list['data'];
            $data['data']['data_stages'] = $data_stages;
            $data['data']['data_categories'] = $data_categories;
            $data['data']['data_materials'] = $data_materials;
            $data['data']['data_suppliers'] = $data_suppliers;
            $data['data']['total'] = $list['total'];
        }

        return json($data);
    }

    /**
     * add 添加数据管理（排放源）
     * 
     * @author wuyinghua
	 * @return void
     */
    public function add(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');

            $data['product_calculate_id'] = $_POST['product_calculate_id'];
            $data['product_id'] = $_POST['product_id'];
            $data['data_stage'] = $_POST['data_stage'];
            $data['category'] = $_POST['category'];
            $data['name'] = $_POST['name'];
            $data['distance'] = empty($_POST['distance']) ? NULL : $_POST['distance'];

            // 排放源为运输时
            if (isset($_POST['materials'])) {
                $material_arr = json_decode($_POST['materials'], true);
                $unit_base = $material_arr[0]['unit'];
                $materials = array();
                $numbers = array();
                $units = array();

                $data['number'] = 0;
                $data['unit'] = $unit_base;
                foreach ($material_arr as $key => $value) {
                    $data['number'] += $value['number'];

                    if (array_search($value['name'], $materials)) {
                        $numbers[array_search($value['name'], $materials)] += $value['number'];
                    } else {
                        $materials[$key + 1] = $value['name'];
                        $numbers[$key + 1] = $value['number'];
                        $units[$key + 1] = $data['unit'];
                    }
                }

                if (!is_null($data['distance'])) {
                    $data['number'] = $data['number'] * $data['distance'];
                }

                $data['materials'] = implode(',', $materials);
                $data['numbers'] = implode(',', $numbers);
                $data['units'] = implode(',', $units);
            } else {
                $data['number'] = empty($_POST['number']) ? NULL : $_POST['number'];
                $data['unit'] = empty($_POST['unit']) ? NULL : $_POST['unit'];
                $data['material'] = empty($_POST['material']) ? NULL : $_POST['material'];
            }

            $data['unit_type'] = empty($_POST['unit_type']) ? NULL : $_POST['unit_type'];
            $data['code'] = empty($_POST['code']) ? NULL : $_POST['code'];
            $data['specs'] = empty($_POST['specs']) ? NULL : $_POST['specs'];
            $data['source'] = empty($_POST['source']) ? ParamModel::SOURCE_TYPE_SELF : $_POST['source'];
            $data['supplier_id'] = empty($_POST['supplier_id']) ? NULL : $_POST['supplier_id'];
            $data['escaping_gas'] = empty($_POST['escaping_gas']) ? NULL : $_POST['escaping_gas'];
            $data['match_type'] = empty($_POST['match_type']) ? 1 : $_POST['match_type'];
            $data['create_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 验证当前核算下，当前阶段，当前排放源（名称 + 编号）是否存在
            if ($this->checkProductData($data['name'], $data['code'], $data['product_calculate_id'], $data['data_stage'])) {
                $add = ProductDataModel::addDataManagement($data);
            } else {
                return json(['code'=>201, 'message'=>"排放源已存在，请重新输入"]);
            }

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '产品碳足迹';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '新建产品核算排放源：' . $data['name'];
            OperationModel::addOperation($data_log);

            if ($add) {
                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>404, 'message'=>"添加失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * edit 修改数据管理（排放源）
     * 
     * @author wuyinghua
	 * @return void
     */
    public function edit(Request $request) {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['product_id'] = $_POST['product_id'];
            $data['product_calculate_id'] = $_POST['product_calculate_id'];
            $data['data_stage'] = $_POST['data_stage'];
            $data['category'] = $_POST['category'];
            $data['name'] = $_POST['name'];
            $data['distance'] = empty($_POST['distance']) ? NULL : $_POST['distance'];

            // 排放源为运输时
            if (isset($_POST['materials'])) {
                $material_arr = json_decode($_POST['materials'], true);
                $unit_base = $material_arr[0]['unit'];
                $materials = array();
                $numbers = array();
                $units = array();

                $data['number'] = 0;
                $data['unit'] = $unit_base;
                foreach ($material_arr as $key => $value) {
                    $data['number'] += $value['number'];

                    if (array_search($value['name'], $materials)) {
                        $numbers[array_search($value['name'], $materials)] += $value['number'];
                    } else {
                        $materials[$key + 1] = $value['name'];
                        $numbers[$key + 1] = $value['number'];
                        $units[$key + 1] = $data['unit'];
                    }
                }
                
                if (!is_null($data['distance'])) {
                    $data['number'] = $data['number'] * $data['distance'];
                }

                $data['materials'] = implode(',', $materials);
                $data['numbers'] = implode(',', $numbers);
                $data['units'] = implode(',', $units);
            } else {
                $data['number'] = empty($_POST['number']) ? NULL : $_POST['number'];
                $data['unit'] = empty($_POST['unit']) ? NULL : $_POST['unit'];
                $data['material'] = empty($_POST['material']) ? NULL : $_POST['material'];
            }

            $data['unit_type'] = empty($_POST['unit_type']) ? NULL : $_POST['unit_type'];
            $data['code'] = empty($_POST['code']) ? NULL : $_POST['code'];
            $data['specs'] = empty($_POST['specs']) ? NULL : $_POST['specs'];
            $data['source'] = empty($_POST['source']) ? ParamModel::SOURCE_TYPE_SELF : $_POST['source'];
            $data['escaping_gas'] = empty($_POST['escaping_gas']) ? NULL : $_POST['escaping_gas'];
            $data['match_type'] = empty($_POST['match_type']) ? ParamModel::MATCH_TYPE_FACTOR : $_POST['match_type'];
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 来源切换为自产时，去除供应商
            if ($data['source'] == ParamModel::SOURCE_TYPE_SELF) {
                $data['supplier_id'] = NULL;
            } else {
                $data['supplier_id'] = empty($_POST['supplier_id']) ? NULL : $_POST['supplier_id'];
            }

            // 变更的时候，清空所选产品
            $is_clear = false;
            $product_data = ProductDataModel::getDataManagement($data['id']);
            if ($data['modify_time'] != $product_data['modify_time']) {
                $is_clear = true;
                $data['match_id'] = NULL;
                $data['factor_title'] = '';
                $data['factor_molecule'] = '';
                $data['factor_denominator'] = '';
                $data['factor_mechanism'] = '';
                $data['factor_year'] = '';
            }

            if ($data['source'] == ParamModel::SOURCE_TYPE_EXTERNAL && ($data['supplier_id'] == NULL || $data['supplier_id'] == '')) {
                return json(['code'=>201, 'message'=>"外采必须选择供应商"]);
            }

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '产品碳足迹';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑产品核算排放源：' . $data['name'];

            // 验证当前核算下，当前阶段，当前排放源（名称 + 编号）是否存在
            if ($this->checkProductData($data['name'], $data['code'], $data['product_calculate_id'], $data['data_stage'], $data['id'])) {
                $edit = ProductDataModel::editDataManagement($data, $is_clear);
            } else {
                return json(['code'=>201, 'message'=>"排放源已存在，请重新输入"]);
            }

            OperationModel::addOperation($data_log);

            if ($edit) {
                return json(['code'=>200, 'message'=>"修改成功"]);
            } else {
                return json(['code'=>404, 'message'=>"修改失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * del 删除数据管理（排放源）
     * 
     * @author wuyinghua
	 * @return void
     */
    public function del(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $id = $_POST['id'];
        $productdata_info = ProductDataModel::getDataManagement($id);

        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '产品碳足迹';
        $data_log['type'] = '功能操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $request->pathinfo();
        $data_log['log'] = '删除数据管理（排放源）：' . $productdata_info['name'];

        OperationModel::addOperation($data_log);
        $del = ProductDataModel::delDataManagement($id);

        if($del){
            return json(['code'=>200, 'message'=>"删除成功"]);
        }else{
            return json(['code'=>404, 'message'=>"删除失败"]);
        }
    }

    /**
     * checkProductData 验证产品数据管理
     * 
     * @author wuyinghua
     * @param $name
     * @param $code
     * @param $product_calculate_id
     * @param $data_stage
     * @param $id
	 * @return void
     */
    public function checkProductData($name, $code, $product_calculate_id, $data_stage, $id = NULL) {
        $list = ProductDataModel::getAllProductDatas($product_calculate_id, $data_stage, $id)->toArray();

        $combine_products = array();
        foreach ($list as $key => $value) {
            $combine_products[$key] = $value['name'] . '-' . $value['code'];
        }

        $combine_product = $name . '-' . $code;

        if (in_array($combine_product, $combine_products)) {
            return false;
        } else {
            return  true;
        }
    }


    /**
     * choice 选择排放因子/核算产品
     * 
     * @author wuyinghua
	 * @return void
     */
    public function choice(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $data['id'] = $_POST['product_data_id']; // 产品数据管理id
        $data['match_id'] = $_POST['id']; // 匹配因子/产品id
        $data['modify_by'] = $data_redis['userid'];

        $data_management = ProductDataModel::getDataManagement($data['id']);
        
        // 匹配类型为因子
        if ($data_management['match_type'] == ParamModel::MATCH_TYPE_FACTOR) {
            $data_factor = FactorModel::seeFactor($data['match_id']);
            $data_factor['old_match_id'] = $data_management['match_id'];
            $data_factor['product_data_id'] = $data['id'];
            $data_factor['modify_by'] = $data_redis['userid'];
            // 快照
            $data['match_id'] = ProductDataModel::addShotFactor($data_factor);
            $data['coefficient'] = $_POST['factor_value'];
            // 数量与因子分母单位不一致时，先判断是否能通过数据字典中单位系数进行换算，如果不能换算则排放量信息展示为空，不纳入计算
            $factor_unit_data = UnitModel::getInfoByName($data_factor['denominator']);
            $data_management_unit_data = UnitModel::findUnit($data_management['unit']);
            if ($factor_unit_data != NULL && $factor_unit_data['type_id'] == $data_management['unit_type']) {
                $data['emissions'] = $_POST['factor_value'] * $data_management['number'] * $data_management_unit_data['conversion_ratio'] / $factor_unit_data['conversion_ratio'];
            } elseif (in_array($data_management['category'], ParamModel::TRANSPORT)) {
                $data['emissions'] = $_POST['factor_value'] * $data_management['number'] * $data_management_unit_data['conversion_ratio'] * 1000;
            } else {
                $data['emissions'] = NULL;
            }

            $data['factor_title'] = $data_factor['title'];
            $data['factor_molecule'] = $data_factor['molecule'];
            $data['factor_denominator'] = $data_factor['denominator'];
            $data['factor_mechanism'] = $data_factor['mechanism'];
            $data['factor_year'] = $data_factor['year'];

        // 匹配类型为产品，来源为自产
        } elseif ($data_management['source'] == ParamModel::SOURCE_TYPE_SELF && $data_management['match_type'] == ParamModel::MATCH_TYPE_PRODUCT) {
            $data_factor = ProductCalculateModel::getCalculate($data['match_id']);
            $data['coefficient'] = $_POST['coefficient'];
            // 数量与因子分母单位不一致时，先判断是否能通过数据字典中单位系数进行换算，如果不能换算则排放量信息展示为空，不纳入计算
            $factor_unit_data = UnitModel::getInfoByName($data_factor['unit_str']);
            if ($factor_unit_data != NULL && $factor_unit_data['type_id'] == $data_management['unit_type']) {
                $data_management_unit_data = UnitModel::findUnit($data_management['unit']);
                $data['emissions'] = $_POST['coefficient'] * $data_management['number'] * $data_management_unit_data['conversion_ratio'] / $factor_unit_data['conversion_ratio'];
            } else {
                $data['emissions'] = NULL;
            }
            $data['factor_title'] = $data_factor['product_name'];
            $data['factor_molecule'] = 'kgCO2e';
            $data['factor_denominator'] = $data_factor['unit_str'];
            $data['factor_mechanism'] = '';
            $data['factor_year'] = '';

        // 匹配类型为产品，来源为外采
        } elseif ($data_management['source'] == ParamModel::SOURCE_TYPE_EXTERNAL && $data_management['match_type'] == ParamModel::MATCH_TYPE_PRODUCT) {
            $data_factor = CustomerProductModel::getDataById($data['match_id']);
            $data['coefficient'] = $_POST['coefficient'];
            // 数量与因子分母单位不一致时，先判断是否能通过数据字典中单位系数进行换算，如果不能换算则排放量信息展示为空，不纳入计算
            $factor_unit_data = UnitModel::getInfoByName($data_factor['unit_str']);
            if ($factor_unit_data != NULL && $factor_unit_data['type_id'] == $data_management['unit_type']) {
                $data_management_unit_data = UnitModel::findUnit($data_management['unit']);
                $data['emissions'] = $_POST['coefficient'] * $data_management['number'] * $data_management_unit_data['conversion_ratio'] / $factor_unit_data['conversion_ratio'];
            } else {
                $data['emissions'] = NULL;
            }
            $data['factor_title'] = $data_factor['product_name'];
            $data['factor_molecule'] = 'kgCO2e';
            $data['factor_denominator'] = $data_factor['unit_str'];
            $data['factor_mechanism'] = '';
            $data['factor_year'] = '';
        }

        $data['product_calculate_id'] = $data_management['product_calculate_id']; // 产品核算id
        $calculate_data = ProductDataModel::getProductCalculatesByMatchType($data['product_calculate_id']);
        $data['number'] = $calculate_data['number']; // 产品核算数量
        $data['modify_by'] = $data_redis['userid'];

        // 选择排放因子/核算产品
        $edit = ProductDataModel::choiceFactor($data);

        if ($edit) {
            return json(['code'=>200, 'message'=>"修改成功"]);
        } else {
            return json(['code'=>404, 'message'=>"修改失败"]);
        }
    }

	//======================================================================
	// PROTECTED FUNCTIONS
	//======================================================================

    /**
     * validateForm 验证
     * 
     * @author wuyinghua
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();
        $transport_ids = [8,12,14,16,18];

        try {

            if (in_array((int)$data['category'], $transport_ids)) {
                validate(ProductDataTransportValidate::class)->check($data);
            } else {
                validate(ProductDataValidate::class)->check($data);
            }

            return true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}