<?php
declare (strict_types = 1);

namespace app\controller\product;

use app\BaseController;
use app\validate\CalculateReportValidate;
use think\exception\ValidateException;
use app\model\product\CalculateReportModel;
use app\model\product\ProductCalculateModel;
use app\model\product\ProductDataModel;
use app\service\ProductCarbonLabelService;
use app\model\system\OperationModel;
use app\model\system\FileModel;
use PhpOffice\PhpWord\TemplateProcessor;
use think\facade\Db;

/**
 * CalculateReport
 */
class CalculateReport extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 核算报告列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function index() {
        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';

        // 过滤条件：产品名称、产品编号、产品规格、产品状态
        $filters = [
            'filter_report_name' => isset($_GET['filterReportName']) ? $_GET['filterReportName'] : '',
            'filter_product_name' => isset($_GET['filterProductName']) ? $_GET['filterProductName'] : '',
            'filter_type' => isset($_GET['filterType']) ? $_GET['filterType'] : '',
            'main_organization_id' => $main_organization_id
        ];

        $list = CalculateReportModel::getCalculateReports($page_size, $page_index, $filters)->toArray();

        // 获取报告类型
        $report_types = CalculateReportModel::getReportTypes()->toArray();

        // 获取核算列表的产品名称-产品编号 + 核算周期
        $products = array();
        $product_ids = array();
        $calculate_products = CalculateReportModel::getCalculateProducts($main_organization_id)->toArray();

        foreach ($calculate_products as $key => $value) {
            if (!in_array($value['product_id'], $product_ids)) {
                array_push($products, ['product_id' => $value['product_id'], 'name' => $value['product_name'] . '-' . $value['product_no']]);
                array_push($product_ids, $value['product_id']);
            }
        }

        $weeks = array();
        if (isset($_GET['product_id']) ) {
            $datas = ProductCalculateModel::getCalculateWeek($_GET['product_id'])->toArray();

            foreach ($datas as $key => $value) {
                $weeks[$key]['product_calculate_id'] = $value['id'];
                $weeks[$key]['name'] = $value['week_start'] . '-' . $value['week_end'];
            }
        }

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['total'] = $list['total'];
        $data['data']['products'] = $products;
        $data['data']['weeks'] = $weeks;
        $data['data']['report_types'] = $report_types;

        return json($data);
    }

    /**
     * weeks 核算周期列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function weeks() {
        $weeks = array();

        if (isset($_GET['product_id']) ) {
            $datas = ProductCalculateModel::getCalculateWeek($_GET['product_id'])->toArray();

            foreach ($datas as $key => $value) {
                $weeks[$key]['product_calculate_id'] = $value['id'];
                $weeks[$key]['count_date'] = $value['week_start'] . '-' . $value['week_end'];
            }
        }

        $data['code'] = 200;
        $data['data']['list'] = $weeks;

        return json($data);
    }

    /**
     * add 添加核算报告
     * 
     * @author wuyinghua
	 * @return void
     */
    public function add() {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $this->request->middleware('data_redis');

            $data['product_id'] = $_POST['product_id'];
            $data['product_calculate_id'] = $_POST['product_calculate_id'];
            $data['main_organization_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
            $data['report_name'] = $_POST['report_name'];
            $data['count_date'] = $_POST['count_date'];
            $data['report_type'] = $_POST['report_type'];
            $data['description'] = isset($_POST['description']) ? $_POST['description'] : '';
            $data['create_by'] = $data_redis['userid'];
            $data['modify_by'] = $data_redis['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '产品碳足迹';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '新建核算报告：' . $data['report_name'];

            Db::startTrans();
            try {
                $add_id = CalculateReportModel::addCalculateReport($data);

                $data_file = $this->createreport($add_id);
                $data_file['main_organization_id'] = $data_redis['main_organization_id'];
                $data_file['create_by'] = $data_redis['userid'];
                $data_file['modify_by'] = $data_redis['userid'];
                $data_file['id'] = guid();
                FileModel::addFile($data_file);

                // 更新核算报告中的文件id
                $data_edit['id'] = $add_id;
                $data_edit['file_id'] = $data_file['id'];
                CalculateReportModel::editCalculateReport($data_edit);
                OperationModel::addOperation($data_log);

                Db::commit();
                return json(['code'=>200, 'message'=>"添加成功"]);
            } catch (\Exception $e) {
                Db::rollback();
                return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * edit 修改核算报告
     * 
     * @author wuyinghua
	 * @return void
     */
    public function edit() {

        if (request()->isPost() && $this->validateForm() === true) {
            $data_redis = $this->request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['product_id'] = $_POST['product_id'];
            $data['product_calculate_id'] = $_POST['product_calculate_id'];
            $data['report_name'] = $_POST['report_name'];
            $data['count_date'] = $_POST['count_date'];
            $data['report_type'] = $_POST['report_type'];
            $data['description'] = isset($_POST['description']) ? $_POST['description'] : '';
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '产品碳足迹';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $this->request->pathinfo();
            $data_log['log'] = '编辑核算报告：' . $data['report_name'];

            Db::startTrans();
            try {
                CalculateReportModel::editCalculateReport($data);
                $report_datas = CalculateReportModel::seeCalculateReport($data['id']);

                $data_file = $this->createreport($data['id']);
                $data_file['main_organization_id'] = $data_redis['main_organization_id'];
                $data_file['create_by'] = $data_redis['userid'];
                $data_file['modify_by'] = $data_redis['userid'];
                $data_file['id'] = guid();

                // 删除之前的文件
                FileModel::delFile($report_datas[0]['file_id']);
                FileModel::addFile($data_file);

                // 更新核算报告中的文件id
                $data_edit['id'] = $data['id'];
                $data_edit['file_id'] = $data_file['id'];
                CalculateReportModel::editCalculateReport($data_edit);
                OperationModel::addOperation($data_log);

                Db::commit();
                return json(['code'=>200, 'message'=>"修改成功"]);
            } catch (\Exception $e) {
                Db::rollback();
                return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * del 删除核算报告
     * 
     * @author wuyinghua
	 * @return void
     */
    public function del() {
        $data_redis = $this->request->middleware('data_redis');
        $id = $_POST['id'];
        $report_info = CalculateReportModel::getReportDatas($id);

        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '产品碳足迹';
        $data_log['type'] = '功能操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $this->request->pathinfo();
        $data_log['log'] = '删除核算报告：' . $report_info['report_name'];

        OperationModel::addOperation($data_log);
        $del = CalculateReportModel::delCalculateReport($id);

        if($del){
            return json(['code'=>200, 'message'=>"删除成功"]);
        }else{
            return json(['code'=>404, 'message'=>"删除失败"]);
        }
    }

    /**
     * see 查看核算报告详情
     * 
     * @author wuyinghua
	 * @return void
     */
    public function see() {
        $id = $_GET['id'];
        $list = CalculateReportModel::seeCalculateReport($id);

        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }

	//======================================================================
	// PRIVATE FUNCTIONS
	//======================================================================

    /**
     * createreport 生成核算报告
     * 
     * @author wuyinghua
     * @param $id
	 * @return $data
     */
    private function createreport($id) {
        // 添加核算报告文件
        ini_set('pcre.backtrack_limit', '999999999');

        // 原材料获取阶段 raw
        $block_raw = false;
        // 原材料获取阶段原材料排放量计算
        $raw_materials = [];
        $block_raw_materials = false;

        // 原材料获取阶段其他材料排放量计算
        $raw_material_others = [];
        $block_raw_material_others = false;

        // 原材料获取阶段运输排放量计算
        $raw_material_transports  = [];
        $block_raw_material_transports = false;

        // 原材料获取阶段能耗排放量计算
        $raw_material_energies = [];
        $block_raw_material_energies = false;

        // 生产制造阶段 manufacturing
        $block_manufacturing = false;

        // 生产制造阶段辅助材料排放量计算
        $manufacturing_auxiliaries  = [];
        $block_manufacturing_auxiliaries = false;

        // 生产制造阶段直接逸散排放量计算
        $manufacturing_escapes = [];
        $block_manufacturing_escapes = false;

        // 生产制造阶段运输排放量计算
        $manufacturing_transports = [];
        $block_manufacturing_transports = false;

        // 生产制造阶段能耗排放量计算
        $manufacturing_energies = [];
        $block_manufacturing_energies = false;

        // 分销零售阶段 distribution
        $block_distribution= false;

        // 分销零售阶段运输排放量计算
        $distribution_transports = [];
        $block_distribution_transports= false;

        // 分销零售阶段能耗排放量计算
        $distribution_energies = [];
        $block_distribution_energies= false;

        // 分销零售阶段物料排放量计算
        $distribution_materials = [];
        $block_distribution_materials= false;

        // 使用阶段 use
        $block_use= false;

        // 使用阶段运输排放量计算
        $use_transports = [];
        $block_use_transports= false;

        // 使用阶段能耗排放量计算
        $use_energies = [];
        $block_use_energies= false;

        // 使用阶段物料排放量计算
        $use_materials = [];
        $block_use_materials= false;

        // 废弃处置阶段 scrap 
        $block_scrap= false;

        // 废弃处置阶段运输排放量计算
        $scrap_transports = [];
        $block_scrap_transports= false;

        // 废弃处置阶段能耗排放量计算
        $scrap_energies = [];
        $block_scrap_energies= false;

        // 废弃处置阶段物料排放量计算
        $scrap_materials = [];
        $block_scrap_materials= false;

        // 废弃处置阶段直接逸散排放量计算
        $scrap_escapes = [];
        $block_scrap_escapes= false;

        $stage = [];
        $list = CalculateReportModel::getReportDatas($id);
        $count_date = explode('-', $list['count_date']);
        $list['create_time'] = explode('-', substr($list['create_time'], 0, 7));
        $list['create_time'] = $list['create_time'][0] . '年' . $list['create_time'][1] . '月';
        $list['count_date'] = $count_date[0] . '年' . $count_date[1] . '月至' . $count_date[2] . '年' . $count_date[3] . '月';
        $list['coefficient'] = $list['coefficient'] . 'kgCO2e/' .$list['unit_str'];
        $list['datas'] = ProductCarbonLabelService::getInfo($list['product_calculate_id']);

        if ($list['scope'] == 1) {
            $list['scope'] = '半生命周期（从摇篮到大门）';
        } elseif ($list['scope'] == 2) {
            $list['scope'] = '全生命周期（从摇篮到坟墓）';
        } elseif ($list['scope'] == 3) {
            $list['scope'] = '自定义';
        }

        // 报告类型：1-PAS 2050，2-ISO 14067 T.B.D 目前只有ISO 14067
        if ($list['report_type'] == 1) {
            $templatePath = "static/新版产品碳足迹评价报告示例.docx";
        } else {
            $templatePath = "static/新版产品碳足迹评价报告示例.docx";
        }

        $list['stage_str'] = '';
        $list['stage_rate'] = '';

        foreach (explode(',', $list['stage']) as $key => $value) {
            $stage_name = ProductDataModel::getStageName((int)$value);
            $stage[$key]['name'] = $stage_name['name'];
            $stage[$key]['rate'] = '0%';
        }

        foreach ($stage as $key => $value) {
            foreach ($list['datas']['tree'] as $tree_value) {
                if ($value['name'] == $tree_value['name']) {
                    $stage[$key]['rate'] = $tree_value['carbon_rate'];
                }
            }
        }

        foreach ($stage as $value) {
            $list['stage_str'] .= $value['name'] . '、';
            $list['stage_rate'] .= $value['rate'] . '、';
        }

        $list['stage_str'] = rtrim($list['stage_str'], "、");
        $list['stage_rate'] = rtrim($list['stage_rate'], "、");

        $templatePath = "static/新版产品碳足迹评价报告示例.docx";
        // 判断目录是否存在
        $filePath= './storage/uploads/'. date('Ymd', time());
        if (!is_dir($filePath)) {
            mkdir($filePath, 0777);
        }

        $filePathNew = $filePath . '/' . time() . '.docx';
        $templateProcessor = new TemplateProcessor($templatePath);
        $templateProcessor->setValues([
            'report_name'       => $list['report_name'], // 报告名称
            'count_date'        => $list['count_date'], // 报告说明
            'description'       => $list['description'], // 组织名称
            'organization_name' => $list['organization_name'],
            'create_time'       => $list['create_time'], // 报告创建时间
            'product_name'      => $list['product_name'], // 产品名称
            'product_no'        => $list['product_no'], // 产品编号
            'product_spec'      => $list['product_spec'], // 规格型号
            'remarks'           => $list['remarks'], // 评价目的
            'unit_str'          => $list['unit_str'], // 核算单位
            'scope'             => $list['scope'], // 研究范围（评价范围）
            'stage'             => $list['stage_str'], // 阶段
            'coefficient'       => $list['coefficient'], // 排放系数
            'stage_rate'        => $list['stage_rate'], // 阶段排放比率
        ]);

        foreach ($list['datas']['tree'] as $value) {
            if ($value['name'] == '原材料获取') {
                $block_raw = true;
                foreach ($value['children'] as $children_value) {
                    if ($children_value['name'] == '原材料') {
                        $block_raw_materials = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $raw_materials[$key]['raw_material_name'] = $children_value_two['name']; //名称
                            $raw_materials[$key]['raw_material_number'] = $children_value_two['number']; //投入量数值
                            $raw_materials[$key]['raw_material_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $raw_materials[$key]['raw_material_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $raw_materials[$key]['raw_material_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str']; //排放因子单位
                            $raw_materials[$key]['raw_material_emissions'] = $children_value_two['carbon_value']; //排放量
                            $raw_materials[$key]['raw_material_activity_source'] = '生产台账'; //活动水平数据来源
                            $raw_materials[$key]['raw_material_source'] = '中汽中心'; //排放因子数据来源
                            $raw_materials[$key]['raw_material_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('raw_material_sum', $children_value['carbon_value']);
                    } elseif ($children_value['name'] == '其他材料') {
                        $block_raw_material_others = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $raw_material_others[$key]['raw_material_other_name'] = $children_value_two['name']; //名称
                            $raw_material_others[$key]['raw_material_other_number'] = $children_value_two['number']; //投入量数值
                            $raw_material_others[$key]['raw_material_other_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $raw_material_others[$key]['raw_material_other_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $raw_material_others[$key]['raw_material_other_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str']; //排放因子单位
                            $raw_material_others[$key]['raw_material_other_emissions'] = $children_value_two['carbon_value']; //排放量
                            $raw_material_others[$key]['raw_material_other_activity_source'] = '生产台账'; //活动水平数据来源
                            $raw_material_others[$key]['raw_material_other_source'] = '中汽中心'; //排放因子数据来源
                            $raw_material_others[$key]['raw_material_other_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('raw_material_other_sum', $children_value['carbon_value']);
                    } elseif ($children_value['name'] == '运输') {
                        $block_raw_material_transports = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $raw_material_transports[$key]['raw_material_transport_name'] = $children_value_two['name']; //名称
                            $raw_material_transports[$key]['raw_material_transport_number'] = $children_value_two['number']; //投入量数值
                            $raw_material_transports[$key]['raw_material_transport_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $raw_material_transports[$key]['raw_material_transport_distance'] = $children_value_two['distance']; //运输距离（公里）
                            $raw_material_transports[$key]['raw_material_transport_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $raw_material_transports[$key]['raw_material_transport_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str'] . 'km'; //排放因子单位
                            $raw_material_transports[$key]['raw_material_transport_emissions'] = $children_value_two['carbon_value']; //排放量
                            $raw_material_transports[$key]['raw_material_transport_source'] = '中汽中心'; //排放因子数据来源
                            $raw_material_transports[$key]['raw_material_transport_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('raw_material_transport_sum', $children_value['carbon_value']);
                    } elseif ($children_value['name'] == '能耗') {
                        $block_raw_material_energies = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $raw_material_energies[$key]['raw_material_energy_name'] = $children_value_two['name']; //名称
                            $raw_material_energies[$key]['raw_material_energy_number'] = $children_value_two['number']; //投入量数值
                            $raw_material_energies[$key]['raw_material_energy_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $raw_material_energies[$key]['raw_material_energy_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $raw_material_energies[$key]['raw_material_energy_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str']; //排放因子单位
                            $raw_material_energies[$key]['raw_material_energy_emissions'] = $children_value_two['carbon_value']; //排放量
                            $raw_material_energies[$key]['raw_material_energy_activity_source'] = '生产台账'; //活动水平数据来源
                            $raw_material_energies[$key]['raw_material_energy_source'] = '中汽中心'; //排放因子数据来源
                            $raw_material_energies[$key]['raw_material_energy_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('raw_material_energy_sum', $children_value['carbon_value']);
                    }
                }
                $templateProcessor->cloneRowAndSetValues('raw_material_number', $raw_materials);
                $templateProcessor->cloneRowAndSetValues('raw_material_other_number', $raw_material_others);
                $templateProcessor->cloneRowAndSetValues('raw_material_transport_number', $raw_material_transports);
                $templateProcessor->cloneRowAndSetValues('raw_material_energy_number', $raw_material_energies);
            } elseif ($value['name'] == '生产制造') {
                $block_manufacturing = true;
                foreach ($value['children'] as $children_value) {
                    if ($children_value['name'] == '辅助材料') {
                        $block_manufacturing_auxiliaries = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $manufacturing_auxiliaries[$key]['manufacturing_auxiliary_name'] = $children_value_two['name']; //名称
                            $manufacturing_auxiliaries[$key]['manufacturing_auxiliary_number'] = $children_value_two['number']; //投入量数值
                            $manufacturing_auxiliaries[$key]['manufacturing_auxiliary_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $manufacturing_auxiliaries[$key]['manufacturing_auxiliary_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $manufacturing_auxiliaries[$key]['manufacturing_auxiliary_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str']; //排放因子单位
                            $manufacturing_auxiliaries[$key]['manufacturing_auxiliary_emissions'] = $children_value_two['carbon_value']; //排放量
                            $manufacturing_auxiliaries[$key]['manufacturing_auxiliary_activity_source'] = '生产台账'; //活动水平数据来源
                            $manufacturing_auxiliaries[$key]['manufacturing_auxiliary_source'] = '中汽中心'; //排放因子数据来源
                            $manufacturing_auxiliaries[$key]['manufacturing_auxiliary_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('manufacturing_auxiliary_sum', $children_value['carbon_value']);
                    } elseif ($children_value['name'] == '直接逸散') {
                        $block_manufacturing_escapes = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $manufacturing_escapes[$key]['manufacturing_escape_name'] = $children_value_two['name']; //名称
                            $manufacturing_escapes[$key]['manufacturing_escape_number'] = $children_value_two['number']; //投入量数值
                            $manufacturing_escapes[$key]['manufacturing_escape_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $manufacturing_escapes[$key]['manufacturing_escape_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $manufacturing_escapes[$key]['manufacturing_escape_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str']; //排放因子单位
                            $manufacturing_escapes[$key]['manufacturing_escape_emissions'] = $children_value_two['carbon_value']; //排放量
                            $manufacturing_escapes[$key]['manufacturing_escape_activity_source'] = '生产台账'; //活动水平数据来源
                            $manufacturing_escapes[$key]['manufacturing_escape_source'] = '中汽中心'; //排放因子数据来源
                            $manufacturing_escapes[$key]['manufacturing_escape_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('manufacturing_escape_sum', $children_value['carbon_value']);
                    } elseif ($children_value['name'] == '运输') {
                        $block_manufacturing_transports = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $manufacturing_transports[$key]['manufacturing_transport_name'] = $children_value_two['name']; //名称
                            $manufacturing_transports[$key]['manufacturing_transport_number'] = $children_value_two['number']; //投入量数值
                            $manufacturing_transports[$key]['manufacturing_transport_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $manufacturing_transports[$key]['manufacturing_transport_distance'] = $children_value_two['distance']; //运输距离（公里）
                            $manufacturing_transports[$key]['manufacturing_transport_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $manufacturing_transports[$key]['manufacturing_transport_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str'] . 'km'; //排放因子单位
                            $manufacturing_transports[$key]['manufacturing_transport_emissions'] = $children_value_two['carbon_value']; //排放量
                            $manufacturing_transports[$key]['manufacturing_transport_source'] = '中汽中心'; //排放因子数据来源
                            $manufacturing_transports[$key]['manufacturing_transport_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('manufacturing_transport_sum', $children_value['carbon_value']);
                    } elseif ($children_value['name'] == '能耗') {
                        $block_manufacturing_energies = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $manufacturing_energies[$key]['manufacturing_energy_name'] = $children_value_two['name']; //名称
                            $manufacturing_energies[$key]['manufacturing_energy_number'] = $children_value_two['number']; //投入量数值
                            $manufacturing_energies[$key]['manufacturing_energy_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $manufacturing_energies[$key]['manufacturing_energy_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $manufacturing_energies[$key]['manufacturing_energy_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str']; //排放因子单位
                            $manufacturing_energies[$key]['manufacturing_energy_emissions'] = $children_value_two['carbon_value']; //排放量
                            $manufacturing_energies[$key]['manufacturing_energy_activity_source'] = '生产台账'; //活动水平数据来源
                            $manufacturing_energies[$key]['manufacturing_energy_source'] = '中汽中心'; //排放因子数据来源
                            $manufacturing_energies[$key]['manufacturing_energy_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('manufacturing_energy_sum', $children_value['carbon_value']);
                    }
                }
                $templateProcessor->cloneRowAndSetValues('manufacturing_auxiliary_number', $manufacturing_auxiliaries);
                $templateProcessor->cloneRowAndSetValues('manufacturing_escape_number', $manufacturing_escapes);
                $templateProcessor->cloneRowAndSetValues('manufacturing_transport_number', $manufacturing_transports);
                $templateProcessor->cloneRowAndSetValues('manufacturing_energy_number', $manufacturing_energies);
            } elseif ($value['name'] == '分销零售') {
                $block_distribution = true;
                foreach ($value['children'] as $children_value) {
                    if ($children_value['name'] == '物料') {
                        $block_distribution_materials = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $distribution_materials[$key]['distribution_material_name'] = $children_value_two['name']; //名称
                            $distribution_materials[$key]['distribution_material_number'] = $children_value_two['number']; //投入量数值
                            $distribution_materials[$key]['distribution_material_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $distribution_materials[$key]['distribution_material_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $distribution_materials[$key]['distribution_material_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str']; //排放因子单位
                            $distribution_materials[$key]['distribution_material_emissions'] = $children_value_two['carbon_value']; //排放量
                            $distribution_materials[$key]['distribution_material_activity_source'] = '生产台账'; //活动水平数据来源
                            $distribution_materials[$key]['distribution_material_source'] = '中汽中心'; //排放因子数据来源
                            $distribution_materials[$key]['distribution_material_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('distribution_material_sum', $children_value['carbon_value']);
                    } elseif ($children_value['name'] == '运输') {
                        $block_distribution_transports = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $distribution_transports[$key]['distribution_transport_name'] = $children_value_two['name']; //名称
                            $distribution_transports[$key]['distribution_transport_number'] = $children_value_two['number']; //投入量数值
                            $distribution_transports[$key]['distribution_transport_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $distribution_transports[$key]['distribution_transport_distance'] = $children_value_two['distance']; //运输距离（公里）
                            $distribution_transports[$key]['distribution_transport_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $distribution_transports[$key]['distribution_transport_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str'] . 'km'; //排放因子单位
                            $distribution_transports[$key]['distribution_transport_emissions'] = $children_value_two['carbon_value']; //排放量
                            $distribution_transports[$key]['distribution_transport_source'] = '中汽中心'; //排放因子数据来源
                            $distribution_transports[$key]['distribution_transport_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('distribution_transport_sum', $children_value['carbon_value']);
                    } elseif ($children_value['name'] == '能耗') {
                        $block_distribution_energies = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $distribution_energies[$key]['distribution_energy_name'] = $children_value_two['name']; //名称
                            $distribution_energies[$key]['distribution_energy_number'] = $children_value_two['number']; //投入量数值
                            $distribution_energies[$key]['distribution_energy_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $distribution_energies[$key]['distribution_energy_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $distribution_energies[$key]['distribution_energy_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str']; //排放因子单位
                            $distribution_energies[$key]['distribution_energy_emissions'] = $children_value_two['carbon_value']; //排放量
                            $distribution_energies[$key]['distribution_energy_activity_source'] = '生产台账'; //活动水平数据来源
                            $distribution_energies[$key]['distribution_energy_source'] = '中汽中心'; //排放因子数据来源
                            $distribution_energies[$key]['distribution_energy_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('distribution_energy_sum', $children_value['carbon_value']);
                    }
                }
                $templateProcessor->cloneRowAndSetValues('distribution_material_number', $distribution_materials);
                $templateProcessor->cloneRowAndSetValues('distribution_transport_number', $distribution_transports);
                $templateProcessor->cloneRowAndSetValues('distribution_energy_number', $distribution_energies);
            } elseif ($value['name'] == '使用') {
                $block_use = true;
                foreach ($value['children'] as $children_value) {
                    if ($children_value['name'] == '物料') {
                        $block_use_materials = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $use_materials[$key]['use_material_name'] = $children_value_two['name']; //名称
                            $use_materials[$key]['use_material_number'] = $children_value_two['number']; //投入量数值
                            $use_materials[$key]['use_material_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $use_materials[$key]['use_material_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $use_materials[$key]['use_material_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str']; //排放因子单位
                            $use_materials[$key]['use_material_emissions'] = $children_value_two['carbon_value']; //排放量
                            $use_materials[$key]['use_material_activity_source'] = '生产台账'; //活动水平数据来源
                            $use_materials[$key]['use_material_source'] = '中汽中心'; //排放因子数据来源
                            $use_materials[$key]['use_material_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('use_material_sum', $children_value['carbon_value']);
                    } elseif ($children_value['name'] == '运输') {
                        $block_use_transports = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $use_transports[$key]['use_transport_name'] = $children_value_two['name']; //名称
                            $use_transports[$key]['use_transport_number'] = $children_value_two['number']; //投入量数值
                            $use_transports[$key]['use_transport_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $use_transports[$key]['use_transport_distance'] = $children_value_two['distance']; //运输距离（公里）
                            $use_transports[$key]['use_transport_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $use_transports[$key]['use_transport_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str'] . 'km'; //排放因子单位
                            $use_transports[$key]['use_transport_emissions'] = $children_value_two['carbon_value']; //排放量
                            $use_transports[$key]['use_transport_source'] = '中汽中心'; //排放因子数据来源
                            $use_transports[$key]['use_transport_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('use_transport_sum', $children_value['carbon_value']);
                    } elseif ($children_value['name'] == '能耗') {
                        $block_use_energies = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $use_energies[$key]['use_energy_name'] = $children_value_two['name']; //名称
                            $use_energies[$key]['use_energy_number'] = $children_value_two['number']; //投入量数值
                            $use_energies[$key]['use_energy_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $use_energies[$key]['use_energy_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $use_energies[$key]['use_energy_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str']; //排放因子单位
                            $use_energies[$key]['use_energy_emissions'] = $children_value_two['carbon_value']; //排放量
                            $use_energies[$key]['use_energy_activity_source'] = '生产台账'; //活动水平数据来源
                            $use_energies[$key]['use_energy_source'] = '中汽中心'; //排放因子数据来源
                            $use_energies[$key]['use_energy_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('use_energy_sum', $children_value['carbon_value']);
                    }
                }
                $templateProcessor->cloneRowAndSetValues('use_material_number', $use_materials);
                $templateProcessor->cloneRowAndSetValues('use_transport_number', $use_transports);
                $templateProcessor->cloneRowAndSetValues('use_energy_number', $use_energies);
            } elseif ($value['name'] == '废弃处置') {
                $block_scrap = true;
                foreach ($value['children'] as $children_value) {
                    if ($children_value['name'] == '物料') {
                        $block_scrap_materials = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $scrap_materials[$key]['scrap_material_name'] = $children_value_two['name']; //名称
                            $scrap_materials[$key]['scrap_material_number'] = $children_value_two['number']; //投入量数值
                            $scrap_materials[$key]['scrap_material_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $scrap_materials[$key]['scrap_material_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $scrap_materials[$key]['scrap_material_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str']; //排放因子单位
                            $scrap_materials[$key]['scrap_material_emissions'] = $children_value_two['carbon_value']; //排放量
                            $scrap_materials[$key]['scrap_material_activity_source'] = '生产台账'; //活动水平数据来源
                            $scrap_materials[$key]['scrap_material_source'] = '中汽中心'; //排放因子数据来源
                            $scrap_materials[$key]['scrap_material_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('scrap_material_sum', $children_value['carbon_value']);
                    } elseif ($children_value['name'] == '直接逸散') {
                        $block_scrap_escapes = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $scrap_escapes[$key]['scrap_escape_name'] = $children_value_two['name']; //名称
                            $scrap_escapes[$key]['scrap_escape_number'] = $children_value_two['number']; //投入量数值
                            $scrap_escapes[$key]['scrap_escape_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $scrap_escapes[$key]['scrap_escape_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $scrap_escapes[$key]['scrap_escape_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str']; //排放因子单位
                            $scrap_escapes[$key]['scrap_escape_emissions'] = $children_value_two['carbon_value']; //排放量
                            $scrap_escapes[$key]['scrap_escape_activity_source'] = '生产台账'; //活动水平数据来源
                            $scrap_escapes[$key]['scrap_escape_source'] = '中汽中心'; //排放因子数据来源
                            $scrap_escapes[$key]['scrap_escape_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('scrap_escape_sum', $children_value['carbon_value']);
                    } elseif ($children_value['name'] == '运输') {
                        $block_scrap_transports = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $scrap_transports[$key]['scrap_transport_name'] = $children_value_two['name']; //名称
                            $scrap_transports[$key]['scrap_transport_number'] = $children_value_two['number']; //投入量数值
                            $scrap_transports[$key]['scrap_transport_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $scrap_transports[$key]['scrap_transport_distance'] = $children_value_two['distance']; //运输距离（公里）
                            $scrap_transports[$key]['scrap_transport_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $scrap_transports[$key]['scrap_transport_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str'] . 'km'; //排放因子单位
                            $scrap_transports[$key]['scrap_transport_emissions'] = $children_value_two['carbon_value']; //排放量
                            $scrap_transports[$key]['scrap_transport_source'] = '中汽中心'; //排放因子数据来源
                            $scrap_transports[$key]['scrap_transport_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('scrap_transport_sum', $children_value['carbon_value']);
                    } elseif ($children_value['name'] == '能耗') {
                        $block_scrap_energies = true;
                        foreach ($children_value['children'] as $key => $children_value_two) {
                            $scrap_energies[$key]['scrap_energy_name'] = $children_value_two['name']; //名称
                            $scrap_energies[$key]['scrap_energy_number'] = $children_value_two['number']; //投入量数值
                            $scrap_energies[$key]['scrap_energy_unit_str'] = $children_value_two['unit_str']; //投入量单位
                            $scrap_energies[$key]['scrap_energy_factor'] = $children_value_two['carbon_value_num']; //排放因子
                            $scrap_energies[$key]['scrap_energy_factor_unit'] = 'kgCO2e/' . $children_value_two['unit_str']; //排放因子单位
                            $scrap_energies[$key]['scrap_energy_emissions'] = $children_value_two['carbon_value']; //排放量
                            $scrap_energies[$key]['scrap_energy_activity_source'] = '生产台账'; //活动水平数据来源
                            $scrap_energies[$key]['scrap_energy_source'] = '中汽中心'; //排放因子数据来源
                            $scrap_energies[$key]['scrap_energy_remarks'] = ''; //备注
                        }
                        $templateProcessor->setValue('scrap_energy_sum', $children_value['carbon_value']);
                    }
                }
                $templateProcessor->cloneRowAndSetValues('scrap_material_number', $scrap_materials);
                $templateProcessor->cloneRowAndSetValues('scrap_escape_number', $scrap_escapes);
                $templateProcessor->cloneRowAndSetValues('scrap_transport_number', $scrap_transports);
                $templateProcessor->cloneRowAndSetValues('scrap_energy_number', $scrap_energies);
            }
        }

        // 原材料获取阶段
        $templateProcessor->cloneBlock('block_raw', $block_raw ? 1 : 0);
        $templateProcessor->cloneBlock('block_raw_materials', $block_raw_materials ? 1 : 0);
        $templateProcessor->cloneBlock('block_raw_material_others', $block_raw_material_others ? 1 : 0);
        $templateProcessor->cloneBlock('block_raw_material_transports', $block_raw_material_transports ? 1 : 0);
        $templateProcessor->cloneBlock('block_raw_material_energies', $block_raw_material_energies ? 1 : 0);

        // 生产制造阶段
        $templateProcessor->cloneBlock('block_manufacturing', $block_manufacturing ? 1 : 0);
        $templateProcessor->cloneBlock('block_manufacturing_auxiliaries', $block_manufacturing_auxiliaries ? 1 : 0);
        $templateProcessor->cloneBlock('block_manufacturing_escapes', $block_manufacturing_escapes ? 1 : 0);
        $templateProcessor->cloneBlock('block_manufacturing_transports', $block_manufacturing_transports ? 1 : 0);
        $templateProcessor->cloneBlock('block_manufacturing_energies', $block_manufacturing_energies ? 1 : 0);

        // 分销零售阶段
        $templateProcessor->cloneBlock('block_distribution', $block_distribution ? 1 : 0);
        $templateProcessor->cloneBlock('block_distribution_materials', $block_distribution_materials ? 1 : 0);
        $templateProcessor->cloneBlock('block_distribution_transports', $block_distribution_transports ? 1 : 0);
        $templateProcessor->cloneBlock('block_distribution_energies', $block_distribution_energies ? 1 : 0);

        // 使用阶段
        $templateProcessor->cloneBlock('block_use', $block_use ? 1 : 0);
        $templateProcessor->cloneBlock('block_use_materials', $block_use_materials ? 1 : 0);
        $templateProcessor->cloneBlock('block_use_transports', $block_use_transports ? 1 : 0);
        $templateProcessor->cloneBlock('block_use_energies', $block_use_energies ? 1 : 0);

        // 废弃处置阶段
        $templateProcessor->cloneBlock('block_scrap', $block_scrap ? 1 : 0);
        $templateProcessor->cloneBlock('block_scrap_materials', $block_scrap_materials ? 1 : 0);
        $templateProcessor->cloneBlock('block_scrap_escapes', $block_scrap_escapes ? 1 : 0);
        $templateProcessor->cloneBlock('block_scrap_transports', $block_scrap_transports ? 1 : 0);
        $templateProcessor->cloneBlock('block_scrap_energies', $block_scrap_energies ? 1 : 0);
        $templateProcessor->saveAs($filePathNew);

        $data_file['module'] = '产品碳足迹';
        $data_file['file_path'] = substr($filePathNew, 1);
        $data_file['source_name'] = $list['report_name'] . '.docx';
        $data_file['create_time'] = date('Y-m-d H:i:s');
        $data_file['modify_time'] = date('Y-m-d H:i:s');

        return $data_file;
    }

	//======================================================================
	// PROTECTED FUNCTIONS
	//======================================================================

    /**
     * validateForm 验证
     * 
     * @author wuyinghua
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(CalculateReportValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}
