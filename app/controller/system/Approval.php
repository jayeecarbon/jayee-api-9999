<?php
namespace app\controller\system;

use app\BaseController;
use app\model\system\ApprovalModel;
use app\model\system\OperationModel;
use think\Request;

/**
 * Approval 审批设置
 */
class Approval extends BaseController {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * index 业务流程列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function index() {
        $data_redis = $this->request->middleware('data_redis');
        $main_organization_id = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
        $list = ApprovalModel::getApprovals($main_organization_id)->toArray();
        foreach ($list as $key => $value) {
            $value['branch_company'] == ApprovalModel::IS_ON_YES ? $list[$key]['branch_company'] = TRUE : $list[$key]['branch_company'] = FALSE;
            $value['factory'] == ApprovalModel::IS_ON_YES ? $list[$key]['factory'] = TRUE : $list[$key]['factory'] = FALSE;
            $value['parent_company'] == ApprovalModel::IS_ON_YES ? $list[$key]['parent_company'] = TRUE : $list[$key]['parent_company'] = FALSE;
        }

        $data['code'] = 200;
        $data['data']['list'] = $list;
        $data['data']['approval_node'] = ApprovalModel::APPROVAL_NODE_TYPE;

        return json($data);
    }

    /**
     * see 审批回显
     * 
     * @author wuyinghua
	 * @return void
     */
    public function see() {
        $id = $_GET['id'];
        $list = ApprovalModel::getApproval($id);
        $list['branch_company'] == ApprovalModel::IS_ON_YES ? $list['branch_company'] = TRUE : $list['branch_company'] = FALSE;
        $list['factory'] == ApprovalModel::IS_ON_YES ? $list['factory'] = TRUE : $list['factory'] = FALSE;
        $list['parent_company'] == ApprovalModel::IS_ON_YES ? $list['parent_company'] = TRUE : $list['parent_company'] = FALSE;

        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }

    /**
     * edit 编辑审批
     * 
     * @author wuyinghua
     * @param $request
     * @return void
     */
    public function edit(Request $request) {

        if (request()->isPost()) {
            $data_redis = $request->middleware('data_redis');

            $data['id'] = $_POST['id'];
            $data['name'] = $_POST['name'];
            $data['factory'] = $_POST['factory'];
            $data['branch_company'] = $_POST['branch_company'];
            $data['parent_company'] = $_POST['parent_company'];
            $data['modify_by'] = $data_redis['userid'];
            $data['modify_time'] = date('Y-m-d H:i:s');

            // 至少有一个开启
            if ($data['factory'] == ApprovalModel::IS_ON_NO && $data['branch_company'] == ApprovalModel::IS_ON_NO && $data['parent_company'] == ApprovalModel::IS_ON_NO) {
                return json(['code'=>201, 'message'=>"至少开启一个审批"]);
            }

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '产品碳足迹';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '编辑审批：' . $data['name'];

            $edit = ApprovalModel::editApproval($data);
            OperationModel::addOperation($data_log);

            if ($edit) {
                return json(['code'=>200, 'message'=>"修改成功"]);
            } else {
                return json(['code'=>404, 'message'=>"修改失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>"修改失败"]);
        }
    }
}