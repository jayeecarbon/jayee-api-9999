<?php
declare (strict_types = 1);

namespace app\controller\data;

use app\BaseController;
use app\model\data\ParamModel;
use app\model\data\ProductEmissionModel;
use app\service\ProductCarbonLabelService;
use app\controller\system\File;
use think\facade\Cache;

class ProductEmission extends BaseController {
    /**
     * 产品排放分析列表
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function index() {
        $data_redis = $this->request->middleware('data_redis');
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        // 过滤条件：产品名称、产品编号、产品状态、核算周期开始、核算周期结束
        $filters = [
            'filter_product_name' => isset($_GET['filterProductName']) ? $_GET['filterProductName'] : '',
            'main_organization_id' => isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : ''
        ];

        $list = ProductEmissionModel::getCalculates($page_size, $page_index, $filters)->toArray();

        $list_new = [];
        foreach ($list['data'] as $key => $value) {
            $tree_data = ProductCarbonLabelService::getInfo($value['id']);

            $value['stage_raw'] = '';
            $value['stage_manufacturing'] = '';
            $value['stage_distribution'] = '';
            $value['stage_use'] = '';
            $value['stage_scrap'] = '';

            foreach ($tree_data['tree'] as $k => $v) {
                if ($v['unique_no'] == ParamModel::STAGE_TYPE_RAW) {
                    $value['stage_raw'] = $v['carbon_value_num'];
                }
                
                if ($v['unique_no'] == ParamModel::STAGE_TYPE_MANUFACTURING) {
                    $value['stage_manufacturing'] = $v['carbon_value_num'];
                }
                
                if ($v['unique_no'] == ParamModel::STAGE_TYPE_DISTRIBUTION) {
                    $value['stage_distribution'] = $v['carbon_value_num'];
                }
                
                if ($v['unique_no'] == ParamModel::STAGE_TYPE_USE) {
                    $value['stage_use'] = $v['carbon_value_num'];
                }
                
                if ($v['unique_no'] == ParamModel::STAGE_TYPE_SCRAP) {
                    $value['stage_scrap'] = $v['carbon_value_num'];
                }
            }

            $list_new[$key] = $value;
        }

        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * 产品详情
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function info() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $customer_product_info = ProductEmissionModel::getProductDataById($id);
        if ($customer_product_info == null) {
            return json(['code'=>201, 'message'=>"未查询到相客户产品相关信息"]);
        }

        $tree_data = ProductCarbonLabelService::getInfo($id);
        $customer_product_info['tree'] = $tree_data;

        // 原材料分析
        $category = ParamModel::STAGE_TYPE_RAW_CATEGORY_RAW;
        $raw_data = ProductEmissionModel::getProductDataByCategory($id, $category)->toArray();
        $emissions_arr = array_column($raw_data, 'emissions');
        $sum_emissions = array_sum($emissions_arr);

        foreach ($raw_data as $key => &$value) {
            $value['emission_rate_num'] = ($sum_emissions == 0) ? (float)$value['emissions'] : (float)$value['emissions'] / $sum_emissions;
            $value['emission_rate_num'] = round($value['emission_rate_num'] * 100, 6);
            $value['emission_rate'] = $value['emission_rate_num'] . '%';
        }

        $customer_product_info['raw_table'] = array_slice($raw_data, 0, 5);
        $customer_product_info['raw_chart'] = $raw_data;

        // 能耗分析 不同阶段可以有相同的能耗名称，需要合并（按能耗排放源的名称）
        $categories = ParamModel::STAGE_TYPE_RAW_CATEGORY_ENERGY . ',' .
                      ParamModel::STAGE_TYPE_MANUFACTURING_CATEGORY_ENERGY . ',' .
                      ParamModel::STAGE_TYPE_DISTRIBUTION_ENERGY . ',' .
                      ParamModel::STAGE_TYPE_USE_ENERGY . ',' .
                      ParamModel::STAGE_TYPE_SCRAP_ENERGY;

        $energy_data = ProductEmissionModel::getProductDataByCategories($id, $categories)->toArray();
        $emissions_arr = array_column($energy_data, 'emissions');
        $sum_emissions = array_sum($emissions_arr);

        foreach ($energy_data as $key => &$value) {
            $value['emission_rate_num'] = ($sum_emissions == 0) ? (float)$value['emissions'] : (float)$value['emissions'] / $sum_emissions;
            $value['emission_rate_num'] = round($value['emission_rate_num'] * 100, 6);
            $value['emission_rate'] = $value['emission_rate_num'] . '%';
        }

        $customer_product_info['energy_table'] = array_slice($energy_data, 0, 5);
        $customer_product_info['energy_chart'] = $energy_data;

        // 供应商分析 需要按照供应商名称合并
        $supplier_data = ProductEmissionModel::getProductDataBySuppliers($id)->toArray();
        $emissions_arr = array_column($supplier_data, 'emissions');
        $sum_emissions = array_sum($emissions_arr);

        foreach ($supplier_data as $key => &$value) {
            $value['emission_rate_num'] = ($sum_emissions == 0) ? (float)$value['emissions'] : (float)$value['emissions'] / $sum_emissions;
            $value['emission_rate_num'] = round($value['emission_rate_num'] * 100, 6);
            $value['emissions'] = round($value['emissions'] * 100, 6);
            $value['emission_rate'] = $value['emission_rate_num'] . '%';
        }

        $customer_product_info['supplier_table'] = array_slice($supplier_data, 0, 5);
        $customer_product_info['supplier_chart'] = $supplier_data;

        $data['code'] = 200;
        $data['data']['list'] = $customer_product_info;
        return json($data);
    }

    /**
     * add 加入对比
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function add() {
        $data_redis = $this->request->middleware('data_redis');
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $id = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($id == '' || $id == null || $id == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        Cache::push($data_redis['userid'] . 'contrast_arr', $id);

        return json(['code'=>200, 'message'=>'添加成功']);
    }

    /**
     * list 对比列表
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function list() {
        $data_redis = $this->request->middleware('data_redis');
        $contrast_arr = Cache::get($data_redis['userid'] . 'contrast_arr');

        $list_new = [];
        if ($contrast_arr != NULL){
            foreach ($contrast_arr as $key => $value) {
                $list_new[] = ProductEmissionModel::getProductDataById($value);
            }
        }

        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        return json($data);
    }

    /**
     * del 对比删除
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function del() {
        $data_redis = $this->request->middleware('data_redis');
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $id = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($id == '' || $id == null || $id == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $contrast_arr = Cache::get($data_redis['userid'] . 'contrast_arr');
        $contrast_arr = array_diff($contrast_arr, [$id]);

        $del = Cache::set($data_redis['userid'] . 'contrast_arr', $contrast_arr);

        if($del){
            return json(['code'=>200, 'message'=>"删除成功"]);
        }else{
            return json(['code'=>404, 'message'=>"删除失败"]);
        }
    }

    /**
     * contrast 产品对比
     *
     * @author wuyinghua
     * @return \think\Response
     */
    public function contrast() {
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $ids = isset($params_payload_arr['ids']) ? $params_payload_arr['ids'] : '';
        if ($ids == '' || $ids == null || $ids == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        // 获取五个阶段以及合计的排放量 (双方都没有的阶段不显示)
        $stage_arr = [];
        foreach ($ids as $value) {
            $stages = ProductEmissionModel::getStages($value)->toArray();
            foreach ($stages as $key => $value) {
                array_push($stage_arr, $value['data_stage']);
            }
        }

        $list_new = [];
        $num = 0;
        foreach ($ids as $key => $value) {
            $calculate_date = ProductEmissionModel::getProductDataById($value);
            $product[$key] = $calculate_date['product_name'] . '-' . $calculate_date['product_no'];
            $emissions[$key] = $calculate_date['emissions'];
            $week[$key] = $calculate_date['week'];
            

            // 遍历当前核算所有的阶段
            foreach (array_unique($stage_arr) as $stage_key =>$stage_value) {
                $stage_info = ProductEmissionModel::getStageName($stage_value);
                $emission_data = ProductEmissionModel::getProductDataByStage($value, $stage_value);

                if ($stage_info['id'] == ParamModel::STAGE_TYPE_RAW) {
                    $stage_key = 'raw';
                } elseif ($stage_info['id'] == ParamModel::STAGE_TYPE_MANUFACTURING) {
                    $stage_key = 'manufacturing';
                } elseif ($stage_info['id'] == ParamModel::STAGE_TYPE_DISTRIBUTION) {
                    $stage_key = 'distribution';
                } elseif ($stage_info['id'] == ParamModel::STAGE_TYPE_USE) {
                    $stage_key = 'use';
                } elseif ($stage_info['id'] == ParamModel::STAGE_TYPE_SCRAP) {
                    $stage_key = 'scrap';
                }

                /** 产品对照结果总览图表 */
                $list_new['overview_chart']['emissions'][$stage_value]['data'][] = round($emission_data['emissions'], 6);

                /** 产品对照结果总览表格 */
                $list_new['overview'][$key][$stage_key] = round($emission_data['emissions'], 6);
                $list_new['overview'][$key]['total'] = $calculate_date['emissions'];

                if ($num > 0) {

                    $list_new['overview']['title'][] = $stage_info['name'];
                    $list_new['overview_chart']['title'][] = $stage_info['name'];
                    // 产品名称 + 编号如果相同 再 + 核算周期
                    if ($product[$key - 1] == $product[$key]) {
                        $product[$key - 1] = $product[$key - 1] . '(' . $week[$key - 1] . ')';
                        $product[$key] = $product[$key] . '(' . $week[$key] . ')';
                    }

                    $list_new['overview_chart']['yaxis'][$key - 1] = $product[$key - 1] . '碳排放(KgCO2e)';
                    $list_new['overview_chart']['yaxis'][$key] = $product[$key] . '碳排放';

                    // 产品1相较于产品2的表个数据
                    $list_new['overview']['reduction'][$stage_key] = round($list_new['overview'][$key - 1][$stage_key] - $list_new['overview'][$key][$stage_key], 6);
                    $list_new['overview']['reduction']['total'] = round($list_new['overview'][$key - 1]['total'] - $list_new['overview'][$key]['total'], 6);

                    $list_new['overview']['reduction_rate'][$stage_key] = ($list_new['overview'][$key - 1][$stage_key] == 0) ? $list_new['overview'][$key - 1][$stage_key] : $list_new['overview']['reduction'][$stage_key] / $list_new['overview'][$key - 1][$stage_key];
                    $list_new['overview']['reduction_rate'][$stage_key] = round($list_new['overview']['reduction_rate'][$stage_key] * 100, 6);
                    $list_new['overview']['reduction_rate'][$stage_key] = $list_new['overview']['reduction_rate'][$stage_key] . '%';

                    $list_new['overview']['reduction_rate']['total'] = ($list_new['overview'][$key - 1]['total'] == 0) ? $list_new['overview'][$key - 1]['total'] : $list_new['overview']['reduction']['total'] / $list_new['overview'][$key - 1]['total'];
                    $list_new['overview']['reduction_rate']['total'] = round($list_new['overview']['reduction_rate']['total'] * 100, 6);

                    // 产品2相较于产品1共增加的碳排放量和增排比例的描述
                    $list_new['product_data'][$key - 1]['product_name'] = $product[$key - 1];
                    $list_new['product_data'][$key - 1]['emissions'] = $emissions[$key - 1] . 'kgCO2e';
                    $list_new['product_data'][$key]['product_name'] = $product[$key];
                    $list_new['product_data'][$key]['emissions'] = $emissions[$key] . 'kgCO2e';
                    $list_new['product_data_summary']['reduction'] = 0 - $list_new['overview']['reduction']['total'];
                    $list_new['product_data_summary']['reduction_rate'] = 0 - $list_new['overview']['reduction_rate']['total'];

                    $list_new['overview']['reduction_rate']['total'] = $list_new['overview']['reduction_rate']['total'] . '%';
                    $list_new['product_data_summary']['reduction_rate'] = $list_new['product_data_summary']['reduction_rate'] . '%';
                }

            }

            /** 原材料分析 */
            $category = ParamModel::STAGE_TYPE_RAW_CATEGORY_RAW;
            $raw_data = ProductEmissionModel::getProductDataByCategory($value, $category)->toArray();
            $emissions_arr = array_column($raw_data, 'emissions');
            $sum_emissions = array_sum($emissions_arr);
    
            foreach ($raw_data as &$raw_value) {
                $raw_value['emission_rate_num'] = ($sum_emissions == 0) ? (float)$raw_value['emissions'] : (float)$raw_value['emissions'] / $sum_emissions;
                $raw_value['emission_rate_num'] = round($raw_value['emission_rate_num'] * 100, 6);
                $raw_value['emission_rate'] = $raw_value['emission_rate_num'] . '%';
            }

            $list_new['raw'][$key]['raw_table'] = array_slice($raw_data, 0, 5);
            $list_new['raw'][$key]['raw_chart'] = $raw_data;

            /** 能耗分析 */
            $categories = ParamModel::STAGE_TYPE_RAW_CATEGORY_ENERGY . ',' .
                          ParamModel::STAGE_TYPE_MANUFACTURING_CATEGORY_ENERGY . ',' .
                          ParamModel::STAGE_TYPE_DISTRIBUTION_ENERGY . ',' .
                          ParamModel::STAGE_TYPE_USE_ENERGY . ',' .
                          ParamModel::STAGE_TYPE_SCRAP_ENERGY;

            $energy_data = ProductEmissionModel::getProductDataByCategories($value, $categories)->toArray();
            $emissions_arr = array_column($energy_data, 'emissions');
            $sum_emissions = array_sum($emissions_arr);

            foreach ($energy_data as &$energy_value) {
                $energy_value['emission_rate_num'] = ($sum_emissions == 0) ? (float)$energy_value['emissions'] : (float)$energy_value['emissions'] / $sum_emissions;
                $energy_value['emission_rate_num'] = round($energy_value['emission_rate_num'] * 100, 6);
                $energy_value['emission_rate'] = $energy_value['emission_rate_num'] . '%';
            }

            $list_new['energy'][$key]['energy_table'] = array_slice($energy_data, 0, 5);
            $list_new['energy'][$key]['energy_chart'] = $energy_data;

            /** 供应商分析 */
            $supplier_data = ProductEmissionModel::getProductDataBySuppliers($value)->toArray();
            $emissions_arr = array_column($supplier_data, 'emissions');
            $sum_emissions = array_sum($emissions_arr);
    
            foreach ($supplier_data as &$supplier_value) {
                $supplier_value['emission_rate_num'] = ($sum_emissions == 0) ? (float)$supplier_value['emissions'] : (float)$supplier_value['emissions'] / $sum_emissions;
                $supplier_value['emission_rate_num'] = round($supplier_value['emission_rate_num'] * 100, 6);
                $supplier_value['emission_rate'] = $supplier_value['emission_rate_num'] . '%';
            }
    
            $list_new['supplier'][$key]['supplier_table'] = array_slice($supplier_data, 0, 5);
            $list_new['supplier'][$key]['supplier_chart'] = $supplier_data;

            $num++;
        }

        /** 原材料分析共有材料 */
        $raw_common_element = self::commonElement($list_new['raw'][0]['raw_chart'], $list_new['raw'][1]['raw_chart']);
        foreach ($raw_common_element['base_arr'] as $key => $base_arr_value) {
            $list_new['raw_comon']['categories'][$key] = $base_arr_value['name'];
        }

        $list_new['raw_comon'][0]['raw_table'] = array_slice($raw_common_element['base_arr'], 0, 5);
        $list_new['raw_comon'][0]['raw_chart'] = $raw_common_element['base_arr'];
        $list_new['raw_comon'][1]['raw_table'] = array_slice($raw_common_element['compare_arr'], 0, 5);
        $list_new['raw_comon'][1]['raw_chart'] = $raw_common_element['compare_arr'];

        /** 能耗分析共有能耗 */
        $energy_common_element = self::commonElement($list_new['energy'][0]['energy_chart'], $list_new['energy'][1]['energy_chart']);
        foreach ($energy_common_element['base_arr'] as $key => $base_arr_value) {
            $list_new['energy_comon']['categories'][$key] = $base_arr_value['name'];
        }

        $list_new['energy_comon'][0]['energy_table'] = array_slice($energy_common_element['base_arr'], 0, 5);
        $list_new['energy_comon'][0]['energy_chart'] = $energy_common_element['base_arr'];
        $list_new['energy_comon'][1]['energy_table'] = array_slice($energy_common_element['compare_arr'], 0, 5);
        $list_new['energy_comon'][1]['energy_chart'] = $energy_common_element['compare_arr'];

        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        return json($data);
    }

    /**
     * export 导出操作日志
     * 
     * @author wuyinghua
	 * @return void
     */
    public function export() {
        $data_redis = $this->request->middleware('data_redis');
        // 过滤条件：产品名称、产品编号、产品状态、核算周期开始、核算周期结束
        $filters = [
            'main_organization_id' => isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : ''
        ];

        // 读取需要导出的数据
        $list = ProductEmissionModel::getAllcalculate($filters);

        $list_new = [];
        foreach ($list as $key => $value) {
            $tree_data = ProductCarbonLabelService::getInfo($value['id']);

            $value['stage_raw'] = '';
            $value['stage_manufacturing'] = '';
            $value['stage_distribution'] = '';
            $value['stage_use'] = '';
            $value['stage_scrap'] = '';

            foreach ($tree_data['tree'] as $k => $v) {
                if ($v['unique_no'] == ParamModel::STAGE_TYPE_RAW) {
                    $value['stage_raw'] = $v['carbon_value_num'];
                }
                
                if ($v['unique_no'] == ParamModel::STAGE_TYPE_MANUFACTURING) {
                    $value['stage_manufacturing'] = $v['carbon_value_num'];
                }
                
                if ($v['unique_no'] == ParamModel::STAGE_TYPE_DISTRIBUTION) {
                    $value['stage_distribution'] = $v['carbon_value_num'];
                }
                
                if ($v['unique_no'] == ParamModel::STAGE_TYPE_USE) {
                    $value['stage_use'] = $v['carbon_value_num'];
                }
                
                if ($v['unique_no'] == ParamModel::STAGE_TYPE_SCRAP) {
                    $value['stage_scrap'] = $v['carbon_value_num'];
                }
            }

            $list_new[$key] = $value;
        }

        // excel 的数据
        $xlsData = [];
        foreach ($list_new as $k => $v) {
            $xlsData[] = [
                '产品名称'            => $v['product_name'],
                '产品编码'            => $v['product_no'],
                '产品规格'            => $v['product_spec'],
                '单位'                => $v['unit_str'],
                '产品碳排放系数'       => $v['coefficient'],
                '原材料获取（kgCO2e）' => $v['stage_raw'],
                '生产制造（kgCO2e）'   => $v['stage_manufacturing'],
                '分销零售（kgCO2e）'   => $v['stage_distribution'],
                '使用（kgCO2e）'       => $v['stage_use'],
                '废弃处置（kgCO2e）'   => $v['stage_scrap'],
                '核算周期'             => $v['week'],
            ];
        }
        if (count($xlsData) == 0) {
            return '暂无产品排放数据';
        }

        // 编辑表格的title，返回的数据格式是：$xlsCell = ['产品名称', '产品编码', '产品规格', ...]
        $length = count($xlsData);
        foreach ($xlsData[$length - 1] as $k => $v) {
            $xlsCell[] = $k;
        }

        // 设置对应的excel坐标
        $setWidth = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'];

        // 调用自定义方法
        File::exportExcel('产品碳排放分析表', $xlsCell, $xlsData, $setWidth);
    }

    /**
     * 获取共有元素
     *
     * @author wuyinghua
     * @param $base_arr
     * @param $compare_arr
     * @return $array
     */
    public function commonElement($base_arr, $compare_arr) {

        $base_arr_new = [];
        $compare_arr_new = [];
        foreach ($base_arr as $key => $value) {
            foreach ($compare_arr as $k => $v) {
                if ($value['name'] == $v['name']) {
                    array_push($base_arr_new, $value);
                    array_push($compare_arr_new, $v);
                }
            }
        }

        $array['base_arr'] = $base_arr_new;
        $array['compare_arr'] = $compare_arr_new;

        return $array;
    }
}
