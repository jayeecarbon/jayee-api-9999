<?php
declare (strict_types=1);

namespace app\validate;

use think\Validate;

class DatumValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'organization_id' => 'require',
        'type' => 'require',
        'policy' => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'organization_id.require' => '组织不能为空',
        'type.require' => '设定类型不能为空',
        'policy.require' => '基准线排放量重算政策不能为空',
    ];
}
