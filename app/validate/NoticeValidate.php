<?php


namespace app\validate;


use think\Validate;

class NoticeValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @author wuyinghua
     * @var array
     */
    protected $rule = [
        'title'                  => 'require',
        'type'                   => 'require',
        'content'                => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @author wuyinghua
     * @var array
     */
    protected $message = [
        'title.require'          => '消息标题不能为空',
        'type.require'           => '分类不能为空',
        'content.require'        => '消息内容不能为空',
    ];
}