<?php


namespace app\validate;


use think\Validate;

class ProductCarbonLabelValidate extends Validate
{

    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @author wuyinghua
     * @var array
     */
    protected $rule = [
        'carbon_label_name'               => 'require',
        'product_id'                      => 'require',
        'week_start'                      => 'require',
        'week_end'                        => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @author wuyinghua
     * @var array
     */
    protected $message = [
        'carbon_label_name.require'       => '碳标签名称不能为空',
        'product_id.require'              => '产品id不能为空',
        'week_start.require'              => '核算时间开始时间不能为空',
        'week_end.require'                => '核算时间结束时间不能为空',
    ];
}