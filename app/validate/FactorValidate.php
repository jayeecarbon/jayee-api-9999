<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class FactorValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'title'              => 'require',
        'grade'              => 'require',
        'factor_value'       => 'require',
        'molecule'           => 'require',
        'denominator'        => 'require',
        'mechanism'          => 'require',
        'year'               => 'require',
        'country'            => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'title.require'          => '因子名称不能为空',
        'grade.require'           => '因子评级不能为空',
        'factor_value.require'          => '因子数值不能为空',
        'molecule.require'           => '因子分子单位不能为空',
        'denominator.require'          => '因子分母单位不能为空',
        'mechanism.require'          => '发布机构不能为空',
        'year.require'           => '发布年份不能为空',
        'country.require'          => '发布国家/组织不能为空',
    ];
}
