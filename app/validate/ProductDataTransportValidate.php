<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class ProductDataTransportValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @author wuyinghua
     * @var array
     */
    protected $rule = [
        'name'                      => 'require|length:2,20',
        'materials'                 => 'require',
        'distance'                  => 'require|float',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @author wuyinghua
     * @var array
     */
    protected $message = [
        'name.require'              => '排放源名称不能为空',
        'name.length'               => '排放源名称长度需在2-20个字符之间',
        'materials.require'         => '材质、用量、单位不能为空',
        'distance.require'          => '排放源运输距离不能为空',
        'distance.float'            => '排放源运输距离需要是数字',
    ];
}
