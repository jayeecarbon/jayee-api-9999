<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class DataOrganizationValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'name'     => 'require',
         'department'     => 'require',
        'register_address'      => 'require',
        'register_address_detail' => 'require',
        'production_address'     => 'require',
        'production_address_detail'     => 'require',
        'business_desc'     => 'require'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'name.require'     => '名称不能为空',
        'department.require'     => '碳盘查部门不能为空',
        'register_address.require'      => '注册地址不能为空',
        'register_address_detail.require' => '注册详细地址不能为空',
        'production_address.require'     => '生产经营许可地址不能为空',
        'production_address_detail.require'     => '生产经营许可地址详细地址不能为空',
        'business_desc.require'     => '企业描述不能为空',
    ];
}
