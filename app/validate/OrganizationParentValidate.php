<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class OrganizationParentValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @author wuyinghua
     * @var array
     */
    protected $rule = [
        'industry_id'    => 'require',
        'description'    => 'require',
        'contact_person' => 'require|length:2,20',
        'telephone'      => 'require|mobile',
        'province'       => 'require',
        'city'           => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @author wuyinghua
     * @var array
     */
    protected $message = [
        'industry_id.require'    => '所属行业不能为空',
        'description.require'    => '企业介绍不能为空',
        'contact_person.require' => '联系人不能为空',
        'contact_person.length'  => '联系人长度需在2-20个字符之间',
        'telephone.require'      => '联系电话不能为空',
        'telephone.mobile'       => '联系电话无效',
        'province.require'       => '省不能为空',
        'city.require'           => '市不能为空',
    ];
}
