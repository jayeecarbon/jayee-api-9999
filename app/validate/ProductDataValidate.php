<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class ProductDataValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @author wuyinghua
     * @var array
     */
    protected $rule = [
        'name'                      => 'require|length:1,20',
        'number'                    => 'require|float',
        'unit_type'                 => 'require',
        'unit'                      => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @author wuyinghua
     * @var array
     */
    protected $message = [
        'name.require'              => '排放源名称不能为空',
        'name.length'               => '排放源名称长度需在1-20个字符之间',
        'number.require'            => '排放源数量不能为空',
        'number.float'              => '排放源数量需要是数字',
        'unit_type.require'         => '排放源单位类型不能为空',
        'unit.require'              => '排放源单位不能为空',
    ];
}
