<?php
declare (strict_types=1);

namespace app\validate;

use think\Validate;

class AttestationOriginalFormValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'data_stage' => 'require',
        'data_stage_code' => 'require',
        'category' => 'require',
        'category_code' => 'require',
        'attestation_id' => 'require',
        'material' => 'array',
        'material.name' => 'require',
        'material.number' => 'require',
        'material.source' => 'require',
        'files' => 'require',
        'compareDataList' => 'array',

    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'data_stage.require' => '缺少参数',
        'data_stage_code.require' => '缺少参数',
        'category.require' => '缺少参数',
        'category_code.require' => '缺少参数',
        'material.array' => '不是数组',
        'material.name.require' => '原材料不能为空',
        'material.number.require' => '数值不能为空',
        'material.source.require' => '来源不能为空',
        'files.require' => '证明材料不能为空',
        'attestation_id.require' => '认证id不能为空',
        'compareDataList.require' => '缺少对比数据',
        'compareDataList.array' => '不是数组'
    ];
}
