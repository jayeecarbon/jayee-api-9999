<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class IsoValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'is_check'     => 'require',
        'active_desc'     => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'is_check.require'     => '纳入碳排放不能为空',
        'active_desc.require'     => '活动描述不能为空',
    ];
}
