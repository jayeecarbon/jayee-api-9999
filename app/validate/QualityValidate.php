<?php
declare (strict_types=1);

namespace app\validate;

use think\Validate;

class QualityValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'version' => 'require',
        'development_time' => 'require',
        'content' => 'require'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'version.require' => '版本号不能为空',
        'development_time.require' => '制定时间不能为空',
        'content.require' => '制定内容不能为空'
    ];
}
