<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class OrgCalculateReportValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @author wuyinghua
     * @var array
     */
    protected $rule = [
        'report_name'               => 'require|length:2,50',
        'organization_id'           => 'require',
        'organization_calculate_id' => 'require',
        'data_quality_id'           => 'require',
        'datum_id'                  => 'require',
        'telephone'                 => 'mobile',
        'email'                     => 'email',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @author wuyinghua
     * @var array
     */
    protected $message = [
        'report_name.require'               => '报告名称不能为空',
        'report_name.length'                => '报告名称长度需在2-50个字符之间',
        'organization_id.require'           => '所属组织不能为空',
        'organization_calculate_id.require' => '核算名称不能为空',
        'data_quality_id.require'           => '数据质量控制计划版本不能为空',
        'datum_id.require'                  => '排放基准不能为空',
        'telephone.mobile'                  => '手机号无效',
        'email.email'                       => '邮箱无效',
    ];
}
