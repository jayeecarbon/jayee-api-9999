<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class UserRegisterValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @author wuyinghua
     * @var array
     */
    protected $rule = [
        'organization_name'          => 'require|length:2,20',
        'organization_code'          => 'require|length:2,20',
        'password'                   => 'require|length:6,20',
        'user_phone'                 => 'mobile',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @author wuyinghua
     * @var array
     */
    protected $message = [
        'organization_name.require'    => '组织名称不能为空',
        'organization_name.length'     => '组织名称长度需在2-20个字符之间',
        'organization_code.require'    => '统一社会信用代码不能为空',
        'organization_code.length'     => '统一社会信用代码长度需在2-20个字符之间',
        'password.require'             => '密码不能为空',
        'password.length'              => '密码长度需在6-20个字符之间',
        'user_phone.mobile'            => '手机号无效',
    ];
}
