<?php

namespace app\model\Examine;

use think\facade\Db;


/**
 * FileModel
 */
class ExamineModel extends Db
{

    public function getList($page_size, $page_index, $filters)
    {
        $where = array();
        if (isset($filters['calculate_name']) && !empty($filters['calculate_name'])) {
            $where[] = array(['c.calculate_name', 'like', '%' . trim($filters['calculate_name']) . '%']);
        }
        if (isset($filters['calculate_year']) && !empty($filters['calculate_year'])) {
            $where[] = array(['r.calculate_year', '=', $filters['calculate_year']]);
        }
        if (isset($filters['calculate_month']) && !empty($filters['calculate_month'])) {
            $where[] = array(['r.calculate_month', '=', $filters['calculate_month']]);
        }
        if (isset($filters['state']) && !empty($filters['state'])) {
            $where[] = array(['r.state', '=', $filters['state']]);
        }

        if (isset($filters['organization_id']) && !empty($filters['organization_id'])) {
            $where[] = array(['r.organization_id', '=', $filters['organization_id']]);
        }else{
            $where[] = array(['r.main_organization_id', '=', $filters['main_organization_id']]);
        }
        //按钮状态
        $status = Db::table('jy_approval')
            ->where([
                'main_organization_id'=>$filters['main_organization_id'],
                'name'=>'组织碳核算数据上报'])->find();
        if($status){
            $has_button=1;//有
        }elsE{
            $has_button=2;//没有
        }
        $list = Db::table('jy_organization_calculate_detail r')
            ->field('r.id,c.calculate_name,r.calculate_year,r.calculate_month,
            r.emissions,r.state,o.name organization_name,
            r.modify_time,r.modify_by,u.username')
            ->leftJoin('jy_organization_calculate c','r.calculate_model_id=c.id')
            ->leftJoin('jy_organization o','r.organization_id=o.id')
            ->leftJoin('jy_user u','u.id=r.modify_by')
            ->where($where)
            ->whereIn('r.state','3,4,5')
            ->order('r.modify_time', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index])
            ->toArray();

        $data=$list['data'];
        if($data){
            foreach ($data as $k=>$v){
                if($v['state']==3){
                    $v['state']='待工厂审核';
                }elseif($v['state']==4){
                    $v['state']='待分公司审核';
                }elseif($v['state']==5){
                    $v['state']='待母公司审核';
                }elseif($v['state']==7){
                    $v['state']='审核不通过';
                }
               $v['has_button']=  $has_button;
               $data[$k] = $v;
            }
            $list['data'] = $data;
        }

        return $list;
    }

    public static function getInfoById($id)
    {
        $list = Db::table('jy_organization_calculate_detail')
            ->where('id', (int)$id)
            ->find();
       $organization= Db::table('jy_organization')
           ->where('id', $list['organization_id'])
           ->find();
       $list['organization_name'] = $organization['name'];
        if($list['state']==3){
            $list['state_name'] = '待工厂审核';
        }elseif($list['state']==4){
            $list['state_name'] = '待分公司审核';
        }elseif($list['state']==5){
            $list['state_name'] = '待母公司审核';
        }elseif($list['state']==6){
            $list['state_name'] = '审核通过';
        }elseif($list['state']==7){
            $list['state_name'] = '审核不通过';
        }

        return $list;
    }

    public static function status($data)
    {
        $info = Db::table('jy_organization_calculate_detail')->where('id',$data['id'])->find();
        $organization_calculate_id =$info['organization_calculate_id'];

        $is_all = Db::table('jy_organization_calculate_detail')
            ->field('state')
            ->where(
                [
                    'organization_calculate_id'=>$organization_calculate_id,
                    'calculate_year'=>$info['calculate_year']
                ]
            )
            ->where('id','<>',$data['id'])
            ->select()->toArray();

        $is_one = array_unique(array_column($is_all,'state'));
        Db::startTrans();
        try {
       if(count($is_one)==1&&$is_one[0]==6){

           Db::table('jy_organization_calculate')->where('id',$organization_calculate_id)
               ->update([
                   'state'=>2,
                   'modify_time'=>date('Y-m-d H:i:s'),
                   'modify_by'=>$data['modify_by']
           ]);

       };
        $update = Db::table('jy_organization_calculate_detail')->save($data);
        $emissions =  Db::table('jy_organization_calculate_detail')
            ->where([
                'organization_calculate_id'=>$organization_calculate_id,
                'is_del'=>1
            ])->sum('emissions');

        Db::table('jy_organization_calculate')->where('id',$organization_calculate_id)
            ->update([
               'emissions'=>$emissions,
                'modify_time'=>date('Y-m-d H:i:s'),
                'modify_by'=>$data['modify_by']
            ]);

            Db::commit();
            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    public static function organizationList($id)
    {

        $list = Db::table('jy_organization')
            ->field('id,name')
            ->where('main_organization_id',$id)
            ->select()
            ->toArray();

        return $list;
    }
}