<?php
declare (strict_types = 1);

namespace app\model\product;

use think\facade\Db;

/**
 * ProductDataModel
 */
class CalculateReportModel extends Db
{

    /**
     * 启用/禁用-启用
     */
    CONST STATE_YES = 1;

    /**
     * 启用/禁用-禁用
     */
    CONST STATE_NO = 2;

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getCalculateReports 查询核算报告
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getCalculateReports($page_size, $page_index, $filters) {
        $where = array();

        if ($filters['filter_report_name']) {
            $where[] = array(['jpcr.report_name', 'like', '%' . trim($filters['filter_report_name']) . '%']);
        }

        if ($filters['filter_product_name']) {
            $where[] = array(['jp.product_name', 'like', '%' . trim($filters['filter_product_name']) . '%']);
        }

        if ($filters['filter_type']) {
            $where[] = array(['jpcr.report_type', '=', trim($filters['filter_type'])]);
        }

        if ($filters['main_organization_id']) {
            $where[] = array(['jpcr.main_organization_id', '=', $filters['main_organization_id']]);
        }

        $list = Db::table('jy_product_count_report jpcr')
            ->field('jpcr.id, jpcr.product_id, jpcr.product_calculate_id, jpcr.report_type report_type_id, jpcr.file_id, jr.name report_type, jpcr.report_name, jpcr.count_date, jpcr.description, ju.username, jp.product_name, jpcr.modify_time')
            ->leftJoin('jy_product jp', 'jpcr.product_id = jp.id')
            ->leftJoin('jy_user ju', 'jpcr.modify_by = ju.id')
            ->leftJoin('jy_report jr', 'jpcr.report_type = jr.id')
            ->where($where)
            ->order(['jpcr.modify_time'=>'desc', 'jpcr.create_time'=>'desc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * getCalculateProducts 查询产品
     * 
     * @author wuyinghua
     * @param $main_organization_id
	 * @return $list
     */
    public static function getCalculateProducts($main_organization_id) {
        $list = Db::table('jy_product_calculate jpc')
            ->field('jpc.id, jpc.product_id, jpc.week_start, jpc.week_end, jp.product_name, jp.product_no')
            ->leftJoin('jy_product jp', 'jpc.product_id = jp.id')
            ->where(['jpc.state'=>self::STATE_YES, 'jpc.main_organization_id'=>$main_organization_id])
            ->order('jpc.id', 'desc')
            ->select();

        return $list;
    }

    /**
     * addCalculateReport 添加核算报告
     * 
     * @author wuyinghua
     * @param $data
	 * @return $add
     */
    public static function addCalculateReport($data) {
        $add = Db::table('jy_product_count_report')->insertGetId($data);

        return $add;
    }

    /**
     * editCalculateReport 编辑核算报告
     * 
     * @author wuyinghua
     * @param $data
	 * @return $edit
     */
    public static function editCalculateReport($data) {
        $edit = Db::table('jy_product_count_report')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

    /**
     * delCalculateReport 删除核算报告
     * 
     * @author wuyinghua
     * @param $id
	 * @return $del
     */
    public static function delCalculateReport($id) {
        $del = Db::table('jy_product_count_report')->where('id', (int)$id)->delete();

        return $del;
    }

    /**
     * seeCalculateReport 查看核算报告详情
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function seeCalculateReport($id) {
        $list = Db::table('jy_product_count_report jpcr')
            ->field('jpcr.id, jpcr.product_id, jpcr.product_calculate_id, jpcr.report_name, jpcr.count_date, ju.username, jp.product_name, jp.product_no, jpcr.modify_time, jr.id report_type_id, jr.name report_type, jpcr.description, jpcr.file_id')
            ->field('0 + CAST(jpc.number AS CHAR) number, jpc.unit, jyu.name unit_str, 0 + CAST(jpc.emissions AS CHAR) emissions, 0 + CAST(jpc.coefficient AS CHAR) coefficient')
            ->leftJoin('jy_product jp', 'jpcr.product_id = jp.id')
            ->leftJoin('jy_user ju', 'jpcr.modify_by = ju.id')
            ->leftJoin('jy_report jr', 'jpcr.report_type = jr.id')
            ->leftJoin('jy_product_calculate jpc', 'jpcr.product_calculate_id = jpc.id')
            ->leftJoin('jy_unit jyu', 'jyu.id = jpc.unit')
            ->where('jpcr.id', (int)$id)
            ->select();

        return $list;
    }

    /**
     * getCalculateWeeks 获取核算周期
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getCalculateWeeks($id) {
        $list = Db::table('jy_product_calculate jpc') -> field('jr.id, jr.name report_type') -> select();

        return $list;
    }

    /**
     * getReportTypes 获取报告类型
     * 
     * @author wuyinghua
	 * @return $list
     */
    public static function getReportTypes() {
        $list = Db::table('jy_report jr') -> field('jr.id, jr.name report_type') -> select();

        return $list;
    }

    /**
     * getCalculateId 获取核算ID
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getCalculateId() {
        $list = Db::table('jy_product_data jpd') -> field('jpd.id') -> select();

        return $list;
    }

    /**
     * getReportDatas 获取核算报告数据
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getReportDatas($id) {
        $list = Db::table('jy_product_count_report jpcr')
        ->field('jpcr.id, jpcr.product_calculate_id product_calculate_id, jpcr.report_name, jpcr.count_date, jpcr.report_type, jpcr.description, jpcr.create_time')
        ->field('jp.product_name, jp.product_no, jp.product_spec')
        ->field('jpc.unit, jpc.scope, jpc.stage, jpc.coefficient, jpc.remarks')
        ->field('jyu.name unit_str, ju.username, jo.name organization_name')
        ->leftJoin('jy_product jp', 'jpcr.product_id = jp.id')
        ->leftJoin('jy_product_calculate jpc', 'jpcr.product_calculate_id = jpc.id')
        ->leftJoin('jy_user ju', 'jpcr.create_by = ju.id')
        ->leftJoin('jy_organization jo', 'ju.main_organization_id = jo.id')
        ->leftJoin('jy_unit jyu', 'jyu.id = jpc.unit')
        ->where('jpcr.id', (int)$id)
        ->find();

        return $list;
    }
}
