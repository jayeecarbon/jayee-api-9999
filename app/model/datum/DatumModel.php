<?php

namespace app\model\datum;

use think\facade\Db;


/**
 * ReductionModel
 */
class DatumModel extends Db
{
    /**
     * getList 查询列表
     *
     * @param $page_ize
     * @param $page_index
     * @param $filters
     * @return $list
     */
    public function getList($page_size, $page_index, $filters)
    {
        $where = array();

        if (isset($filters['organization_id']) && !empty($filters['organization_id'])) {
            $where[] = array(['r.organization_id', '=', $filters['organization_id']]);
        }else{
            $where[] = array(['r.main_organization_id', '=', $filters['main_organization_id']]);
        }

        $list = Db::table('jy_datum r')
            ->field('o.name organization_name,r.id,r.type,0+CAST(r.total AS CHAR) total,
            r.modify_time,r.modify_by,u.username')
            ->leftJoin('jy_organization o', 'r.organization_id=o.id')
            ->leftJoin('jy_user u','u.id=r.modify_by')
            ->where($where)
            ->order('r.modify_time', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * add 添加
     *
     * @param $data
     * @return $add
     */
    public static function add($data)
    {
       $has_exist =  Db::table('jy_datum')->where('organization_id',$data['organization_id'])->find();
       if($has_exist){
           return 2;
       }
       $add = Db::table('jy_datum')->insertGetId($data);
        if($add){
            return 1;
        }else{
            return false;
        }

    }

    /**
     * getInfoById 获取详情
     *
     * @return $del
     */
    public static function getInfoById($id)
    {
        $list = Db::table('jy_datum r')
            ->where('r.id', (int)$id)
            ->find();
        return $list;
    }


    /**
     * updates 修改
     *
     * @return $del
     */
       public static function edit($data)
    {
            $update = Db::table('jy_datum')
                ->save($data);
            if($update){
                return true;
            }
            return false;
    }

    public static function organizationList($id)
    {

        $list = Db::table('jy_organization')
            ->field('id,name')
            ->where('main_organization_id',$id)
            ->select()
            ->toArray();

        return $list;
    }


}