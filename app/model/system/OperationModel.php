<?php
namespace app\model\system;

use think\facade\Db;

/**
 * OperationModel
 */
class OperationModel extends Db {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getOperations 查询操作日志
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getOperations($page_size, $page_index, $filters) {
        $where = array();

        if ($filters['filter_user_name']) {
            $where[] = array(['ju.username', 'like', '%' . trim($filters['filter_user_name']) . '%']);
        }

        if ($filters['filter_module']) {
            $where[] = array(['jol.module', 'like', '%' . trim($filters['filter_module']) . '%']);
        }

        if ($filters['filter_time_start']) {
            $where[] = array(['jol.time', '>=', $filters['filter_time_start']]);
        }

        if ($filters['filter_time_end']) {
            $where[] = array(['jol.time', '<=', $filters['filter_time_end']]);
        }

        $where[] = array(['jol.module', 'in', $filters['modules']]);
        $where[] = array(['jol.main_organization_id', '=', $filters['main_organization_id']]);

        $list = Db::table('jy_operation_log jol')
            ->field('jol.user_id, jol.time, jol.log, jol.url, ju.username, jol.module, jol.type')
            ->leftJoin('jy_user ju', 'jol.user_id = ju.id')
            ->where($where)
            ->order('jol.id', 'desc')
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * addOperation 添加操作日志
     * 
     * @author wuyinghua
     * @param $data
	 * @return $add
     */
    public static function addOperation($data) {
        $add = Db::table('jy_operation_log')->insert($data);

        return $add;
    }

    /**
     * getAllOperation 获取所有操作日志
     * 
     * @author wuyinghua
	 * @return $list
     */
    public static function getAllOperation() {
        $list = Db::table('jy_operation_log jol')
        ->field('jol.user_id, jol.time, jol.log, jol.url, ju.username, jol.module, jol.type')
        ->leftJoin('jy_user ju', 'jol.user_id = ju.id')
        ->order('jol.id', 'desc')
        ->select();

        return $list;
    }
 
}