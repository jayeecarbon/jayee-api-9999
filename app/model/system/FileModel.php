<?php
namespace app\model\system;

use think\facade\Db;

/**
 * FileModel
 */
class FileModel extends Db {

    /**
     * 下载类型-供应产品导入模板（供应商）
     */
    CONST SUPPLIER_TEMPLATE = 'supplier_template';

    /**
     * 下载类型-供应产品导入模板（采购者）
     */
    CONST CUSTOMER_TEMPLATE = 'customer_template';

    /**
     * 下载类型-碳排放因子导入模板
     */
    CONST FACTOR_TEMPLATE = 'factor_template';

    /**
     * 下载类型-认证
     */
    CONST CERTIFICATION_TYPE = 'certification';

    /**
     * 下载类型-认证zip
     */
    CONST CERTIFICATION_ZIP_TYPE = 'certification_zip';

    /**
     * 下载类型-认证
     */
    CONST NOTICE_TYPE = 'notice';

    /**
     * 下载类型-捷亦云系统操作手册
     */
    CONST SYSTEM_MANUAL_TYPE = 'system_manual';

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getFiles 查询下载文件
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getFiles($page_size, $page_index, $filters) {
        $where = array();

        if ($filters['filter_user_name']) {
            $where[] = array(['ju.username', 'like', '%' . trim($filters['filter_user_name']) . '%']);
        }

        if ($filters['filter_module']) {
            $where[] = array(['jf.module', 'like', '%' . trim($filters['filter_module']) . '%']);
        }

        if ($filters['filter_time_start']) {
            $where[] = array(['jf.modify_time', '>=', $filters['filter_time_start']]);
        }

        if ($filters['filter_time_end']) {
            $where[] = array(['jf.modify_time', '<=', $filters['filter_time_end']]);
        }

        $where[] = array(['jf.module', 'in', $filters['modules']]);
        $where[] = array(['jf.main_organization_id', '=', $filters['main_organization_id']]);

        $list = Db::table('jy_file jf')
            ->field('jf.id, jf.module, jf.source_name file_name, jf.modify_time, ju.username, jo.name organization')
            ->leftJoin('jy_user ju', 'jf.create_by = ju.id')
            ->leftJoin('jy_organization jo', 'ju.main_organization_id = jo.id')
            ->where($where)
            ->order(['jf.modify_time'=>'desc', 'jf.create_time'=>'desc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * getFile 查询文件
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getFile($id) {
        $list = Db::table('jy_file jf')
            ->field('jf.id, jf.source_name, jf.file_path')
            ->where('id', $id)
            ->find();

        return $list;
    }

    /**
     * getFileByIds 查询文件
     * 
     * @author wuyinghua
     * @param $ids
	 * @return $list
     */
    public static function getFileByIds($ids) {
        $list = Db::table('jy_file jf')
            ->field('jf.id, jf.source_name name, jf.file_path')
            ->whereIn('id', $ids)
            ->select();

        return $list;
    }

    /**
     * addFile 添加文件
     * 
     * @author wuyinghua
     * @param $data
	 * @return $id
     */
    public static function addFile($data) {
        $add = Db::table('jy_file')->insert($data);

        return $add;
    }

    /**
     * updateFile 更新文件
     * 
     * @author wuyinghua
     * @param $data
	 * @return $id
     */
    public static function updateFile($data = null, $file_id = null) {
        if ($data != null) {
            // 生成文件之后将状态变更为已生成（status = 2）
            $update = Db::table('jy_file')->where(['id'=>$file_id,'type'=>2])->update([
                'file_path' => $data['file_path'],
                'status'    => 2,
            ]);
        } else {
            // 执行脚本，将生成中（status = 1）的文件状态变更为生成失败（status = 3）
            $update = Db::table('jy_file')->where(['status'=>1,'type'=>2])->update([
                'status'    => 3,
            ]);
        }


        return $update;
    }

    /**
     * getModules 查询操作模块
     * 
     * @author wuyinghua
	 * @return $list
     */
    public static function getModules() {
        $list = Db::table('jy_menu jm')
            ->field('jm.id, jm.title')
            ->where('pid', 0)
            ->select();

        return $list;
    }

    /**
     * delFile 删除文件
     * 
     * @author wuyinghua
     * @param $id
	 * @return $del
     */
    public static function delFile($id) {
        $del = Db::table('jy_file')->where('id', $id)->delete();

        return $del;
    }

    /**
     * @notes 通过id获取文件信息详情
     * 
     * @author wuyinghua
     * @param $id
     * @return array|mixed|Db|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getInfoById($id) {
        $list = Db::table('jy_file')
            ->field('id, file_path, source_name as name')
            ->where('id', $id)
            ->find();

        return $list;
    }
}