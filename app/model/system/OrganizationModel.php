<?php
namespace app\model\system;

use think\facade\Db;

/**
 * OrganizationModel
 */
class OrganizationModel extends Db {

    /**
     * 组织类型-分公司
     */
    CONST TYPE_BRANCH_COMPANY = 1;

    /**
     * 组织类型-工厂
     */
    CONST TYPE_FACTORY = 2;

    /**
     * 上级组织-母公司
     */
    CONST TYPE_PARENT_COMPANY  = 0;

    /**
     * 组织类型-下拉选择map，母公司不可在组织管理-新增组织下创建
     */
    CONST ORGANIZATION_TYPE_SELECT_MAP = [
        ['id'=>self::TYPE_BRANCH_COMPANY, 'name'=>'分公司'],
        ['id'=>self::TYPE_FACTORY, 'name'=>'工厂'],
    ];

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getOrganizations 查询组织
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getOrganizations($page_size, $page_index, $filters) {
        if ($filters['main_organization_id']) {
            $where[] = array(['jo.main_organization_id', '=', $filters['main_organization_id']]);
        }

        $list = Db::table('jy_organization jo')
            ->where($where)
            ->order(['jo.id' => 'asc', 'jo.pid' => 'asc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * getInOrganizations 根据id查询组织
     * 
     * @author wuyinghua
     * @param $ids
	 * @return $list
     */
    public static function getInOrganizations($ids) {

        $list = Db::table('jy_organization jo')
        ->field('jo.id, jo.pid, jo.name')
        ->whereIn('jo.id', $ids)
        ->select();

        return $list;
    }

    /**
     * getAllOrganizations 查询全部组织
     * 
     * @author wuyinghua
	 * @return $list
     */
    public static function getAllOrganizations() {
        $list = Db::table('jy_organization')
            ->field('id, pid, name, type')
            ->select();

        return $list;
    }

    /**
     * getBranchOrganizations 查询自身及下级组织
     * 
     * @author wuyinghua
	 * @return $list
     */
    public static function getBranchOrganizations($organization_id) {
        $whereor = [['id', '=', (int)$organization_id], ['pid', '=', (int)$organization_id]];
        $list = Db::table('jy_organization')
            ->field('id, pid, name, type')
            ->where('id', (int)$organization_id)
            ->whereOr('pid',(int)$organization_id)
            ->select();

        return $list;
    }

    /**
     * getAllFiledOrganizations 查询全部组织
     * 
     * @author wuyinghua
     * @param $main_organization_id
	 * @return $list
     */
    public static function getAllFiledOrganizations($main_organization_id) {
        $list = Db::table('jy_organization')->where('main_organization_id', (int)$main_organization_id)->select();

        return $list;
    }

    /**
     * getOrganization 查询组织
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getOrganization($id) {
        $list = Db::table('jy_organization')->where('id', (int)$id)->find();

        return $list;
    }

    /**
     * getIndustry 查询行业
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getIndustry($id) {
        $list = Db::table('jy_industry')->where('id', (int)$id)->find();

        return $list;
    }

    /**
     * addOrganization 添加组织
     * 
     * @author wuyinghua
     * @param $data
	 * @return $list
     */
    public static function addOrganization($data) {
        $add = Db::table('jy_organization')->insertGetId($data);

        return $add;
    }

    /**
     * editOrganization 编辑组织
     * 
     * @author wuyinghua
     * @param $data
	 * @return $list
     */
    public static function editOrganization($data) {
        $edit = Db::table('jy_organization')->where('id', (int)$data['id'])->update($data);

        return $edit;
    }

    /**
     * delOrganization 删除组织
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function delOrganization($id) {

        Db::startTrans();
        try {
            // 查询删除组织的下属
            $children_data = Db::table('jy_organization')->where('pid', (int)$id)->select();

            // 查询删除组织的上属
            $del_data = Db::table('jy_organization')->where('id', (int)$id)->find();

            // 删除子公司其下属工厂将归属母公司
            foreach ($children_data as $value) {
                Db::table('jy_organization')->where('id', (int)$value['id'])->update(['pid' => (int)$del_data['pid']]);
            }

            // 删除组织
            Db::table('jy_organization')->where('id', (int)$id)->delete();

            // 删除用户表中的组织
            $user_list = Db::table('jy_user ju')->select();
            foreach ($user_list as $key => $value) {
                $organization = explode(',', $value['organization_id']);
                foreach ($organization as $k => $v) {
                    if ($v == $id) {
                        unset($organization[$k]);
                    }
                }

                Db::table('jy_user')->where('id', (int)$value['id'])->update(['organization_id' => implode(',', $organization)]);
            }

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * getCitys 查询城市
     * 
     * @author wuyinghua
	 * @return $list
     */
    public static function getCitys() {
        $list = Db::table('jy_city')
        ->field('id, pid, name, level')
        ->select();

        return $list;
    }

    /**
     * getByLevelCitys 通过level查询城市
     * 
     * @author wuyinghua
	 * @return $list
     */
    public static function getByLevelCitys($level) {
        $list = Db::table('jy_city')
        ->field('id, name')
        ->where('level', (int)$level)
        ->select();

        return $list;
    }

    /**
     * getCity 查询城市
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public static function getCity($id) {
        $list = Db::table('jy_city')->where('id', (int)$id)->find();

        return $list;
    }

    /**
     * getIndustries 查询行业
     * 
     * @author wuyinghua
	 * @return $list
     */
    public static function getIndustries() {
        $list = Db::table('jy_industry')->select();

        return $list;
    }

     /**
     * getOrganizationByCode 通过用户名查找用户
     * 
     * @author wuyinghua
     * @param $data
	 * @return $list
     */
    public static function getOrganizationByCode($data) {
        $list = Db::table('jy_organization')->where('code', $data)->select();

        return $list;
    }
 
}