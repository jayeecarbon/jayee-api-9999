<?php
namespace app\model\admin;

use think\facade\Db;

/**
 * CarbonPriceModel
 */
class CarbonPriceModel extends Db {

	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getCarbonPrices 查询碳价管理列表
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public function getCarbonPrices($filters) {
        $where = array();

        if (isset($filters['filter_city'])) {
            $where[] = array(['jcp.city', 'like', '%' . trim($filters['filter_city']) . '%']);
        }

        if (isset($filters['filter_time_start'])) {
            $where[] = array(['jcp.time', '>=', $filters['filter_time_start']]);
        }

        if (isset($filters['filter_time_end'])) {
            $where[] = array(['jcp.time', '>=', $filters['filter_time_end']]);
        }

        $list = Db::table('jy_carbon_price jcp')
            ->field('jcp.city, YEAR(jcp.time) time, jcp.trading_avg_price, jcp.turnover, jcp.amount, jcp.daily_percent')
            ->where($where)
            ->order('jcp.id', 'asc')
            ->select();

        return $list;
    }

    /**
     * getCitys 城市列表
     * 
     * @author wuyinghua
	 * @return $list
     */
    public function getCitys() {
        $list = Db::table('jy_carbon_price')->field('DISTINCT(city)')->select();

        return $list;
    }

    /**
     * addCarbonPrice 添加产品核算
     * 
     * @author wuyinghua
     * @param $data
	 * @return $add
     */
    public function addCarbonPrice($data) {
        $add = Db::table('jy_carbon_price')->insertAll($data);

        return $add;
    }

    /**
     * delCarbonPrice 删除核算报告
     * 
     * @author wuyinghua
	 * @return $del
     */
    public function delCarbonPrice() {
        $del = Db::table('jy_carbon_price')->delete();

        return $del;
    }
}