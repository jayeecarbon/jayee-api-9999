<?php
namespace app\model\admin;

use think\facade\Db;

/**
 * FactorModel
 */
class FactorModel extends Db {

    //======================================================================
    // PUBLIC FUNCTIONS
    //======================================================================

    /**
     * getFactorList 查询因子库管理列表
     *
     * @param $page_ize
     * @param $page_index
     * @param $filters
     * @return $list
     */
    public function getFactorList($page_size, $page_index,$filters) {
        $where = array();

        if (isset($filters['title'])&& !empty($filters['title'])) {
            $where[] = array(['jcp.title', 'like', '%' . trim($filters['title']) . '%']);
            $organization_id=[99999999,$filters['organization_id']];
        }else{
            $organization_id=[$filters['organization_id']];
        }
        if (isset($filters['country'])&& !empty($filters['country'])) {
            $where[] = array(['jcp.country', 'like', '%' . trim($filters['country']) . '%']);
        }
        if (isset($filters['factor_id'])&& !empty($filters['factor_id'])) {
            $where[] = array(['jcp.id','=',$filters['factor_id']]);
        }
        if (isset($filters['mechanism'])&& !empty($filters['mechanism'])) {
            $where[] = array(['jcp.mechanism','=',$filters['mechanism']]);
        }
        if (isset($filters['start'])&& !empty($filters['start'])) {
            $where[] = array(['jcp.year','>=', $filters['start']]);
        }

        if (isset($filters['end'])&& !empty($filters['end'])) {
            $where[] = array(['jcp.year','<=', $filters['end']]);
        }
        if (isset($filters['factor_source'])&&$filters['factor_source'] !=0 &&!empty($filters['factor_source'])) {
            $where[] = array(['jcp.factor_source','=', $filters['factor_source']]);
        }
        if (isset($filters['status'])&& $filters['status'] !=0 && !empty($filters['status'])) {
            $where[] = array(['jcp.status','=',$filters['status']]);
        }
        if (isset($filters['grade'])&& !empty($filters['grade'])) {
            $where[] = array(['jcp.grade','=', $filters['grade']]);
        }
       // $where[] = array(['jcp.factor_source', '=', '2']);

      // $organization_id=[1,$filters['organization_id']];

        $list = Db::table('jy_factor jcp')
            ->field('jcp.id,jcp.title,jcp.factor_id,jcp.model,jcp.describtion,jcp.molecule
            ,jcp.denominator,jcp.year,jcp.country,jcp.region,jcp.grade,jcp.status,jcp.factor_source,jcp.create_by,
            jcp.create_time,TRUNCATE(jcp.factor_value,6) factor_value,jcp.mechanism,ju.username')
            ->leftJoin('jy_user ju', 'jcp.modify_by = ju.id')
            ->where($where)
            ->whereNotNull('factor_value')
            ->whereIn('jcp.organization_id',$organization_id)
            ->order('jcp.id', 'desc')
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);


        return $list;
    }
    public function getFactorAll($main_organization_id) {
        $list = Db::table('jy_factor jcp')
            ->field('jcp.title,jcp.language,jcp.title_language,jcp.model,jcp.describtion,jcp.mechanism,
            jcp.mechanism_short,jcp.grade,jcp.year,jcp.country,jcp.region,jcp.file_name,jcp.factor_value,
            jcp.molecule,jcp.denominator,jcp.factor_id,jcp.id,jcp.create_time')
            ->where('jcp.status',1)
            ->where('organization_id',$main_organization_id)
            ->order('jcp.id', 'desc')
            ->select();

        return $list;

    }

    /**
     * addFactor 添加排放因子
     *
     * @param $data
     * @return $add
     */
    public static function addFactor($data) {

        $add = Db::table('jy_factor')->insertAll($data);

        return $add;
    }

    /**
     * changeStatus 禁用/启用因子
     *
     * @return $del
     */
    public static function changeStatus($data) {

        $del = Db::table('jy_factor')->where('id',$data['id'])->update([
            'status'=>$data['status'],
            'del_time'=>date('Y-m-d H:i:s',time())
        ]);

        return $del;
    }
    /**
     * getInfoById 获取详情
     *
     * @return $del
     */
    public static function getInfoById($id) {
        $list = Db::table('jy_factor jcp')
            ->field('jcp.id,jcp.title,jcp.factor_id,jcp.model,jcp.describtion,jcp.describtion,jcp.molecule
            ,jcp.denominator,jcp.year,jcp.country,jcp.region,jcp.grade,jcp.status,jcp.factor_source,jcp.create_by,
            jcp.create_time,jcp.file_name,jcp.mechanism,jcp.uncertainty,jcp.factor_value')
            ->where('jcp.id', (int)$id)
            ->find();

        return $list;
    }
    /**
     * update 修改碳因子库
     *
     * @return $del
     */
    public static function updateFactor($data) {
        $list = Db::table('jy_factor')
            ->save($data);

        return $list;
    }
}