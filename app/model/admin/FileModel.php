<?php


namespace app\model\admin;


use think\facade\Db;

class FileModel extends Db
{
    /**
     * @notes 通过id获取文件信息详情
     * 
     * @author wuyinghua
     * @param $id
     * @return array|mixed|Db|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getInfoById($id) {
        $list = Db::table('jy_file')
            ->field('id, file_path, source_name as name')
            ->where('id', $id)
            ->find();

        return $list;
    }
}