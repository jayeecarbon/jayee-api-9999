<?php

namespace app\model\admin;

use think\facade\Db;

class IndustryModel extends Db
{
    /**
     * getList 查看行业列表
     * 
     * @author wuyinghua
	 * @return $list
     */
    public static function getList() {
        $list = Db::table('jy_industry')
            ->select()
            ->toArray();

        return $list;
    }
}