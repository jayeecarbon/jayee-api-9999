<?php
declare (strict_types = 1);

namespace app\model\data;

use app\model\data\ParamModel;
use think\facade\Db;

/**
 * ProductEmissionModel
 */
class ProductEmissionModel extends Db {

    /**
     * getCalculates 查询产品核算列表
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getCalculates($page_size, $page_index, $filters) {
        $where = [];

        if ($filters['filter_product_name']) {
            $where[] = array(['jp.product_name', 'like', '%' . trim($filters['filter_product_name']) . '%']);
        }

        if ($filters['main_organization_id']) {
            $where[] = array(['jpc.main_organization_id', '=', $filters['main_organization_id']]);
        }

        $where[] = array(['jpc.is_del', '=', ParamModel::IS_DEL_NO]);
        $where[] = array(['jpc.state', '=', ParamModel::STATE_COMPLETED]);

        $list = Db::table('jy_product_calculate jpc')
            ->field('jpc.id, jpc.product_id, jp.product_name, jp.product_no, jp.product_spec, jyu.name unit_str, 0 + CAST(jpc.coefficient AS CHAR) coefficient')
            ->field('jpc.week_start, jpc.week_end, CONCAT_WS("-", jpc.week_start, jpc.week_end) week')
            ->leftJoin('jy_product jp', 'jp.id = jpc.product_id')
            ->leftJoin('jy_unit jyu', 'jyu.id = jpc.unit')
            ->where($where)
            ->order(['jpc.modify_time'=>'desc', 'jpc.create_time'=>'desc'])
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * getProductDataById 获取产品排放分析产品详情（通过主键id来获取）
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getProductDataById($id) {

        $list = Db::table('jy_product_calculate jpc')
            ->field('jpc.id, jp.product_name, jp.product_no, jp.product_spec, ju.name unit_str, CONCAT_WS("-", jpc.week_start, jpc.week_end) week, 0 + CAST(jpc.coefficient AS CHAR) coefficient, 0 + CAST(jpc.emissions AS CHAR) emissions')
            ->leftJoin('jy_product jp', 'jp.id = jpc.product_id')
            ->leftJoin('jy_unit ju', 'ju.id = jpc.unit')
            ->where('jpc.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getProductDataByCategory 通过类型获取产品排放分析
     *
     * @author wuyinghua
     * @param $id
     * @param $category
     * @return $list
     */
    public static function getProductDataByCategory($id, $category) {

        $list = Db::table('jy_product_data jpd')
            ->field('jpd.id, jpd.name, 0 + CAST(jpd.emissions AS CHAR) emissions')
            ->where(['jpd.product_calculate_id'=>(int)$id, 'jpd.category'=>(int)$category])
            ->order('emissions', 'desc')
            ->select();

        return $list;
    }

    /**
     * getProductDataByCategories 通过多个类型获取产品排放分析
     *
     * @author wuyinghua
     * @param $id
     * @param $categories
     * @return $list
     */
    public static function getProductDataByCategories($id, $categories) {

        $list = Db::table('jy_product_data jpd')
            ->field('jpd.id, jpd.name, SUM(0 + CAST(jpd.emissions AS CHAR)) emissions')
            ->group('jpd.name')
            ->where(['jpd.product_calculate_id'=>(int)$id])
            ->whereIn('jpd.category', $categories)
            ->order('emissions', 'desc')
            ->select();

        return $list;
    }

    /**
     * getProductDataBySuppliers 通过多个供应商获取产品排放分析
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getProductDataBySuppliers($id) {

        $list = Db::table('jy_product_data jpd')
            ->field('jpd.id, jo.name, COUNT(jpd.supplier_id) count, SUM(0 + CAST(jpd.emissions AS CHAR)) emissions')
            ->leftJoin('jy_organization jo', 'jo.id = jpd.supplier_id')
            ->group('jpd.supplier_id')
            ->where('jpd.product_calculate_id', (int)$id)
            ->where('jpd.supplier_id', '<>', NULL)
            ->order('emissions', 'desc')
            ->select();

        return $list;
    }

    /**
     * getProductDataByStages 通过阶段获取产品排放量
     *
     * @author wuyinghua
     * @param $id
     * @param $data_stages
     * @return $list
     */
    public static function getProductDataByStages($id, $data_stages) {

        $list = Db::table('jy_product_data jpd')
            ->field('jpd.id,jds.name, SUM(0 + CAST(jpd.emissions AS CHAR)) emissions')
            ->leftJoin('jy_data_stage jds', 'jds.id = jpd.data_stage')
            ->group('jds.name')
            ->where('jpd.product_calculate_id', (int)$id)
            ->whereIn('jpd.data_stage', $data_stages)
            ->order('jpd.data_stage', 'asc')
            ->select();

        return $list;
    }


    /**
     * getProductDataByStage 通过阶段获取产品排放量
     *
     * @author wuyinghua
     * @param $id
     * @param $data_stage
     * @return $list
     */
    public static function getProductDataByStage($id, $data_stage) {

        $list = Db::table('jy_product_data jpd')
            ->field('SUM(0 + CAST(jpd.emissions AS CHAR)) emissions')
            ->where('jpd.product_calculate_id', (int)$id)
            ->where('jpd.data_stage', (int)$data_stage)
            ->find();

        return $list;
    }

    /**
     * getStages 获取核算所有的阶段
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getStages($id) {

        $list = Db::table('jy_product_data jpd')
            ->field('jpd.data_stage, jds.name')
            ->leftJoin('jy_data_stage jds', 'jds.id = jpd.data_stage')
            ->group('jpd.data_stage')
            ->where('jpd.product_calculate_id', (int)$id)
            ->select();

        return $list;
    }

    /**
     * getStageName 获取阶段名称
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getStageName($id) {

        $list = Db::table('jy_data_stage jds')
            ->field('jds.id, jds.name')
            ->where('jds.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getAllcalculate 获取所有核算数据
     * 
     * @author wuyinghua
	 * @return $list
     */
    public static function getAllcalculate($filters) {
        $where[] = array(['jpc.is_del', '=', ParamModel::IS_DEL_NO]);
        $where[] = array(['jpc.state', '=', ParamModel::STATE_COMPLETED]);
        // $where[] = array(['jpc.main_organization_id', '=', $filters['main_organization_id']]);
        $list = Db::table('jy_product_calculate jpc')
            ->field('jpc.id, jpc.product_id, jp.product_name, jp.product_no, jp.product_spec, jyu.name unit_str, 0 + CAST(jpc.coefficient AS CHAR) coefficient')
            ->field('jpc.week_start, jpc.week_end, CONCAT_WS("-", jpc.week_start, jpc.week_end) week')
            ->leftJoin('jy_product jp', 'jp.id = jpc.product_id')
            ->leftJoin('jy_unit jyu', 'jyu.id = jpc.unit')
            ->where($where)
            ->order(['jpc.modify_time'=>'desc', 'jpc.create_time'=>'desc'])
            ->select();

        return $list;
    }
}
