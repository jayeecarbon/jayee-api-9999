<?php
declare (strict_types = 1);

namespace app\model\organization;

use app\model\data\ParamModel;
use think\facade\Db;

/**
 * @mixin \think\Model
 */
class CalculateReportModel extends Db
{
    /**
     * getCalculateReports 查询核算报告列表
     * 
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getCalculateReports($page_size, $page_index, $filters) {
        $where = [];

        if (isset($filters['organization_id']) && $filters['organization_id'] != 0) {
            $where[] = array(['jocr.organization_id', '=', $filters['organization_id']]);
        }

        if (isset($filters['calculate_year']) && $filters['calculate_year'] != '') {
            $where[] = array(['jocr.calculate_year', '=', $filters['calculate_year']]);
        }

        if (isset($filters['report_name']) && $filters['report_name'] != '') {
            $where[] = array(['jocr.report_name', 'like', '%' . trim($filters['report_name']) . '%']);
        }

        $where[] = array(['jocr.main_organization_id', '=', $filters['main_organization_id']]);

        $list = Db::table('jy_organization_calculate_report jocr')
            ->field('jocr.id, jocr.report_name, joc.calculate_year, 0 + CAST(joc.emissions AS CHAR) actual_emissions, 0 + CAST(jd.total AS CHAR) base_emissions, jocr.organization_id, jo.name organization_name, ju.username, jocr.modify_time')
            ->leftJoin('jy_organization jo', 'jocr.organization_id = jo.id')
            ->leftJoin('jy_user ju', 'jocr.modify_by = ju.id')
            ->leftJoin('jy_datum jd', 'jocr.datum_id = jd.id')
            ->leftJoin('jy_organization_calculate joc', 'jocr.organization_calculate_id = joc.id')
            ->where($where)
            ->order(['jocr.modify_time'=>'desc', 'jocr.create_time'=>'desc'])
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }

    /**
     * addCalculateReport 添加核算报告
     * 
     * @author wuyinghua
     * @param $data
	 * @return $add
     */
    public static function addCalculateReport($data) {

        Db::startTrans();
        try {
            $add_id = Db::table('jy_organization_calculate_report')->insertGetId([
                'main_organization_id'      => (int)$data['main_organization_id'],
                'report_name'               => $data['report_name'],
                'organization_id'           => (int)$data['organization_id'],
                'organization_calculate_id' => (int)$data['organization_calculate_id'],
                'data_quality_id'           => (int)$data['data_quality_id'],
                'datum_id'                  => (int)$data['datum_id'],
                'username'                  => $data['username'],
                'telephone'                 => $data['telephone'],
                'email'                     => $data['email'],
                'exempte_description'       => $data['exempte_description'],
                'change_description'        => $data['change_description'],
                'discharge_description'     => $data['discharge_description'],
                'other_description'         => $data['other_description'],
                'create_by'                 => (int)$data['userid'],
                'create_time'               => date('Y-m-d H:i:s'),
                'modify_by'                 => (int)$data['userid'],
                'modify_time'               => date('Y-m-d H:i:s')
            ]);

            // 更新核算报告关联减排场景表对应的核算报告ID
            Db::table('jy_organization_calculate_report_reduction')->where('virtual_id', $data['virtual_id'])->update(['organization_calculate_report_id' => $add_id]);
            // 清除virtual_id
            Db::table('jy_organization_calculate_report_reduction')->where('virtual_id', $data['virtual_id'])->update(['virtual_id' => NULL]);

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
        }

    /**
     * editCalculateReport 编辑核算报告
     * 
     * @author wuyinghua
     * @param $data
	 * @return $edit
     */
    public static function editCalculateReport($data) {

        $edit = Db::table('jy_organization_calculate_report')->where('id', (int)$data['id'])->update([
            'main_organization_id'      => (int)$data['main_organization_id'],
            'report_name'               => $data['report_name'],
            'organization_id'           => (int)$data['organization_id'],
            'organization_calculate_id' => (int)$data['organization_calculate_id'],
            'data_quality_id'           => (int)$data['data_quality_id'],
            'datum_id'                  => (int)$data['datum_id'],
            'username'                  => $data['username'],
            'telephone'                 => $data['telephone'],
            'email'                     => $data['email'],
            'exempte_description'       => $data['exempte_description'],
            'change_description'        => $data['change_description'],
            'discharge_description'     => $data['discharge_description'],
            'other_description'         => $data['other_description'],
            'modify_by'                 => (int)$data['userid'],
            'modify_time'               => date('Y-m-d H:i:s')
        ]);

        return $edit;
    }

    /**
     * getCalculateReport 获取核算报告数据
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getCalculateReport($id) {

        $list = Db::table('jy_organization_calculate_report jocr')
            ->field('jocr.*, joc.calculate_year, joc.contrast_year, jo.name organization_name, ju.username')
            ->field('job.organization_setting, job.organization_desc, job.organization_change')
            ->field('joi.business_desc, joi.manage_build_desc, joi.manage_build, joi.organization_file_desc, joi.organization_file')
            ->field('jd.type datum_type, jd.start_year, jd.end_year, jd.policy datum_policy, 0 + CAST(jd.total AS CHAR) datum_total, jg.en_short_name')
            ->leftJoin('jy_organization jo', 'jocr.organization_id = jo.id')
            ->leftJoin('jy_user ju', 'jocr.modify_by = ju.id')
            ->leftJoin('jy_datum jd', 'jocr.datum_id = jd.id')
            ->leftJoin('jy_organization_calculate joc', 'jocr.organization_calculate_id = joc.id')
            ->leftJoin('jy_organization_info joi', 'jocr.organization_id = joi.organization_id')
            ->leftJoin('jy_organizational_boundaries job', 'jocr.organization_id = job.organization_id')
            ->leftJoin('jy_calculate_model jom', 'joc.calculate_model_id = jom.id')
            ->leftJoin('jy_gwp jg', 'jom.gwp_id = jg.id')
            ->where('jocr.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getCalculateCountGas 获取碳核算- 排放量计算
     *
     * @author wuyinghua
     * @param $organization_calculate_id
     * @return $list
     */
    public static function getCalculateCountGas($organization_calculate_id) {

        $list = Db::table('jy_organization_calculate_count_gas joccg')
            ->where('joccg.organization_calculate_id', (int)$organization_calculate_id)
            ->select();

        return $list;
    }

    /**
     * delCalculateReport 删除设备
     * 
     * @author wuyinghua
     * @param $id
	 * @return $del
     */
    public static function delCalculateReport($id) {
        Db::startTrans();
        try {
            // 删除核算报告
            Db::table('jy_organization_calculate_report')->where('id', (int)$id)->delete();

            // 删除关联的减排场景
            Db::table('jy_organization_calculate_report_reduction')->where('organization_calculate_report_id', (int)$id)->delete();
            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * getAllCalculate 获取所有核算
     *
     * @author wuyinghua
     * @param $organization_id
     * @return $list
     */
    public static function getAllCalculate($organization_id) {

        $list = Db::table('jy_organization_calculate')
            ->field('id, calculate_name, calculate_year')
            ->where('organization_id', (int)$organization_id)
            ->where(['organization_id'=>(int)$organization_id, 'state'=>ParamModel::ORG_STATE_COMPLETED])
            ->select();

        return $list;
    }

    /**
     * getCalculateByContrastYear 根据组织和对比年查找完成的组织核算
     *
     * @author wuyinghua
     * @param $organization_id
     * @param $contrast_year
     * @return $list
     */
    public static function getCalculateByContrastYear($organization_id, $contrast_year) {

        $list = Db::table('jy_organization_calculate')
            ->where(['organization_id'=>(int)$organization_id, 'calculate_year'=>$contrast_year, 'state'=>ParamModel::ORG_STATE_COMPLETED])
            ->find();

        return $list;
    }

    /**
     * getAllDataQuality 获取所有数据质量控制版本
     *
     * @author wuyinghua
     * @param $organization_id
     * @return $list
     */
    public static function getAllDataQuality($organization_id) {

        $list = Db::table('jy_data_quality')
            ->field('id, version')
            ->where('organization_id', (int)$organization_id)
            ->select();

        return $list;
    }

    /**
     * getAllDatum 获取所有排放基准
     *
     * @author wuyinghua
     * @param $organization_id
     * @return $list
     */
    public static function getAllDatum($organization_id) {

        $list = Db::table('jy_datum')
            ->field('id, start_year, end_year')
            ->where('organization_id', (int)$organization_id)
            ->select();

        return $list;
    }

    /**
     * getReduction 获取减排场景信息
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getReduction($id) {

        $list = Db::table('jy_organization_calculate_report_reduction jr')
        ->field('jr.id, jr.organization_id, jr.type, jo.name organization_name, jr.name, 0 + CAST(jr.total_reduction_low AS CHAR) total_reduction_low, 0 + CAST(jr.total_reduction_high AS CHAR) total_reduction_high,
        jr.total_unit, 0 + CAST(jr.unit_low AS CHAR) unit_low, 0 + CAST(jr.unit_high AS CHAR) unit_high, jr.molecule, jr.denominator, jr.desc')
            ->leftJoin('jy_organization jo', 'jr.organization_id = jo.id')
            ->where('jr.organization_calculate_report_id', $id)
            ->select();

        return $list;
    }

    /**
     * getGases 全球增温潜势值,来源：【碳核算选择的GWP信息】
     *
     * @author wuyinghua
     * @return $list
     */
    public static function getGases() {

        $list = Db::table('jy_gases jr')->select();

        return $list;
    }

    /**
     * getGwps 获取gwp信息
     *
     * @author wuyinghua
     * @return $list
     */
    public static function getGwps() {

        $list = Db::table('jy_gwp')
            ->field('id, name gwp_name, en_short_name, short_name')
            ->select();

        return $list;
    }

    /**
     * getReductionByVirtual 通过虚拟ID获取减排场景信息
     *
     * @author wuyinghua
     * @param $id
     * @param $organization_id
     * @return $list
     */
    public static function getReductionByVirtual($id, $organization_id) {

        $list = Db::table('jy_organization_calculate_report_reduction jr')
            ->field('jr.id, jr.organization_id, jr.type, jo.name organization_name, jr.name, 0 + CAST(jr.total_reduction_low AS CHAR) total_reduction_low, 0 + CAST(jr.total_reduction_high AS CHAR) total_reduction_high,
            jr.total_unit, 0 + CAST(jr.unit_low AS CHAR) unit_low, 0 + CAST(jr.unit_high AS CHAR) unit_high, jr.molecule, jr.denominator, jr.desc')
            ->leftJoin('jy_organization jo', 'jr.organization_id = jo.id')
            ->where(['jr.virtual_id'=>$id, 'jr.organization_id'=>(int)$organization_id])
            ->select();

        return $list;
    }

    /**
     * getReductionById 获取减排场景信息
     *
     * @author wuyinghua
     * @param $id
     * @return $list
     */
    public static function getReductionById($id) {

        $list = Db::table('jy_organization_calculate_report_reduction jr')
            ->field('jr.id, jr.organization_id, jo.name organization_name, jr.name, jr.total_reduction_low, jr.total_reduction_high,
            jr.total_unit, jr.unit_low, jr.unit_high, jr.molecule, jr.denominator, jr.desc')
            ->leftJoin('jy_organization jo', 'jr.organization_id = jo.id')
            ->where('jr.id', $id)
            ->find();

        return $list;
    }

    /**
     * getReductionByReportId 获取减排场景信息
     *
     * @author wuyinghua
     * @param $id
     * @param $report_id
     * @param $virtual_id
     * @return $list
     */
    public static function getReductionByReportId($id, $report_id, $virtual_id) {

        if ($report_id == '') {
            $list = Db::table('jy_organization_calculate_report_reduction')->where(['reduction_id'=>(int)$id, 'virtual_id'=>$virtual_id])->find();
        } else {
            $list = Db::table('jy_organization_calculate_report_reduction')->where(['reduction_id'=>(int)$id, 'organization_calculate_report_id'=>(int)$report_id])->find();
        }

        return $list;
    }

    /**
     * delReduction 删除减排场景
     * 
     * @author wuyinghua
     * @param $id
	 * @return $del
     */
    public static function delReduction($id) {
        $del = Db::table('jy_organization_calculate_report_reduction')->where('id', (int)$id)->delete();

        return $del;
    }

    /**
     * getReductions 获取减排场景信息
     *
     * @author wuyinghua
     * @param $page_ize
     * @param $page_index
     * @param $filters
     * @return $list
     */
    public static function getReductions($page_ize, $page_index, $filters) {
        $where = [];
        if (isset($filters['reduction_name']) && $filters['reduction_name'] != '') {
            $where[] = array(['jr.name', 'like', '%' . trim($filters['reduction_name']) . '%']);
        }

        $where[] = array(['jr.main_organization_id', '=', $filters['main_organization_id']]);
        $where[] = array(['jr.organization_id', '=', $filters['organization_id']]);

        $list = Db::table('jy_reduction jr')
            ->field('jr.id, jr.organization_id, jo.name organization_name, jr.name, jr.desc')
            ->leftJoin('jy_organization jo', 'jr.organization_id = jo.id')
            ->where($where)
            ->order(['jr.modify_time'=>'desc', 'jr.create_time'=>'desc'])
            ->paginate(['list_rows'=>$page_ize, 'page'=>$page_index]);

        return $list;
    }

    /**
     * reductionChoice 选择
     * 
     * @author wuyinghua
     * @param $data
	 * @return $copy
     */
    public static function reductionChoice($data) {
        Db::startTrans();
        try {
            foreach ($data['ids'] as $id) {
                $list = Db::table('jy_reduction')->where('id', (int)$id)->find();
                Db::table('jy_organization_calculate_report_reduction')
                ->insertGetId([
                    'reduction_id'                     => $id,
                    'organization_calculate_report_id' => $data['report_id'],
                    'organization_id'                  => (int)$list['organization_id'],
                    'virtual_id'                       => $data['virtual_id'],
                    'name'                             => $list['name'],
                    'type'                             => $list['type'],
                    'desc'                             => $list['desc'],
                    'total_desc'                       => $list['total_desc'],
                    'reduction_type'                   => $list['reduction_type'],
                    'total_reduction_low'              => $list['total_reduction_low'],
                    'total_reduction_high'             => $list['total_reduction_high'],
                    'total_unit'                       => (int)$list['total_unit'],
                    'unit_desc'                        => $list['unit_desc'],
                    'unit_type'                        => $list['unit_type'],
                    'unit_low'                         => $list['unit_low'],
                    'unit_high'                        => $list['unit_high'],
                    'molecule'                         => (int)$list['molecule'],
                    'denominator'                      => $list['denominator'],
                    'create_by'                        => (int)$data['userid'],
                    'create_time'                      => date('Y-m-d H:i:s'),
                    'modify_by'                        => (int)$data['userid'],
                    'modify_time'                      => date('Y-m-d H:i:s')
                ]);
            }

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * ghgList GHG列表
     * 
     * @author wuyinghua
     * @param $quality_id
	 * @return $list
     */
    public static function ghgList($quality_id)
    {
        $list = Db::table('jy_ghg')
            ->field('name,id,pid')
            ->where(['type'=>1,'pid'=>0,'quality_id'=>$quality_id])
            ->order('id asc')
            ->select()
            ->toArray();
        foreach ($list as $k=>$v){
            $children = Db::table('jy_ghg')
                ->field('name,id,pid,desc,active_desc,collection_instructions,calculation_description,data_description')
                ->where(['type'=>1,'pid'=>$v['id']])
                ->order('id asc')
                ->select()
                ->toArray();
            $v['children'] = $children;
            $list[$k] = $v;
        }
        return $list;
    }

    /**
     * ghgList GHG列表
     * 
     * @author wuyinghua
     * @param $quality_id
	 * @return $list
     */
    public static function isoList($quality_id)
    {

        $list = Db::table('jy_ghg')
            ->field('name,id,pid')
            ->where(['type'=>2,'pid'=>0,'quality_id'=>$quality_id])
            ->order('id asc')
            ->select()
            ->toArray();

        foreach ($list as $k=>$v){
            $children = Db::table('jy_ghg')
                ->field('name,id,pid,desc,active_desc,collection_instructions,calculation_description,data_description')
                ->where(['type'=>2,'pid'=>$v['id']])
                ->order('id asc')
                ->select()
                ->toArray();
            $v['children'] = $children;
            $list[$k] = $v;
        }
        return $list;
    }
}
