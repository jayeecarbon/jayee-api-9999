<?php

namespace app\model\quality;

use think\facade\Db;


/**
 * QualityModel
 */
class QualityModel extends Db
{
    /**
     * getList 查询列表
     *
     * @param $page_ize
     * @param $page_index
     * @param $filters
     * @return $list
     */
    public function getList($page_size, $page_index, $filters)
    {
        $where = array();
        if (isset($filters['version']) && !empty($filters['version'])) {
            $where[] = array(['r.version', 'like',  trim(  '%'.$filters['version']) . '%']);
        }
        if (isset($filters['organization_id']) && !empty($filters['organization_id'])) {
            $where[] = array(['r.organization_id', '=', $filters['organization_id']]);
        }else{
            $where[] = array(['r.main_organization_id', '=', $filters['main_organization_id']]);
        }

        $list = Db::table('jy_data_quality r')
            ->field('o.name organization_name,r.id,r.version,r.development_time,r.content,r.remark,
            r.modify_time,r.modify_by,u.username,o.id organization_id')
            ->leftJoin('jy_organization o', 'r.organization_id=o.id')
            ->leftJoin('jy_user u','u.id=r.modify_by')
            ->where($where)
            ->order('r.modify_time', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    public static function add($data)
    {
       $has_exist =  Db::table('jy_data_quality')->where(['organization_id'=>$data['organization_id'],'version'=>$data['version']])->find();
//       return $has_exist;
       if($has_exist){
           $datasmg['code'] = 404;
           $datasmg['message'] = "添加的版本相同";
           return json($datasmg);
       }
       $add_id = Db::table('jy_data_quality')->insertGetId($data);
        self::addghg($data['main_organization_id'],$data['organization_id'],$add_id);
        if($add_id){
            $datasmg['code'] = 200;
            $datasmg['message'] = "添加成功";
            return json($datasmg);
        }else{
            $datasmg['code'] = 404;
            $datasmg['message'] = "添加失败";
            return json($datasmg);
        }

    }

    public static function getInfoById($id)
    {
        $list = Db::table('jy_data_quality r')
            ->where('r.id', (int)$id)
            ->find();
        return $list;
    }

    public static function copy($id,$main_organization_id,$userid)
    {
        $list = Db::table('jy_data_quality')
            ->field('organization_id,version,development_time,content,remark')
            ->where('id', (int)$id)
            ->find();
        $list['create_by'] = $userid;
        $list['version'] = $list['version'];
        $list['main_organization_id'] = $main_organization_id;
        $list['create_time'] = date('Y-m-d H:i:s');
        $list['modify_by'] =$userid;
        $list['modify_time'] = date('Y-m-d H:i:s');
        $add_id = Db::table('jy_data_quality')->insertGetId($list);
        self::addghg($main_organization_id,$list['organization_id'],$add_id);
        return $add_id;
    }

    public static function edit($data)
    {
        $info = Db::table('jy_data_quality')
            ->where('id',$data['id'])->find();
        $has_exist =  Db::table('jy_data_quality')
            ->where([
                'organization_id'=>$info['organization_id'],
                'version'=>$data['version']])
            ->where('id','<>',$data['id'])->find();
//       return $has_exist;
        if($has_exist){
            $datasmg['code'] = 404;
            $datasmg['message'] = "添加的版本相同";
            return json($datasmg);
        }
            $update = Db::table('jy_data_quality')
                ->save($data);
        if($update){
            $datasmg['code'] = 200;
            $datasmg['message'] = "编辑成功";
            return json($datasmg);
        }else{
            $datasmg['code'] = 404;
            $datasmg['message'] = "编辑失败";
            return json($datasmg);
        }

    }
    public static function del($id)
    {
        $del = Db::table('jy_data_quality')->where('id', $id)->delete();
        if($del){
            return true;
        }else{
            return false;
        }
    }

    //组织基本信息
    public static function organizationalEdit($data)
    {
        if($data['id']!=0){
            $update = Db::table('jy_organization_info')
                ->save($data);
        }else{
            unset($data['id']);
            $update = Db::table('jy_organization_info')
                ->insert($data);
        }
        if($update){
            return true;
        }
        return false;
    }

    public static function organizationalInfo($quality_id)
    {
        $list = Db::table('jy_organization_info')
            ->where('quality_id',$quality_id)
            ->find();
        if($list){
            $organization_files = $list['organization_file'];
            $manage_builds = $list['manage_build'];
            $organization_file_list = Db::table('jy_file')->field('id,file_path,source_name')->whereIn('id',$organization_files)->select()->toArray();
            $manage_build_list = Db::table('jy_file')->field('id,file_path,source_name')->whereIn('id',$manage_builds)->select()->toArray();
            if($organization_file_list){
                foreach ($organization_file_list as $k=>$v){
                    $v['file_path'] = 'http://47.122.18.241:81'.$v['file_path'];
                    $organization_file_list[$k]=$v;
                }
                $list['organization_file'] =$organization_file_list;
            }
            if($manage_build_list){
                foreach ($manage_build_list as $k=>$v){
                    $v['file_path'] = 'http://47.122.18.241:81'.$v['file_path'];
                    $manage_build_list[$k]=$v;
                }
                $list['manage_build'] =$manage_build_list;
            }

        }


        return $list;
    }

    //组织边界
    public static function organizationalBoundariesEdit($data)
    {
        if($data['id']!=0){
            $update = Db::table('jy_organizational_boundaries')
                ->save($data);
        }else{
            $update = Db::table('jy_organizational_boundaries')
                ->insert([
                    'organization_setting'=>$data['organization_setting'],
                    'organization_desc'=>$data['organization_desc'],
                    'organization_change'=>$data['organization_change'],
                    'main_organization_id'=>$data['main_organization_id'],
                    'organization_id'=>$data['organization_id'],
                    'quality_id'=>$data['quality_id'],
                    ]);
        }

        if($update){
            return true;
        }
        return false;
    }

    public static function organizationalBoundariesInfo($quality_id)
    {

        $list = Db::table('jy_organizational_boundaries')
            ->where('quality_id',$quality_id)
            ->find();
        return $list;
    }

    //ghg
    public function ghgList($quality_id)
    {
        $list = Db::table('jy_ghg')
            ->field('name,id,pid')
            ->where(['type'=>1,'pid'=>0,'quality_id'=>$quality_id])
            ->order('id asc')
            ->select()
            ->toArray();
        foreach ($list as $k=>$v){
            $children = Db::table('jy_ghg')
                ->field('name,id,pid,desc,active_desc')
                ->where(['type'=>1,'pid'=>$v['id']])
                ->order('id asc')
                ->select()
                ->toArray();
            $v['children'] = $children;
            $list[$k] = $v;
        }
        return $list;
    }

    public static function ghgedit($data)
    {
        $update = Db::table('jy_ghg')
            ->save($data);
        if($update){
            return true;
        }
        return false;

    }

    public static function ghgInfo($id)
    {
        $list = Db::table('jy_ghg r')
            ->where('r.id', (int)$id)
            ->find();
        $partent= Db::table('jy_ghg')
            ->where('id',$list['pid'] )
            ->find();
        $list['cate']=$partent['name'];
        return $list;
    }

    //iso
    public function isoList($quality_id)
    {

        $list = Db::table('jy_ghg')
            ->field('name,id,pid')
            ->where(['type'=>2,'pid'=>0,'quality_id'=>$quality_id])
            ->order('id asc')
            ->select()
            ->toArray();

        foreach ($list as $k=>$v){
            $children = Db::table('jy_ghg')
                ->field('name,id,pid,desc,active_desc')
                ->where(['type'=>2,'pid'=>$v['id']])
                ->order('id asc')
                ->select()
                ->toArray();
            $v['children'] = $children;
            $list[$k] = $v;
        }
        return $list;
    }

    public static function isoedit($data)
    {
        $update = Db::table('jy_ghg')
            ->save($data);
        if($update){
            return true;
        }
        return false;
    }

    public static function isoInfo($id)
    {
        $list = Db::table('jy_ghg r')
            ->where('r.id', (int)$id)
            ->find();
        $partent= Db::table('jy_ghg')
            ->where('id',$list['pid'] )
            ->find();
        $list['cate']=$partent['name'];
        return $list;
    }


    //数据质量管理规定
    public static function dataQualityRoleEdit($data)
    {
        if($data['id']!=0){
            $update = Db::table('jy_data_quality_role')
                ->save($data);
        }else{
            $update = Db::table('jy_data_quality_role')
                ->insert([
                    'content'=>$data['content'],
                    'main_organization_id'=>$data['main_organization_id'],
                    'organization_id'=>$data['organization_id'],
                    'quality_id'=>$data['quality_id'],
                    ]);
        }

        if($update){
            return true;
        }
        return false;
    }

    public static function dataQualityRoleInfo($quality_id)
    {

        $list = Db::table('jy_data_quality_role')
            ->where('quality_id',$quality_id)
            ->find();

        return $list;
    }

    public static function organizationList($id)
    {

        $list = Db::table('jy_organization')
            ->field('id,name')
            ->where('main_organization_id',$id)
            ->select()
            ->toArray();
        return $list;
    }

    public static function addghg($main_organization_id,$organization_id,$add_id){
        $list = Db::table('jy_ghg_template')
            ->field('name,id')
            ->where(['type'=>1,'pid'=>0])
            ->order('id asc')
            ->select()
            ->toArray();
        foreach ($list as $k=>$v){
            $children = Db::table('jy_ghg_template')
                ->field('name,desc')
                ->where(['pid'=>$v['id']])
                ->order('id asc')
                ->select()
                ->toArray();
            $insert_id =  Db::table('jy_ghg')->insertGetId([
                'type'=>1,
                'name'=>$v['name'],
                'pid'=>0,
                'quality_id'=>$add_id,
                'main_organization_id'=>$main_organization_id,
                'organization_id'=>$organization_id
            ]);
            foreach ($children as $kk=>$vv){
                $vv['pid'] = $insert_id;
                $vv['type']=1;
                $vv['quality_id']=$add_id;
                $vv['main_organization_id']=$main_organization_id;
                $vv['organization_id']=$organization_id;
                $children[$kk]=$vv;
            }
            Db::table('jy_ghg')->insertAll($children);

        }

        $list_iso = Db::table('jy_ghg_template')
            ->field('name,id')
            ->where(['type'=>2,'pid'=>0])
            ->order('id asc')
            ->select()
            ->toArray();
        foreach ($list_iso as $k=>$v){
            $children_iso = Db::table('jy_ghg_template')
                ->field('name,desc')
                ->where(['pid'=>$v['id']])
                ->order('id asc')
                ->select()
                ->toArray();
            $insert_id_iso =  Db::table('jy_ghg')->insertGetId([
                'type'=>2,
                'name'=>$v['name'],
                'pid'=>0,
                'quality_id'=>$add_id,
                'main_organization_id'=>$main_organization_id,
                'organization_id'=>$main_organization_id
            ]);
            foreach ($children_iso as $kk=>$vv){
                $vv['pid'] = $insert_id_iso;
                $vv['type']=2;
                $vv['quality_id']=$add_id;
                $vv['main_organization_id'] = $main_organization_id;
                $vv['organization_id'] = $main_organization_id;
                $children_iso[$kk]=$vv;
            }
            Db::table('jy_ghg')->insertAll($children_iso);

        }
        return true;
    }
}