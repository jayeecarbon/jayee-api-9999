<?php


namespace app\model\supply;


use think\facade\Db;

class SupplierCustomerRelationModel extends Db
{
    /**
     * 主体是供应商-是
     */
    CONST ID_SUPPLIER_YES = 1;
    /**
     * 主体是供应商-不是
     */
    CONST ID_SUPPLIER_NO = 0;
    /**
     * 主体是客户-是
     */
    CONST IS_CUSTOMER_YES = 1;
    /**
     * 主体是客户-不是
     */
    CONST IS_CUSTOMER_NO = 0;
}