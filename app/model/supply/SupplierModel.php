<?php


namespace app\model\supply;


use think\facade\Db;

class SupplierModel extends Db
{
    /**
     * 是否使用-未使用
     */
    CONST IS_USE_NO = 1;
    /**
     * 是否使用-已使用
     */
    CONST IS_USE_YES = 2;
    /**
     * 是否主体-是
     */
    CONST IS_MAIN_YES = 1;
    /**
     * 是否主体-否
     */
    CONST IS_MAIN_NO = 0;
    /**
     * 是否确认被邀请-接受
     */
    CONST CONFIRM_TYPE_YES = 1;
    /**
     * 是否确认被邀请-拒绝
     */
    CONST CONFIRM_TYPENO = 0;
    /**
     * 启用/禁用-启用
     */
    CONST STATE_YES = 1;
    /**
     * 启用/禁用-禁用
     */
    CONST STATE_NO = 2;
    /**
     * 风险状态-正常
     */
    CONST RISK_STATE_YES = 1;
    /**
     * 风险状态-风险
     */
    CONST RISK_STATE_NO = 2;

    CONST pageSize = 10;

    CONST pageIndex = 0;

    CONST STATE_MAP = [
        self::STATE_YES => '启用',
        self::STATE_NO => '禁用',
    ];

    CONST RISK_STATE_MAP = [
        self::RISK_STATE_YES => '正常',
        self::RISK_STATE_NO => '风险',
    ];

    CONST STATE_SELECT_MAP = [
        ['id'=>self::STATE_YES, 'name'=>'启用'],
        ['id'=>self::STATE_NO, 'name'=>'禁用']
    ];

    CONST RISK_STATE_SELECT_MAP = [
        ['id'=>self::RISK_STATE_YES, 'name'=>'正常'],
        ['id'=>self::RISK_STATE_NO, 'name'=>'风险']
    ];

    /**
     * list 查询客户的供应商列表
     *
     * @param $page_ize
     * @param $page_index
     * @param $filters
     * @return $list
     */
    public static function getList($page_size, $page_index, $filters) {
        $where = array();
        if (isset($filters['title']) && $filters['title'] != '') {
            $where[] = array(['jo.name', 'like', '%' . trim($filters['title']) . '%']);
        }
        if (isset($filters['state']) &&  $filters['state'] != '') {
            $where[] = array(['jscr.state', '=', trim($filters['state'])]);
        }
        if (isset($filters['risk_state']) &&  $filters['risk_state'] != '') {
            $where[] = array(['jscr.risk_state', '=', trim($filters['risk_state'])]);
        }
        if (isset($filters['industry_id']) &&  $filters['industry_id'] != '') {
            $where[] = array(['jo.industry_id', '=', trim($filters['industry_id'])]);
        }

        $list = Db::table('jy_supplier_customer_relation jscr')
            ->field('jo.id, jo.industry_id, jo.name title, jscr.state, jscr.risk_state, ji.name as industry_name')
            ->leftJoin('jy_organization jo', 'jscr.relation_id = jo.id')
            ->leftJoin('jy_industry ji', 'ji.id = jo.industry_id')
            ->where($where)
            ->where('jscr.main_id', (int)$filters['main_id'])
            ->where('jscr.is_customer', (int)SupplierCustomerRelationModel::IS_CUSTOMER_YES)
            ->order('jo.id', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * @getCustomerProductAddFormData 客户端添加产品获取供应商相关信息
     *
     * @param $main_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getCustomerProductAddFormData($main_id) {

        $list = Db::table('jy_supplier_customer_relation jscr')
            ->field('jo.id, jo.name title')
            ->leftJoin('jy_organization jo', 'jscr.relation_id = jo.id')
            ->leftJoin('jy_industry ji', 'ji.id = jo.industry_id')
            ->where('jscr.main_id', (int)$main_id)
            ->where('jscr.is_customer', (int)SupplierCustomerRelationModel::IS_CUSTOMER_YES)
            ->order('jo.id', 'desc')
            ->select()
            ->toArray();

        return $list;
    }

    /**
     * @getRelation 查询供应关系
     *
     * @param $main_id
     * @param $relation_id
     * @return $list
     */
    public static function getRelation($main_id, $relation_id) {

        $list = Db::table('jy_supplier_customer_relation jscr')
            ->where(['jscr.main_id'=>(int)$main_id, 'jscr.relation_id'=>(int)$relation_id, 'jscr.is_supplier'=>self::IS_MAIN_NO])
            ->select();

        return $list;
    }

    /**
     * getDataById 获取供应商的信息（通过id来获取）
     * @param $data
     * @return $list
     */
    public static function getDataById($data) {

        $list = Db::table('jy_organization jo')
            ->field('jo.id, jo.industry_id, jo.name title, jscr.state, jscr.risk_state,
            ji.name as industry_name, jo.contact_person user_name, jo.telephone phone, jo.email,
            jo.create_by, jo.modify_by, jo.create_time, jo.modify_time')
            ->leftJoin('jy_industry ji', 'ji.id = jo.industry_id')
            ->leftJoin('jy_supplier_customer_relation jscr', 'jscr.relation_id = jo.id')
            ->where('jo.id', (int)$data['id'])
            ->where('jscr.main_id', (int)$data['main_id'])
            ->find();

        return $list;
    }

    /**
     * updateState 供应商启用/禁用
     * @param $data
     * @return int
     * @throws \think\db\exception\DbException
     */
    public static function updateState($data) {
        $update_state = Db::table('jy_supplier_customer_relation')->where(['relation_id' => (int)$data['id'], 'main_id' => (int)$data['main_id']])->update(['state' => $data['state']]);

        return $update_state;
    }

    /**
     * updateRiskState 供应商 风险等级变更
     * @param $data
     * @return int
     * @throws \think\db\exception\DbException
     */
    public static function updateRiskState($data) {
        $update_state = Db::table('jy_supplier_customer_relation')->where(['relation_id' => (int)$data['id'], 'main_id' => (int)$data['main_id']])->update(['risk_state' => $data['risk_state']]);

        return $update_state;
    }

    /**
     * delSupplierProduct 删除产品
     *
     * @param $data
     * @return $del
     */
    public static function delSupplierProduct($data) {
        $del = Db::table('jy_supplier_product')->where('id', (int)$data['id'])->update($data);

        return $del;
    }

    /**
     * getSupplierInviteByCode 通过邀请码查询供应商邀请
     * 
     * @param $invite_code
	 * @return $add
     */
    public static function getSupplierInviteByCode($invite_code) {
        $add = Db::table('jy_supplier_invite')->where('invite_code', $invite_code)->find();

        return $add;
    }

    /**
     * addSupplierInvite 添加供应商邀请CODE
     * 
     * @param $data
	 * @return $add
     */
    public static function addSupplierInvite($data) {
        $add = Db::table('jy_supplier_invite')->insert($data);

        return $add;
    }

    /**
     * updateSupplierInvite 供应商邀请
     * @param $data
     * @return int
     * @throws \think\db\exception\DbException
     */
    public static function updateSupplierInvite($data) {
        $update_state = Db::table('jy_supplier_invite')->where('invite_code', $data['invite_code'])->update($data);

        return $update_state;
    }

    /**
     * addRelation 新增供应商采购者关系
     * @param $data
     * @return int
     * @throws \think\db\exception\DbException
     */
    public static function addRelation($data) {
        $add = Db::table('jy_supplier_customer_relation')->insert($data);

        return $add;
    }

    /**
     * getProductCountBySupplierId 采购端-获取供应产品数量通过采购者和供应商的ID
     *
     * @param $customer_id
     * @param $supplier_id
     * @return \think\Paginator
     * @throws \think\db\exception\DbException
     */
    public static function getProductCountBySupplierId($customer_id, $supplier_id) {
        $list = Db::table('jy_customer_product jcp')
            ->field('count(jcp.id) as count')
            ->where(['jcp.customer_id'=>$customer_id, 'jcp.supplier_id'=>$supplier_id])
            ->where('jcp.audit_state', CustomerProductModel::AUDIT_STATE_VERIFY)
            ->group('jcp.supplier_id')
            ->find();

        return $list;
    }

    /**
     * getProductCountByCustomerId 供应端-获取供应产品数量通过采购者和供应商的ID
     *
     * @param $customer_id
     * @param $supplier_id
     * @return \think\Paginator
     * @throws \think\db\exception\DbException
     */
    public static function getProductCountByCustomerId($customer_id, $supplier_id) {
        $list = Db::table('jy_supplier_fill_product_customer_relation jsfpcr')
            ->field('count(jsfpcr.id) as count')
            ->where(['jsfpcr.customer_id'=>$customer_id, 'jsfpcr.supplier_id'=>$supplier_id])
            // ->where('audit_state', CustomerProductModel::AUDIT_STATE_VERIFY)
            ->group('jsfpcr.customer_id')
            ->find();

        return $list;
    }
}