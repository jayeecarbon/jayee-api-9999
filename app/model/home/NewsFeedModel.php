<?php
declare (strict_types = 1);

namespace app\model\home;

use think\facade\Db;

/**
 * NewsfeedModel
 */
class NewsFeedModel extends Db {
	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * 是否删除-未删除
     */
    CONST IS_DEL_NOT_DEL = 0;
    /**
     * 是否删除-已删除
     */
    CONST IS_DEL_DEL = 1;
    /**
     * getNews 获取菜单模块列表
     * 
     * @author wuyinghua
     * @param $page_size
     * @param $page_index
     * @param $filter
	 * @return $list
     */
    public function getNews($page_size, $page_index, $filter) {
        $list = Db::table('jy_notices')
            ->field('id, title, file_url, state, create_time, author')
            ->where('title', 'like', '%' . trim($filter) . '%')
            ->where('is_del', '=', self::IS_DEL_NOT_DEL)
            ->where('type', '=', '2')
            ->order('create_time', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index]);

        return $list;
    }

    /**
     * seeNew 获取资讯详情
     * 
     * @author wuyinghua
     * @param $id
	 * @return $list
     */
    public function seeNew($id) {
        $list = Db::table('jy_notices')
            ->field('id, title, content, file_url, state, create_time')
            ->where('id', (int)$id)
            ->order('create_time', 'desc')
            ->find();

        return $list;
    }
}
