<?php
declare (strict_types = 1);

namespace app\model\home;

use think\facade\Db;

/**
 * CommonConfigModel
 */
class CommonConfigModel extends Db {
	//======================================================================
	// PUBLIC FUNCTIONS
	//======================================================================

    /**
     * getMenus 获取菜单模块列表
     * 
     * @author wuyinghua
	 * @return $list
     */
    public function getMenus() {
        $list = Db::table('jy_menu jm')
            ->field('jm.id, jm.title, jm.pid, jm.component')
            ->select();

        return $list;
    }

    /**
     * getUserMenus 获取用户常用功能配置
     * 
     * @author wuyinghua
     * @param $user_id
	 * @return $list
     */
    public function getUserMenus($user_id) {
        $list = Db::table('jy_common_config jcc')
            ->field('jcc.menu_id')
            ->where('jcc.user_id', '=', $user_id)
            ->select();

        return $list;
    }

    /**
     * addUserMenus 新增用户常用功能配置
     * 
     * @author wuyinghua
     * @param $data
	 * @return $add
     */
    public static function addUserMenus($data) {
        $add = Db::table('jy_common_config')->insert($data);

        return $add;
    }

    /**
     * delUserMenus 删除用户常用功能配置
     * 
     * @author wuyinghua
     * @param $data
	 * @return $del
     */
    public function delUserMenus($data) {
        $del = Db::table('jy_common_config')->where('menu_id', (int)$data['menu_id'])->where('user_id', (int)$data['user_id'])->delete();

        return $del;
    }
}
