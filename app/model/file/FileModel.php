<?php

namespace app\model\file;

use think\facade\Db;


/**
 * FileModel
 */
class FileModel extends Db
{

    public function getList($page_size, $page_index, $filters)
    {
        $where = array();
        if (isset($filters['module']) && !empty($filters['module'])) {
            $where[] = array(['r.module', '=', $filters['module']]);
        }

        if (isset($filters['organization_id']) && !empty($filters['organization_id'])) {
            $where[] = array(['r.organization_id', '=', $filters['organization_id']]);
        } else {
            $where[] = array(['r.main_organization_id', '=', $filters['main_organization_id']]);
        }

        $list = Db::table('jy_file r')
            ->field('r.id,r.source_name,r.module,
            r.modify_time,r.modify_by,u.username,o.name organization_name,r.create_time,r.status')
            ->leftJoin('jy_user u', 'u.id=r.modify_by')
            ->leftJoin('jy_organization o', 'o.id=r.organization_id')
            ->where($where)
            ->where('r.type', 2)
            ->order('r.modify_time', 'desc')
            ->paginate(['list_rows' => $page_size, 'page' => $page_index])
            ->toArray();
        $data=$list['data'];
        if($data){
            foreach ($data as $k=>$v){
                if($v['status']==1){
                    $v['state_name'] = '生成中';
                }elseif($v['status']==2){
                    $v['state_name'] = '已生成';
                }elseif($v['status']==3){
                    $v['state_name'] = '生成失败';
                }
                $data[$k] = $v;
            }
            $list['data'] = $data;
        }
        return $list;
    }


    public static function getTitleById($id)
    {
        $info = Db::table('jy_file')
            ->where('id', $id)
            ->find();

        return $info;
    }

    public static function organizationList($id)
    {

        $list = Db::table('jy_organization')
            ->field('id,name')
            ->where('main_organization_id', $id)
            ->select()
            ->toArray();
        return $list;
    }

}