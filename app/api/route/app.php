<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

// 产品碳足迹碳标签 详情接口
Route::get('productcarbonlabelinfo', 'product.ProductCarbonLabel/info');

Route::group(function () {
// 产品碳足迹碳标签管理列表
Route::get('productcarbonlabel', 'product.ProductCarbonLabel/index');
// 产品碳足迹碳标签 获取新增表单接口
Route::get('productcarbonlabeladdform', 'product.ProductCarbonLabel/addform');
// 产品碳足迹碳标签 新增接口
Route::post('productcarbonlabeladd', 'product.ProductCarbonLabel/add');
// 产品碳足迹碳标签 新增接口
Route::post('productcarbonlabeledit', 'product.ProductCarbonLabel/edit');
// 产品碳足迹碳标签 删除接口
Route::post('productcarbonlabeldel', 'product.ProductCarbonLabel/del');
// 产品碳足迹碳标签 获取二维码图片接口
Route::get('productcarbonlabelqrcodeurl', 'product.ProductCarbonLabel/getqrcodeurl');

//供应链-我是采购端-供应商列表
Route::get('supplycustomer', 'supply.Customer/index');
//供应链-我是采购端-供应商详情
Route::get('supplycustomerinfo', 'supply.Customer/info');
//供应链-我是采购端-供应商禁用/启用
Route::post('supplycustomerstate', 'supply.Customer/state');
//供应链-我是采购端-供应商风险状态调整表单
Route::get('supplycustomerriskstatemap', 'supply.Customer/riskstatemap');
//供应链-我是采购端-供应商风险状态调整
Route::post('supplycustomerriskstate', 'supply.Customer/riskstate');
//供应链-我是采购端-生成供应商邀请链接
Route::post('supplycustomersupplierinvite', 'supply.Customer/supplierinvite');
//供应链-我是采购端-供应商确认绑定
Route::post('supplycustomersupplierconfirm', 'supply.Customer/supplierconfirm');
//供应链-我是采购端-供应产品管理列表
Route::get('supplycustomerproduct', 'supply.CustomerProduct/index');
//供应链-我是采购端-供应产品新增
Route::post('supplycustomerproductadd', 'supply.CustomerProduct/add');
//供应链-我是供应端-产品新增表单信息
Route::get('supplycustomerproductaddform', 'supply.CustomerProduct/addForm');
//供应链-我是采购端-供应产品编辑
Route::post('supplycustomerproductedit', 'supply.CustomerProduct/edit');
//供应链-我是采购端-供应产品删除
Route::post('supplycustomerproductdel', 'supply.CustomerProduct/del');
//供应链-我是采购端-供应产品详情
Route::get('supplycustomerproductinfo', 'supply.CustomerProduct/info');
//供应链-我是采购端-供应产品启用/禁用
Route::post('supplycustomerproductstate', 'supply.CustomerProduct/state');
//供应链-我是采购端-供应产品要求填报
Route::post('supplycustomerproductrequestfill', 'supply.CustomerProduct/requestfill');
//供应链-我是采购端-供应产品审核/驳回
Route::post('supplycustomerproductauditstate', 'supply.CustomerProduct/auditstate');
//供应链-我是采购端-供应产品导入
Route::post('supplycustomerproductimport', 'supply.CustomerProduct/import');
//供应链-我是采购端-供应产品报送审批列表
Route::get('supplycustomerproductauditlist', 'supply.CustomerProduct/auditlist');

//供应链-我是供应端-客户列表
Route::get('supplysupplier', 'supply.Supplier/index');
//供应链-我是供应端-客户详情
Route::get('supplysupplierinfo', 'supply.Supplier/info');
//供应链-我是供应端-客户列表
Route::post('supplysupplierstate', 'supply.Supplier/state');
//供应链-我是供应端-产品列表
Route::get('supplysupplierproduct', 'supply.SupplierProduct/index');
//供应链-我是供应端-产品新增
Route::post('supplysupplierproductadd', 'supply.SupplierProduct/add');
//供应链-我是供应端-产品新增表单信息
Route::get('supplysupplierproductaddform', 'supply.SupplierProduct/addForm');
//供应链-我是供应端-供应产品编辑
Route::post('supplysupplierproductedit', 'supply.SupplierProduct/edit');
//供应链-我是供应端-供应产品删除
Route::post('supplysupplierproductdel', 'supply.SupplierProduct/del');
//供应链-我是供应端-供应产品详情
Route::get('supplysupplierproductinfo', 'supply.SupplierProduct/info');
//供应链-我是供应端-供应产品管理-客户报送
Route::get('supplysupplierproductfilllist', 'supply.SupplierProduct/supplierProductFillList');
//供应链-我是供应端-供应产品管理-发起报送
Route::post('supplysupplierproductrequestfill', 'supply.SupplierProduct/RequestFill');
//供应链-我是供应端-供应产品管理-更新报送
Route::post('supplysupplierproductupdaterequestfill', 'supply.SupplierProduct/UpdateRequestFill');
//供应链-我是供应端-货品报送-进行报送列表
Route::get('supplysupplierproductcustomerrequestfilllist', 'supply.SupplierProduct/CustomerRequestFillList');
//供应链-我是供应端-货品报送-进行报送选择已有
Route::post('supplysupplierproductsubbmitfillexistproduct', 'supply.SupplierProduct/SubbmitFillExistProduct');
//供应链-我是供应端-货品报送-进行报送新增产品
Route::post('supplysupplierproductsubbmitfillnewadd', 'supply.SupplierProduct/SubbmitFillNewAdd');
//供应链-我是供应端-供应产品管理-添加排放源表单获取
Route::get('supplysupplieraddfactorform', 'supply.SupplierProduct/AddFactorForm');
//供应链-我是供应端-供应产品管理-添加排放源验证接口
Route::post('supplysupplieraddfactorvarify', 'supply.SupplierProduct/AddFactorVerify');
//供应链-我是供应端-供应产品导入
Route::post('supplysupplierproductimport', 'supply.SupplierProduct/import');

// 碳因子库管理列表
Route::get('factorlist', 'factor.Factor/index');
// 碳因子库获取详情
Route::get('factorfind', 'factor.Factor/find');
// 碳因子库 新增接口
Route::post('factoradd', 'factor.Factor/add');
// 碳因子库编辑接口
Route::post('factoredit', 'factor.Factor/edit');
// 碳因子库 启用禁止接口
Route::post('factorchange', 'factor.Factor/changStatus');

// 碳因子库导出路由
Route::get('factorexport', 'factor.Factor/export');
// 碳因子库导入路由
Route::post('factorimport', 'factor.Factor/import');

//认证管理
//认证列表
Route::get('attestationlist', 'attestation.Attestation/index');
//添加认证
Route::post('attestationadd', 'attestation.Attestation/add');
//认证详情
Route::get('attestationinfo', 'attestation.Attestation/find');
//认证删除
Route::post('attestationdel', 'attestation.Attestation/del');
//认证编辑
Route::post('attestationedit', 'attestation.Attestation/edit');
//上传图片
Route::post('attestationupload', 'attestation.Attestation/upload');
//上传文件
Route::post('attestationuploadform', 'attestation.Attestation/uploadform');
//获取侧边栏
Route::get('leftmenu', 'attestation.Attestation/leftmenu');
//添加认证表单
Route::post('addform', 'attestation.Attestation/addform');
//编辑认证表单
Route::post('editform', 'attestation.Attestation/editform');
//快照详情
Route::get('shot_find', 'attestation.Attestation/shotFind');
//撤回
Route::post('attestationback', 'attestation.Attestation/back');
//提交认证
Route::post('submitattestation', 'attestation.Attestation/submitattestation');
//统计认证申请
Route::get('getcount', 'attestation.Attestation/getcount');
//删除认证表单
Route::post('delform', 'attestation.Attestation/delform');
//获取城市列表
Route::get('getcity', 'attestation.Attestation/getcity');
//下载报告
Route::get('getreport', 'attestation.Attestation/download');
//下载证书
Route::get('getcertificate', 'attestation.Attestation/download');
//产品列表
Route::get('getproductcalculate', 'attestation.Attestation/productList');
Route::get('down', 'attestation.Attestation/down');

//减排场景管理
//列表
Route::get('reductionlist', 'reduction.Reduction/index');
//添加
Route::post('reductionadd', 'reduction.Reduction/add');
//详情
Route::get('reductioninfo', 'reduction.Reduction/find');
//删除
Route::get('reductiondel', 'reduction.Reduction/del');
//编辑
Route::post('reductionedit', 'reduction.Reduction/edit');


//排放基准管理
//列表
Route::get('datumlist', 'datum.Datum/index');
//添加
Route::post('datumadd', 'datum.Datum/add');
//详情
Route::get('datuminfo', 'datum.Datum/find');
//编辑
Route::post('datumedit', 'datum.Datum/edit');

//数据质量管理
//列表
Route::get('qualitylist', 'quality.Quality/index');
//添加
Route::post('qualityadd', 'quality.Quality/add');
//详情
Route::get('qualityinfo', 'quality.Quality/find');
//复制
Route::get('qualitycopy', 'quality.Quality/copy');
//删除
Route::get('qualitydel', 'quality.Quality/del');
//编辑
Route::post('qualityedit', 'quality.Quality/edit');

//组织基本信息
Route::post('organizational_edit', 'quality.Quality/organizationalEdit');
Route::get('organizational_info', 'quality.Quality/organizationalInfo');
Route::post('organizational_upload', 'attestation.Attestation/upload');
//组织边界
Route::post('organizational_boundary_edit', 'quality.Quality/organizationalBoundariesEdit');
Route::get('organizational_boundary_info', 'quality.Quality/organizationalBoundariesInfo');
//ghg，iso管理
//ghg列表
Route::get('ghglist', 'quality.Quality/ghgIndex');
//ghg编辑
Route::post('ghgedit', 'quality.Quality/ghgEdit');
//ghg详情
Route::get('ghginfo', 'quality.Quality/ghgInfo');
//iso列表
Route::get('isolist', 'quality.Quality/isoIndex');
//iso编辑
Route::post('isoedit', 'quality.Quality/isoEdit');
//iso详情
Route::get('isoinfo', 'quality.Quality/isoInfo');

//数据质量管理规定
Route::post('data_quality_edit', 'quality.Quality/dataQualityRoleEdit');

Route::get('data_quality_info', 'quality.Quality/dataQualityRoleInfo');


//排放源管理
//列表
Route::get('emissionlist', 'emission.Emission/index');
//添加
Route::post('emissionadd', 'emission.Emission/add');
//详情
Route::get('emissioninfo', 'emission.Emission/find');
//删除
Route::get('emissiondel', 'emission.Emission/del');
//编辑
Route::post('emissionedit', 'emission.Emission/edit');
//复制
Route::get('emissioncopy', 'emission.Emission/copy');
Route::get('addghg', 'emission.Emission/addghg');
//审核管理
//列表
Route::get('examinelist', 'examine.Examine/index');
//审核
Route::post('examinestatus', 'examine.Examine/status');
//详情
Route::get('examineinfo', 'examine.Examine/find');

Route::post('up', 'file.FactorFile/up');
Route::post('upId', 'file.FactorFile/upId');
Route::post('delid', 'file.FactorFile/delid');
Route::get('ghgexcel', 'file.File/ghgExcel');
Route::get('isoexcel', 'file.File/isoExcel');
//下载管理
//列表
Route::get('filelist', 'file.File/index');
Route::post('filedown', 'file.File/down');
})->middleware(\app\middleware\Check::class);



