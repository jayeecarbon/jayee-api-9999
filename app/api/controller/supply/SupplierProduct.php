<?php

namespace app\api\controller\supply;

use app\BaseController;
use app\model\admin\FileModel;
use app\model\admin\IndustryModel;
use app\model\supply\CustomerModel;
use app\model\supply\CustomerProductDischargeModel;
use app\model\supply\CustomerProductModel;
use app\model\supply\FactorTypeModel;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use app\model\supply\SupplierFillProductCustomerRelationModel;
use app\model\supply\SupplierProductDischargeModel;
use app\model\supply\SupplierProductModel;
use app\model\system\OrganizationModel;
use app\model\system\OperationModel;
use app\validate\SupplierProductValidate;
use app\model\admin\UnitModel;

use think\exception\ValidateException;
use think\facade\Db;
use think\Request;

class SupplierProduct extends BaseController
{
    /**
     * 查看的是供应商的产品信息
     */

    /**
     * @notes 供应商产品列表
     * @author fengweizhe
     */
    public function index(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $product_name = isset($_GET['product_name']) ? $_GET['product_name'] : '';
        $product_type = isset($_GET['product_type']) ? $_GET['product_type'] : '';
        $customer_name = isset($_GET['customer_name']) ? $_GET['customer_name'] : '';
        $auth = isset($_GET['auth']) ? $_GET['auth'] : '';
        $supplier_id = $data_redis['main_organization_id'];
        $page_size = isset($_GET['pageSize']) ?$_GET['pageSize'] : SupplierProductModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : SupplierProductModel::pageIndex;
        $list = SupplierProductModel::getList($page_size, $page_index, ['product_name'=>$product_name, 'product_type'=>$product_type,
            'auth'=>$auth, 'supplier_id'=>$supplier_id])->toArray();
        $list_new = [];
        $auth_map = SupplierProductModel::AUTH_MAP;
        //增加客户名称信息
        $product_ids = array_column($list['data'], 'id');
        $customer_name_info = SupplierFillProductCustomerRelationModel::getCustomerTitleBySupplyProductIds($product_ids);
        $customer_name_map = [];
        foreach ($customer_name_info as $ck => $cv) {
            $customer_name_map[$cv['supplier_product_id']][] = $cv['title'];
            $customer_name_map[$cv['supplier_product_id']] = array_unique($customer_name_map[$cv['supplier_product_id']]);
        }

        foreach ($list['data'] as $k => $v) {
            if (isset($auth_map[$v['auth']])) {
                $v['auth_name'] = $auth_map[$v['auth']];
            } else {
                $v['auth_name'] = '';
            }
            //客户名称
            $customer_name_arr = isset($customer_name_map[$v['id']]) ? $customer_name_map[$v['id']] : [];
            $customer_name_str = implode(',', $customer_name_arr);
            $v['customer_name'] = $customer_name_str;
            $list_new[$k] = $v;
        }

        $search_list = [];
        foreach ($list_new as $k => $v) {
            // 匹配供給客戶
            if (preg_match('/' . $customer_name . '/i', $v['customer_name']) > 0) {
                $search_list[$k] = $v;
            }
        }

        unset($list['data']);
        $data['code'] = 200;
        $data['data']['list'] = array_values($search_list);
        $data['data']['total'] = $list['total'] ;
        $data['data']['auth'] = SupplierProductModel::AUTH_SELECT_MAP;

        return json($data);
    }

    /**
     * @notes 供应商产品列表
     * @author fengweizhe
     */
    public function info() {
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $supplier_product_info = SupplierProductModel::getDataById($id);
        if ($supplier_product_info == null) {
            return json(['code'=>201, 'message'=>"未查询到供应商产品相关信息"]);
        }

        // 第三方认证
        $auth_map = CustomerProductModel::AUTH_MAP;
        if (isset($auth_map[$supplier_product_info['auth']])) {
            $supplier_product_info['auth_name'] = $auth_map[$supplier_product_info['auth']];
        } else {
            $supplier_product_info['auth_name'] = '';
        }

         //根据产品id去供应产品与客户关系表中查询对应客户
        $customer_name = SupplierFillProductCustomerRelationModel::getCustomerTitleBySupplyProductId($supplier_product_info['id']);
        $customer_name_arr = array_unique(array_column($customer_name, 'title'));
        $customer_name_str = implode(',', $customer_name_arr);
        $supplier_product_info['customer_name'] = $customer_name_str;

        //把文件的相关信息补全
        $file_url = $supplier_product_info['file_url'];
        $supplier_product_info['files'] = [];
        if ($file_url != '') {
            $file_url_arr = explode(',', $file_url);
            foreach ($file_url_arr as $k => $v) {
                $file_data = FileModel::getInfoById($v);
                $supplier_product_info['files'][] = $file_data;
            }
        }

        //根据产品id查询碳核算信息
        $discharge_info = SupplierProductDischargeModel::getDataByProductId($supplier_product_info['id'])->toArray();
        $type_id_arr = array_column($discharge_info, 'type_id');
        $type_id_arr = array_unique($type_id_arr);
        $factor_map = FactorTypeModel::getList();
        $factor_map_new = [];
        foreach ($factor_map as $k => $v) {
            $factor_map_new[$v['id']] = $v['name'];
        }

        //封装排放源结构
        $discharge_info_new = [];
        foreach ($type_id_arr as $kk => $vv) {
            $arr['type_name'] = $factor_map_new[$vv];
            $tmp = [];
            foreach ($discharge_info as $kkk => $vvv) {
                $vvv['unit_type'] = (float)$vvv;
                $vvv['unit'] = (float)$vvv;
                if ($vvv['type_id'] == $vv) {
                    $tmp[] = $vvv;
                }
            }
            $arr['datas'] = $tmp;
            $discharge_info_new[] = $arr;
        }

        if (!empty($discharge_info)) {
            $supplier_product_info['discharge'] = $discharge_info_new;
        } else {
            $supplier_product_info['discharge'] = [];
        }

        $data['code'] = 200;
        $data['data'] = $supplier_product_info;

        return json($data);
    }

    /**
     * @notes 供应商产品新增
     * @author fengweizhe
     */
    public function add(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['product_name'] = $params_payload_arr['product_name'];
            $data['product_model'] = $params_payload_arr['product_model'];
            $data['product_type'] = $params_payload_arr['product_type'];
            $data['auth'] = $params_payload_arr['auth'];
            $data['week_start'] = $params_payload_arr['week_start'];
            $data['week_end'] = $params_payload_arr['week_end'];
            $data['number'] = $params_payload_arr['number'];
            $data['unit_type'] = $params_payload_arr['unit_type'];
            $data['unit'] = $params_payload_arr['unit'];
            $data['file_url'] = isset($params_payload_arr['file_url']) ? $params_payload_arr['file_url'] : '';
            $data['supplier_id'] = $data_redis['main_organization_id'];
            $data['discharge'] = $params_payload_arr['discharge'];
            $data['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $discharge = $data['discharge'];
            unset($data['discharge']);
            //当第三方认证选项为有时，附件信息为必填项，至少需要上传一个附件文件，否则提示 尚未上传认证材料
            if ($data['auth'] == SupplierProductModel::AUTH_HAVE && empty($params_payload_arr['file_url'])) {
                return json(['code'=>201, 'message'=>"尚未上传认证材料"]);
            }

            //封装排放因子数据, 以及产品排放量和排放系数
            foreach ($discharge as $dk => &$dv) {
                // 数量与因子分母单位不一致时，先判断是否能通过数据字典中单位系数进行换算，如果不能换算则排放量信息展示为空，不纳入计算
                $factor_denominator_arr = explode('/', $dv['factor_unit']); // factor_unit，格式'kgCO2/kg'
                $factor_denominator_name = end($factor_denominator_arr);
                $factor_unit_data = UnitModel::getInfoByName($factor_denominator_name);
                $data_management_unit_data = UnitModel::findUnit($dv['unit']);
                if ($factor_unit_data != NULL && $factor_unit_data['type_id'] == $dv['unit_type'] && $data_management_unit_data != NULL) {
                    $dv['emissions'] = $dv['value'] * $dv['coefficient'] * $data_management_unit_data['conversion_ratio'] / $factor_unit_data['conversion_ratio'];
                } else {
                    $dv['emissions'] = NULL;
                }
            }

            //计算出产品的碳排放量
            $emissions_arr = array_column($discharge, 'emissions');
            $product_emissions = array_sum($emissions_arr);
            $data['emissions'] = $product_emissions;
            $data['coefficient'] = $product_emissions / $data['number'];

            Db::startTrans();
            try {
                // 验证供应商，当前产品（产品名称 + 产品编号）是否存在
                if (self::checkProduct($data['supplier_id'], $data['product_name'], $data['product_model'])) {
                    $product_id = SupplierProductModel::addSupplierProduct($data);
                } else {
                    return json(['code'=>201, 'message'=>"产品已存在，请重新输入"]);
                }

                $discharge_insert = [];
                foreach ($discharge as $kk => $vv) {
                    $vv['supplier_product_id'] = $product_id;
                    $discharge_insert[$kk] = $vv;
                }
                SupplierProductDischargeModel::addSupplierProductDischarge($discharge_insert);

                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '供应链碳管理';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['url'] = $request->pathinfo();
                $data_log['log'] = '新增产品：' . $data['product_name'];
                OperationModel::addOperation($data_log);

                Db::commit();
                return json(['code'=>200, 'message'=>"添加成功"]);
            } catch (\Exception $e) {
                Db::rollback();
                return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
            }
        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * @notes 供应商产品编辑
     * @author fengweizhe
     */
    public function edit(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data_redis = $request->middleware('data_redis');
            $data['id'] = $params_payload_arr['id'];
            $data['product_name'] = $params_payload_arr['product_name'];
            $data['product_model'] = $params_payload_arr['product_model'];
            $data['product_type'] = $params_payload_arr['product_type'];
            $data['auth'] = $params_payload_arr['auth'];
            $data['week_start'] = $params_payload_arr['week_start'];
            $data['week_end'] = $params_payload_arr['week_end'];
            $data['number'] = (float)$params_payload_arr['number'];
            $data['unit_type'] = $params_payload_arr['unit_type'];
            $data['unit'] = $params_payload_arr['unit'];
            $data['file_url'] = isset($params_payload_arr['file_url']) ? $params_payload_arr['file_url'] : '';
            $data['supplier_id'] = $data_redis['main_organization_id'];
            $data['discharge'] = $params_payload_arr['discharge'];
            $data['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $discharge = $data['discharge'];
            unset($data['discharge']);
            //当第三方认证选项为有时，附件信息为必填项，至少需要上传一个附件文件，否则提示 尚未上传认证材料
            if ($data['auth'] == SupplierProductModel::AUTH_HAVE && empty($params_payload_arr['file_url'])) {
                return json(['code'=>201, 'message'=>"尚未上传认证材料"]);
            }

            //封装排放因子数据, 以及产品排放量和排放系数
            foreach ($discharge as $dk => &$dv) {
                $dv['coefficient'] = floatval($dv['coefficient']);
                $dv['value'] = floatval($dv['value']);
                // 数量与因子分母单位不一致时，先判断是否能通过数据字典中单位系数进行换算，如果不能换算则排放量信息展示为空，不纳入计算
                $factor_denominator_arr = explode('/', $dv['factor_unit']); // factor_unit，格式'kgCO2/kg'
                $factor_denominator_name = end($factor_denominator_arr);
                $factor_unit_data = UnitModel::getInfoByName($factor_denominator_name);
                $data_management_unit_data = UnitModel::findUnit($dv['unit']);
                if ($factor_unit_data != NULL && $factor_unit_data['type_id'] == $dv['unit_type'] && $data_management_unit_data != NULL) {
                    $dv['emissions'] = $dv['value'] * $dv['coefficient'] * $data_management_unit_data['conversion_ratio'] / $factor_unit_data['conversion_ratio'];
                } else {
                    $dv['emissions'] = NULL;
                }
                
                $dv['factor_name'] = isset($dv['factor_name']) ? $dv['factor_name'] : '';
                $dv['year'] = isset($dv['year']) ? $dv['year'] : '';
            }

            //计算出产品的碳排放量
            $emissions_arr = array_column($discharge, 'emissions');
            $product_emissions = array_sum($emissions_arr);
            $data['emissions'] = $product_emissions;
            $data['coefficient'] = $product_emissions / $data['number'];

            Db::startTrans();
            try {
                // 验证供应商，当前产品（产品名称 + 产品编号）是否存在
                if (self::checkProduct($data['supplier_id'], $data['product_name'], $data['product_model'], $data['id'])) {
                    SupplierProductModel::editSupplierProduct($data);
                } else {
                    return json(['code'=>201, 'message'=>"产品已存在，请重新输入"]);
                }
                
                $discharge_insert = [];
                foreach ($discharge as $kk => $vv) {
                    $vv['supplier_product_id'] = $data['id'];
                    $discharge_insert[$kk] = $vv;
                }
                //把排放源全部删除
                SupplierProductDischargeModel::delProductDischarge($data['id']);
                //重新新增排放源信息
                SupplierProductDischargeModel::addSupplierProductDischarge($discharge_insert);

                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '供应链碳管理';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['url'] = $request->pathinfo();
                $data_log['log'] = '编辑产品：' . $data['product_name'];
                OperationModel::addOperation($data_log);

                Db::commit();
                return json(['code'=>200, 'message'=>"编辑成功"]);
            } catch (\Exception $e) {
                Db::rollback();
                return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
            }
        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * @notes 供应商产品删除
     * @author fengweizhe
     */
    public function del(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        Db::startTrans();
        try {
            //删除商品
            SupplierProductModel::delSupplierProduct($data['id']);
            $supplier_product = SupplierProductModel::getDataById($data['id']);
            //删除商品的排放源
            SupplierProductDischargeModel::delProductDischarge($data['id']);

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '供应链碳管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '删除产品：' . $supplier_product['product_name'];
            OperationModel::addOperation($data_log);

            Db::commit();
            return json(['code'=>200, 'message'=>"删除成功"]);
        } catch (\Exception $e) {
            Db::rollback();
            return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
        }
    }

    /**
     * @notes 获取产品添加时的表单信息
     *
     * @author wuyinghua
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function addForm(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $main_id = $data_redis['main_organization_id'];
        $supplier_list = CustomerModel::addForm($main_id);
        $data['code'] = 200;
        $data['data']['list'] = $supplier_list;

        return json($data);
    }

    /**
     * @notes 供应产品excel上传
     * 
     * @author wuyinghua
     * @return array|Json
     */
    public function import(Request $request) {

        Db::startTrans();
        $data_redis = $request->middleware('data_redis');
        $files = $request->file("file");
        $file[] = $files;
        // 验证文件大小，名称等是否正确
        validate(['file' => 'filesize:51200|fileExt:xls,xlsx'])->check($file);
        // 把上传的excel文件下载到本地一份
        $savename = \think\facade\Filesystem::disk('public')->putFile('file', $file[0]);
        // 读取本地的excel文件
        $spreadsheet = IOFactory::load('storage/' . $savename);
        $sheet = $spreadsheet->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        Coordinate::columnIndexFromString($highestColumn);

        try {
            // 供应产品数据
            $product_data = [];
            $product_data['product_name'] = $spreadsheet->getActiveSheet()->getCell("B2")->getValue();
            $product_data['product_model'] = $spreadsheet->getActiveSheet()->getCell("B3")->getValue();
            $product_data['product_type'] = $spreadsheet->getActiveSheet()->getCell("F3")->getValue();
            $week = $spreadsheet->getActiveSheet()->getCell("B4")->getValue();
            $product_data['week_start'] = substr($week, 0, 7);
            $product_data['week_end'] = substr($week, 8, 7);

            if (!$product_data['week_start'] || !$product_data['week_end'] || $product_data['week_start']>$product_data['week_end']) {
                return json(['code'=>201, 'message'=>"请输入正确的核算周期，例：2022-10-2022-11"]);
            }

            $product_data['number'] = $spreadsheet->getActiveSheet()->getCell("B5")->getValue();
            if (!is_numeric($product_data['number'])) {
                return json(['code'=>201, 'message'=>"请输入正确的核算数量"]);
            }

            $unit = strtolower($spreadsheet->getActiveSheet()->getCell("F5")->getValue());
            $unit_data = SupplierProductModel::getUnitIdByName($unit);
            if ($unit_data != NULL) {
                $product_data['unit_type'] = $unit_data['type_id'];
                $product_data['unit'] = $unit_data['id'];
            } else {
                $product_data['unit_type'] = NULL;
                $product_data['unit'] = NULL;
            }
            
            $auth = $spreadsheet->getActiveSheet()->getCell("B6")->getValue();
            $auth_map = SupplierProductModel::AUTH_MAP;
            if ($auth == $auth_map[SupplierProductModel::AUTH_HAVE]) {
                $product_data['auth'] = SupplierProductModel::AUTH_HAVE;
            } else {
                $product_data['auth'] = SupplierProductModel::AUTH_NOT_HAVE;
            }

            $product_data['supplier_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
            $product_data['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $product_data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $product_data['create_time'] = date('Y-m-d H:i:s');
            $product_data['modify_time'] = date('Y-m-d H:i:s');

            // 验证供应商，当前产品（产品名称 + 产品编号）是否存在
            if (self::checkProduct($product_data['supplier_id'], $product_data['product_name'], $product_data['product_model'])) {
                // 插入供应产品表
                $supplier_product_id = SupplierProductModel::addSupplierProduct($product_data);
            } else {
                return json(['code'=>201, 'message'=>"产品已存在，请重新编辑后上传"]);
            }

            // 排放源数据
            $discharge_data = [];
            $line_arr = [];
            $type_id = '';
            for ($row = 8; $row <= $highestRow; $row++) {
                $temp_type = $spreadsheet->getActiveSheet()->getCell('A' . $row)->getValue();
                $factor_type_data = SupplierProductModel::getFactorIdByName($temp_type);
                if ($factor_type_data != NULL) {
                    $type_id = $factor_type_data['id'];
                    $row = $row + 1;
                    continue;
                } else {
                    $discharge_data[$row]['type_id'] = $type_id;
                }

                $discharge_data[$row]['name'] = $spreadsheet->getActiveSheet()->getCell('A' . $row)->getValue();
                $discharge_data[$row]['loss_type'] = $spreadsheet->getActiveSheet()->getCell('B' . $row)->getValue();
                $discharge_data[$row]['value'] = $spreadsheet->getActiveSheet()->getCell('C' . $row)->getValue();
                $discharge_unit = strtolower($spreadsheet->getActiveSheet()->getCell('D' . $row)->getValue());
                $discharge_unit_data = SupplierProductModel::getUnitIdByName($discharge_unit);
                if ($discharge_unit_data != NULL) {
                    $discharge_data[$row]['unit_type'] = $discharge_unit_data['type_id'];
                    $discharge_data[$row]['unit'] = $discharge_unit_data['id'];
                } else {
                    $discharge_data[$row]['unit_type'] = NULL;
                    $discharge_data[$row]['unit'] = NULL;
                }

                $discharge_data[$row]['coefficient'] = $spreadsheet->getActiveSheet()->getCell('E' . $row)->getValue();
                if (!is_numeric($discharge_data[$row]['coefficient'])) {
                    return json(['code'=>201, 'message'=>"请输入正确的排放因子数值"]);
                }
                $discharge_data[$row]['factor_unit'] = $spreadsheet->getActiveSheet()->getCell('F' . $row)->getValue();
                $discharge_data[$row]['factor_source'] = $spreadsheet->getActiveSheet()->getCell('G' . $row)->getValue();
                // 数量与因子分母单位不一致时，先判断是否能通过数据字典中单位系数进行换算，如果不能换算则排放量信息展示为空，不纳入计算
                $factor_denominator_arr = explode('/', $discharge_data[$row]['factor_unit']); // factor_unit，格式'kgCO2/kg'
                $factor_denominator_name = end($factor_denominator_arr);
                $factor_unit_data = UnitModel::getInfoByName($factor_denominator_name);
                if ($discharge_data[$row]['unit'] != NULL) {
                    $data_management_unit_data = UnitModel::findUnit($discharge_data[$row]['unit']);
                } else {
                    $data_management_unit_data = NULL;
                }
                
                if ($factor_unit_data != NULL && $factor_unit_data['type_id'] == $discharge_data[$row]['unit_type'] && $data_management_unit_data != NULL) {
                    $dv['emissions'] = (float)$discharge_data[$row]['value'] * (float)$discharge_data[$row]['coefficient'] * $data_management_unit_data['conversion_ratio'] / $factor_unit_data['conversion_ratio'];
                } else {
                    $dv['emissions'] = NULL;
                }
                $discharge_data[$row]['supplier_product_id'] = (int)$supplier_product_id;
                $discharge_data[$row]['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
                $discharge_data[$row]['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
                $discharge_data[$row]['create_time'] = date('Y-m-d H:i:s');
                $discharge_data[$row]['modify_time'] = date('Y-m-d H:i:s');

                $combineData[] = $discharge_data[$row]['name'] . $discharge_data[$row]['loss_type'];
            }

            // 导入数据中重复数据以最后一条为准
            foreach ($combineData as $key => $val) {
                unset($combineData[$key]);
                if(in_array($val, $combineData)) {
                    unset($discharge_data[$key]);
                }
            }

            foreach ($line_arr as $val) {
                unset($discharge_data[$val]);
            }

            // 新增排放量
            $add_discharge_data = SupplierProductDischargeModel::addSupplierProductDischarge($discharge_data);

            // 计算出产品的碳排放量
            $emissions_arr = array_column($discharge_data, 'emissions');
            $product_emissions = array_sum($emissions_arr);
            $product_update_data['id'] = (int)$supplier_product_id;
            $product_update_data['emissions'] = $product_emissions;
            $product_update_data['coefficient'] = $product_emissions / $product_data['number'];

            // 更新供应产品表中的排放量和排放系数
            SupplierProductModel::editSupplierProduct($product_update_data);

            // 删除本地下载的文件
            unlink('storage/' . $savename);

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '供应链碳管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '导入产品';
            OperationModel::addOperation($data_log);

            // 提交事务
            Db::commit();
            return json(['code'=>200, 'message'=>'文件上传成功，已经导入1件产品' . $add_discharge_data . '件排放源']);
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            return json(['code'=>404, 'message'=>'导入数据失败，' . $e->getMessage()]);
        }
    }

    /**
     * @notes  客户报送列表
     * @author fengweizhe
     */
    public function supplierProductFillList(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $data['id'] = isset($_GET['id']) ? $_GET['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $data['product_name'] = isset($_GET['product_name']) ? $_GET['product_name'] : '';
        $data['carbon_coefficient'] = isset($_GET['carbon_coefficient']) ? $_GET['carbon_coefficient'] : '';
        $data['title'] = isset($_GET['title']) ? $_GET['title'] : '';
        $data['industry_id'] = isset($_GET['industry_id']) ? $_GET['industry_id'] : 0;
        //先通过公共商 查询出所有的相关联的客户的信息 列表中的客户信息就通过这里来查询出来
        $main = $data_redis['main_organization_id']; // 当前供应商的id
        $customer_arr = CustomerModel::getAllDataById($main, $data['title'], $data['industry_id']);
        //查询供应产品数量 以map形式来绑定每行数据
        $customer_id_arr = array_column($customer_arr, 'id');
        $customer_id_arr_str = implode(',', $customer_id_arr);
        $customer_product_count_arr = SupplierProductModel::getProductCountByCustomerIdArrString($main, $customer_id_arr_str);
        $customer_product_count_map = [];
        if (!empty($customer_product_count_arr)) {
            foreach ($customer_product_count_arr as $k => $v) {
                $customer_product_count_map[$v['customer_id']] = $v['count'];
            }
        }

        //查询报送表 看看有哪些客户报送了 以绑定的方式绑定已报送或者未报送 有客户id的就是已经报送的
        $id = $data['id']; //供应产品id 
        $relation = SupplierFillProductCustomerRelationModel::getDataBySupplierProductId($id);
        $relation_customer_id_arr = array_column($relation, 'customer_id');
        //进行数据整合
        $return_data = [];
        foreach ($customer_arr as $kk => $vv) {
            if (isset($customer_product_count_map[$vv['id']])) {
                $vv['supplier_product_count'] = $customer_product_count_map[$vv['id']];
            } else {
                $vv['supplier_product_count'] = 0;
            }
            if (in_array($vv['id'], $relation_customer_id_arr)) {
                $vv['fill_state'] = '已报送';
            } else {
                $vv['fill_state'] = '未报送';
            }
            $return_data[$kk] = $vv;
        }

        // 行业map
        $industry_id_map = IndustryModel::getList();
        $data_json['code'] = 200;
        $data_json['data']['list'] = $return_data;
        $data_json['data']['industry_id_map'] = $industry_id_map;
        $data_json['data']['product_name'] = $data['product_name'];
        $data_json['data']['carbon_coefficient'] = $data['carbon_coefficient'];

        return json($data_json);
    }

    /**
     * @notes  客户报送列表-发起报送
     * @author fengweizhe
     */
    public function requestFill(Request $request) {
        $data_redis = $request->middleware('data_redis');
        // 发起报送是把供应商的产品和排放源 分别查询出来 插入到客户的产品表 和 排放源表 然后最后把 带有客户表的产品id 存入到上报关系表中
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $data['customer_id'] = isset($params_payload_arr['customer_id']) ? $params_payload_arr['customer_id'] : '';
        if ($data['customer_id'] == '' || $data['customer_id'] == null || $data['customer_id'] == 0) {
            return json(['code'=>201, 'message'=>"参数customer_id错误"]);
        }

        // 查看报送供应商在采购者端的状态，禁用状态无法发起报送，直接返回
        $state_list = SupplierProductModel::getSupplierState($data['customer_id'], $data_redis['main_organization_id']);
        if ($state_list['state'] == SupplierProductModel::STATE_NO) {
            return json(['code'=>200, 'message'=>"操作成功"]);
        }

        Db::startTrans();
        try {
            // 查询供应商产品
            $product_info = SupplierProductModel::getDataById($data['id']);
            if ($product_info == null) {
                return json(['code'=>201, 'message'=>"未查询到供应商端产品信息"]);
            }
            // 整合数据写入产品
            $insert_data['product_name'] = $product_info['product_name'];
            $insert_data['product_model'] = $product_info['product_model'];
            $insert_data['product_type'] = $product_info['product_type'];
            $insert_data['supplier_id'] = $product_info['supplier_id'];
            $insert_data['auth'] = $product_info['auth'];
            $insert_data['state'] = CustomerProductModel::STATE_NO; // 报送产品不显示在采购者供应产品列表中，只有审批通过之后才会显示
            $insert_data['file_url'] = $product_info['file_url'];
            $insert_data['create_time'] = date('Y-m-d H:i:s');
            $insert_data['modify_time'] = date('Y-m-d H:i:s');
            $insert_data['customer_id'] = $data['customer_id'];
            $insert_data['audit_state'] = CustomerProductModel::AUDIT_STATE_NOT_VERIFY;
            $insert_data['week_start'] = $product_info['week_start'];
            $insert_data['week_end'] = $product_info['week_end'];
            $insert_data['number'] = $product_info['number'];
            $insert_data['unit_type'] = $product_info['customer_id'];
            $insert_data['unit'] = $product_info['unit'];
            $insert_data['emissions'] = $product_info['emissions'];
            $insert_data['coefficient'] = $product_info['coefficient'];
            $insert_data['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $insert_data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            // 整合数据写入排放源
            $product_add_back_id = CustomerProductModel::addCustomerProductAndGetId($insert_data);
            // 查询排放源
            $discharge_info = SupplierProductDischargeModel::getDataByProductId($data['id']);
            $batch_insert_data = [];
            foreach ($discharge_info as $k => $v) {
                $v_new['type_id'] = $v['type_id'];
                $v_new['type_name'] = $v['type_name'];
                $v_new['name'] = $v['name'];
                $v_new['loss_type'] = $v['loss_type'];
                $v_new['value'] = $v['value'];
                $v_new['unit_type'] = $v['unit_type'];
                $v_new['unit'] = $v['unit'];
                $v_new['factor_name'] = $v['factor_name'];
                $v_new['year'] = $v['year'];
                $v_new['coefficient'] = $v['coefficient'];
                $v_new['factor_source'] = $v['factor_source'];
                $v_new['emissions'] = $v['emissions'];
                $v_new['coefficient'] = $v['coefficient'];
                $v_new['customer_product_id'] = $product_add_back_id;
                $v_new['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
                $v_new['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
                $v_new['create_time'] = date('Y-m-d H:i:s');
                $v_new['modify_time'] = date('Y-m-d H:i:s');
                $v_new['is_del'] = SupplierProductDischargeModel::IS_DEL_NOT_DEL;
                $batch_insert_data[$k] = $v_new;
            }

            if (!empty($batch_insert_data)) {
                CustomerProductDischargeModel::addCustomerProductDischarge($batch_insert_data);
            }

            // 在供应商的关系表中,写入对应的关系信息 主要写入产品id 客户id 客户产品id
            $relation_insert_data = [
                'supplier_id'         => isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '',
                'supplier_product_id' => $data['id'],
                'customer_id'         => $data['customer_id'],
                'customer_product_id' => $product_add_back_id,
                'create_by'           => isset($data_redis['userid']) ? $data_redis['userid'] : '',
                'create_time'         => date('Y-m-d H:i:s'),
            ];

            SupplierFillProductCustomerRelationModel::addRelation($relation_insert_data);
            $customer_data = OrganizationModel::getOrganization($data['customer_id']);

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '供应链碳管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '向客户：' . $customer_data['name'] . '报送产品：' . $product_info['product_name'];
            OperationModel::addOperation($data_log);

            Db::commit();
            return json(['code'=>200, 'message'=>"报送成功"]);
        } catch (\Exception $e) {
            Db::rollback();
            return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
        }
    }

    /**
     * @notes  客户报送列表-更新报送
     * @author fengweizhe
     */
    public function updateRequestFill(Request $request) {
        //更新报送 主要是根据关系表中的客户产品id 来更新客户产品信息 以及客户产品信息的碳排放源重写
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $data['customer_id'] = isset($params_payload_arr['customer_id']) ? $params_payload_arr['customer_id'] : '';
        if ($data['customer_id'] == '' || $data['customer_id'] == null || $data['customer_id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        //获取客户产品id
        $customer_product_info = SupplierFillProductCustomerRelationModel::getInfoBySupplierProductIdAndCustomerId($data['id'], $data['customer_id']);
        if (!isset($customer_product_info['customer_product_id'])) {
            return json(['code'=>201, 'message'=>"没有找到对应得客户产品信息"]);
        }

        Db::startTrans();
        try {
            //根据客户产品id更新产品数据
            //查询供应商产品
            $product_info = SupplierProductModel::getDataById($data['id']);
            //整合数据写入产品
            $insert_data['product_name'] = $product_info['product_name'];
            $insert_data['product_model'] = $product_info['product_model'];
            $insert_data['product_type'] = $product_info['product_type'];
            $insert_data['supplier_id'] = $product_info['supplier_id'];
            $insert_data['auth'] = $product_info['auth'];
            $insert_data['state'] = CustomerProductModel::STATE_NO; // 更新报送产品不显示在采购者供应产品列表中，只有审批通过之后才会显示
            $insert_data['audit_state'] = CustomerProductModel::AUDIT_STATE_NOT_VERIFY; // 更新报送产品后审批状态变更为待审批
            $insert_data['file_url'] = $product_info['file_url'];
            $insert_data['create_time'] = date('Y-m-d H:i:s');
            $insert_data['modify_time'] = date('Y-m-d H:i:s');
            $insert_data['customer_id'] = $data['customer_id'];
            $insert_data['week_start'] = $product_info['week_start'];
            $insert_data['week_end'] = $product_info['week_end'];
            $insert_data['number'] = $product_info['number'];
            $insert_data['unit_type'] = $product_info['customer_id'];
            $insert_data['unit'] = $product_info['unit'];
            $insert_data['emissions'] = $product_info['emissions'];
            $insert_data['coefficient'] = $product_info['coefficient'];
            $data_redis = $request->middleware('data_redis');
            $insert_data['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $insert_data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $insert_data['id'] = $customer_product_info['customer_product_id'];
            CustomerProductModel::editCustomerProduct($insert_data);
            //删除客户产品源排放源信息
            CustomerProductDischargeModel::delProductDischarge($customer_product_info['customer_product_id']);
            //整合排放源及新增排放源信息
            $discharge_info = SupplierProductDischargeModel::getDataByProductId($data['id']);
            $batch_insert_data = [];
            foreach ($discharge_info as $k => $v) {
                $v_new['type_id'] = $v['type_id'];
                $v_new['type_name'] = $v['type_name'];
                $v_new['name'] = $v['name'];
                $v_new['loss_type'] = $v['loss_type'];
                $v_new['value'] = $v['value'];
                $v_new['unit_type'] = $v['unit_type'];
                $v_new['unit'] = $v['unit'];
                $v_new['factor_name'] = $v['factor_name'];
                $v_new['year'] = $v['year'];
                $v_new['factor_source'] = $v['factor_source'];
                $v_new['emissions'] = $v['emissions'];
                $v_new['coefficient'] = $v['coefficient'];
                $v_new['customer_product_id'] = $customer_product_info['customer_product_id'];
                $v_new['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
                $v_new['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
                $v_new['create_time'] = date('Y-m-d H:i:s');
                $v_new['modify_time'] = date('Y-m-d H:i:s');
                $v_new['is_del'] = SupplierProductDischargeModel::IS_DEL_NOT_DEL;
                $batch_insert_data[$k] = $v_new;
            }
            if (!empty($batch_insert_data)) {
                CustomerProductDischargeModel::addCustomerProductDischarge($batch_insert_data);
            }
            $customer_data = OrganizationModel::getOrganization($data['customer_id']);

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '供应链碳管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '向客户：' . $customer_data['name'] . '更新报送产品：' . $product_info['product_name'];
            OperationModel::addOperation($data_log);

            Db::commit();
            return json(['code'=>200, 'message'=>"更新成功"]);
        } catch (\Exception $e) {
            Db::rollback();
            return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
        }
    }

    /**
     * @notes  货品报送列表
     * @author fengweizhe
     */
    public function customerRequestFillList(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $supplier_id = $data_redis['main_organization_id'];
        $product_name = isset($_GET['product_name']) ? $_GET['product_name'] : '';
        $title = isset($_GET['title']) ? $_GET['title'] : '';
        if ($supplier_id == '' || $supplier_id == null || $supplier_id == 0) {
           return json(['code'=>201, 'message'=>"参数supplier_id错误"]);
        }

        //这里是查询出客户列表中审核状态为待审核的数据信息  这里的数据  不进行回显 就是那几个字段的信息不进行回写  只推送碳排放信息
        $list = CustomerProductModel::getNotVerifyListBySupplerId($supplier_id, $product_name, $title);
        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }

    /**
     * $notes 货品报送 上报已经存在的商品
     * 
     * @author wuyinghua
     */
    public function subbmitFillExistProduct(Request $request) {
        //前端传客户id和产品的id过来还有客户端的产品id也要传过来 然后自己进行客户产品的相关信息的更新操作 然后产品名称 型号 类型 供应商 创建时间 创建人不改变
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['customer_id'] = isset($params_payload_arr['customer_id']) ? $params_payload_arr['customer_id'] : '';
        if ($data['customer_id'] == '' || $data['customer_id'] == null || $data['customer_id'] == 0) {
            return json(['code'=>201, 'message'=>"参数customer_id错误"]);
        }
        $data['customer_product_id'] = isset($params_payload_arr['customer_product_id']) ? $params_payload_arr['customer_product_id'] : '';
        if ($data['customer_product_id'] == '' || $data['customer_product_id'] == null || $data['customer_product_id'] == 0) {
            return json(['code'=>201, 'message'=>"参数customer_product_id错误"]);
        }
        $data['supplier_product_id'] = isset($params_payload_arr['supplier_product_id']) ? $params_payload_arr['supplier_product_id'] : '';
        if ($data['supplier_product_id'] == '' || $data['supplier_product_id'] == null) {
            return json(['code'=>201, 'message'=>"参数supplier_product_id错误"]);
        }
        //查询供应商产品
        $product_info = SupplierProductModel::getDataById($data['supplier_product_id']);
        //查询供客户产品
        $customer_product_info = CustomerProductModel::getDataById($data['customer_product_id']);

        //整合数据写入产品
        $insert_data['auth'] = $product_info['auth'];
        $insert_data['file_url'] = $product_info['file_url'];
        $insert_data['modify_time'] = date('Y-m-d H:i:s');
        $insert_data['audit_state'] = $product_info['audit_state'];
        $insert_data['week_start'] = $product_info['week_start'];
        $insert_data['week_end'] = $product_info['week_end'];
        $insert_data['number'] = $product_info['number'];
        $insert_data['emissions'] = $product_info['emissions'];
        $insert_data['coefficient'] = $product_info['coefficient'];
        $insert_data['unit_type'] = $product_info['unit_type'];
        $insert_data['unit'] = $product_info['unit'];
        $data_redis = $request->middleware('data_redis');
        $insert_data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';

        Db::startTrans();
        try {
            // 根据客户产品id进行更新
            $insert_data['id'] = $data['customer_product_id'];
            CustomerProductModel::editCustomerProduct($insert_data);
            // 把客户端产品关联的排放源信息全部删除
            CustomerProductDischargeModel::delProductDischarge($data['customer_product_id']);
            // 查询出供应商产品的排放源信息并写入到客户端产品的排放源表中
            $discharge_info = SupplierProductDischargeModel::getDataByProductId($data['supplier_product_id']);
            $batch_insert_data = [];
            foreach ($discharge_info as $k => $v) {
                $v_new['type_id'] = $v['type_id'];
                $v_new['type_name'] = $v['type_name'];
                $v_new['name'] = $v['name'];
                $v_new['loss_type'] = $v['loss_type'];
                $v_new['value'] = $v['value'];
                $v_new['unit_type'] = $v['unit_type'];
                $v_new['unit'] = $v['unit'];
                $v_new['factor_name'] = $v['factor_name'];
                $v_new['year'] = $v['year'];
                $v_new['coefficient'] = $v['coefficient'];
                $v_new['factor_source'] = $v['factor_source'];
                $v_new['emissions'] = $v['emissions'];
                $v_new['coefficient'] = $v['coefficient'];
                $v_new['customer_product_id'] = $data['customer_product_id'];
                $v_new['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
                $v_new['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
                $v_new['create_time'] = date('Y-m-d H:i:s');
                $v_new['modify_time'] = date('Y-m-d H:i:s');
                $v_new['is_del'] = SupplierProductDischargeModel::IS_DEL_NOT_DEL;
                $batch_insert_data[$k] = $v_new;
            }
            if (!empty($batch_insert_data)) {
                CustomerProductDischargeModel::addCustomerProductDischarge($batch_insert_data);
            }

            // 供应商填报了对应产品信息后，碳数据将暂时写入对应的供应产品表中，此时状态将由未填报变为待审核
            CustomerProductModel::updateAuditState($data['customer_product_id'], CustomerProductModel::AUDIT_STATE_NOT_VERIFY);

            // 在供应商的关系表中,写入对应的关系信息 主要写入产品id 客户id 客户产品id
            $relation_insert_data = [
                'supplier_id'         => isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '',
                'supplier_product_id' => $data['supplier_product_id'],
                'customer_id'         => $data['customer_id'],
                'customer_product_id' => $data['customer_product_id'],
                'create_by'           => isset($data_redis['userid']) ? $data_redis['userid'] : '',
                'create_time'         => date('Y-m-d H:i:s'),
            ];

            SupplierFillProductCustomerRelationModel::addRelation($relation_insert_data);
            $customer_data = OrganizationModel::getOrganization($data['customer_id']);

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '供应链碳管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '对客户：' . $customer_data['name'] . '的产品：' . $customer_product_info['product_name'] . '选择已有产品' . $product_info['product_name'] . '进行报送';
            OperationModel::addOperation($data_log);

            Db::commit();
            return json(['code'=>200, 'message'=>"上报成功"]);
        } catch (\Exception $e) {
            Db::rollback();
            return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
        }
    }

    /**
     * $notes 货品报送 上报新增的商品
     * 
     * @author wuyinghua
     */
    public function subbmitFillNewAdd(Request $request) {
        if (request()->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $params['customer_id'] = isset($params_payload_arr['customer_id']) ? $params_payload_arr['customer_id'] : '';
            if ($params['customer_id'] == '' || $params['customer_id'] == null || $params['customer_id'] == 0) {
                return json(['code'=>201, 'message'=>"参数customer_id错误"]);
            }
            $param_customer_product_id = isset($params_payload_arr['customer_product_id']) ? $params_payload_arr['customer_product_id'] : '';
            if ($param_customer_product_id == '' || $param_customer_product_id == null || $param_customer_product_id == 0) {
                return json(['code'=>201, 'message'=>"参数customer_product_id错误"]);
            }
            // 写入供应商方的产品和排放源表
            $data_redis = $request->middleware('data_redis');
            $data['supplier_id'] = isset($data_redis['main_organization_id']) ? $data_redis['main_organization_id'] : '';
            $data['product_name'] = $params_payload_arr['product_name'];
            $data['product_model'] = $params_payload_arr['product_model'];
            $data['product_type'] = $params_payload_arr['product_type'];
            $data['auth'] = $params_payload_arr['auth'];
            $data['week_start'] = $params_payload_arr['week_start'];
            $data['week_end'] = $params_payload_arr['week_end'];
            $data['number'] = $params_payload_arr['number'];
            $data['unit_type'] = $params_payload_arr['unit_type'];
            $data['unit'] = $params_payload_arr['unit'];
            $data['file_url'] = isset($params_payload_arr['file_url']) ? $params_payload_arr['file_url'] : '';
            $data['discharge'] = $params_payload_arr['discharge'];
            $data['create_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $discharge = $data['discharge'];
            unset($data['discharge']);

            // 封装排放因子数据, 以及产品排放量和排放系数
            foreach ($discharge as $dk => &$dv) {
                // 数量与因子分母单位不一致时，先判断是否能通过数据字典中单位系数进行换算，如果不能换算则排放量信息展示为空，不纳入计算
                $factor_denominator_arr = explode('/', $dv['factor_unit']); // factor_unit，格式'kgCO2/kg'
                $factor_denominator_name = end($factor_denominator_arr);
                $factor_unit_data = UnitModel::getInfoByName($factor_denominator_name);
                $data_management_unit_data = UnitModel::findUnit($dv['unit']);
                if ($factor_unit_data != NULL && $factor_unit_data['type_id'] == $dv['unit_type'] && $data_management_unit_data != NULL) {
                    $dv['emissions'] = $dv['value'] * $dv['coefficient'] * $data_management_unit_data['conversion_ratio'] / $factor_unit_data['conversion_ratio'];
                } else {
                    $dv['emissions'] = NULL;
                }
            }

            // 计算出产品的碳排放量
            $emissions_arr = array_column($discharge, 'emissions');
            $product_emissions = array_sum($emissions_arr);
            $data['emissions'] = $product_emissions;
            $data['coefficient'] = $product_emissions / $data['number'];

            // 根据客户产品id进行更新
            $data_customer_update['auth'] = $data['auth'];
            $data_customer_update['file_url'] = $data['file_url'];
            $data_customer_update['modify_time'] = date('Y-m-d H:i:s');
            $data_customer_update['week_start'] = $data['week_start'];
            $data_customer_update['week_end'] = $data['week_end'];
            $data_customer_update['number'] = $data['number'];
            $data_customer_update['emissions'] = $data['emissions'];
            $data_customer_update['coefficient'] = $data['coefficient'];
            $data_customer_update['unit_type'] = $data['unit_type'];
            $data_customer_update['unit'] = $data['unit'];
            $data_customer_update['modify_by'] = isset($data_redis['userid']) ? $data_redis['userid'] : '';
            $data_customer_update['id'] = $param_customer_product_id;

            //查询供客户产品
            $customer_product_info = CustomerProductModel::getDataById($param_customer_product_id);

            Db::startTrans();
            try {
                $product_id = SupplierProductModel::addSupplierProduct($data);
                $discharge_insert = [];
                foreach ($discharge as $kk => $vv) {
                    $vv['supplier_product_id'] = $product_id;
                    $discharge_insert[$kk] = $vv;
                }
                SupplierProductDischargeModel::addSupplierProductDischarge($discharge_insert);

                // 根据客户产品id进行更新
                CustomerProductModel::editCustomerProduct($data_customer_update);

                // 把客户端产品关联的排放源信息全部删除
                CustomerProductDischargeModel::delProductDischarge($param_customer_product_id);

                $customer_discharge_insert = [];
                foreach ($discharge as $ck => $cv) {
                    $cv['customer_product_id'] = $param_customer_product_id;
                    $customer_discharge_insert[$ck] = $cv;
                }
                CustomerProductDischargeModel::addCustomerProductDischarge($customer_discharge_insert);

                // 供应商填报了对应产品信息后，碳数据将暂时写入对应的供应产品表中，此时状态将由未填报变为待审核
                CustomerProductModel::updateAuditState($param_customer_product_id, CustomerProductModel::AUDIT_STATE_NOT_VERIFY);

                // 在供应商的关系表中,写入对应的关系信息 主要写入产品id 客户id 客户产品id
                $relation_insert_data = [
                    'supplier_id'         => $data['supplier_id'],
                    'supplier_product_id' => $product_id,
                    'customer_id'         => $params['customer_id'],
                    'customer_product_id' => $param_customer_product_id,
                    'create_by'           => isset($data_redis['userid']) ? $data_redis['userid'] : '',
                    'create_time'         => date('Y-m-d H:i:s'),
                ];

                SupplierFillProductCustomerRelationModel::addRelation($relation_insert_data);
                $customer_data = OrganizationModel::getOrganization($params['customer_id']);

                // 添加操作日志
                $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                $data_log['user_id'] = $data_redis['userid'];
                $data_log['module'] = '供应链碳管理';
                $data_log['type'] = '功能操作';
                $data_log['time'] = date('Y-m-d H:i:s');
                $data_log['url'] = $request->pathinfo();
                $data_log['log'] = '对客户：' . $customer_data['name'] . '的产品：' . $customer_product_info['product_name'] . '选择新增产品' . $data['product_name'] . '进行报送';
                OperationModel::addOperation($data_log);

                Db::commit();
                return json(['code'=>200, 'message'=>"添加成功"]);
            } catch (\Exception $e) {
                Db::rollback();
                return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
            }
        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * addFactorForm
     *
     * @author wuyinghua
     * @return void
     */
    public function addFactorForm() {
        $select_list = FactorTypeModel::getList();
        $data['code'] = 200;
        $data['data']['list'] = $select_list;

        return json($data);
    }

    /**
     * checkProduct 验证产品
     * 
     * @author wuyinghua
     * @param $supplier_id
     * @param $product_name
     * @param $product_model
     * @param $id
	 * @return void
     */
    public function checkProduct($supplier_id, $product_name, $product_model, $id = NULL) {
        $list = SupplierProductModel::getAllProducts($supplier_id, $id)->toArray();

        $combine_products = [];
        foreach ($list as $key => $value) {
            $combine_products[$key] = $value['product_name'] . '-' . $value['product_model'];
        }

        $combine_product = $product_name . '-' . $product_model;

        if (in_array($combine_product, $combine_products)) {
            return false;
        } else {
            return  true;
        }
    }

    /**
     * validateForm 验证
     *
     * @author wuyinghua
     * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(SupplierProductValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {

            return $e->getError();
        }
    }
}