<?php

namespace app\api\controller\supply;

use app\BaseController;
use app\model\admin\IndustryModel;
use app\model\supply\CustomerModel;
use app\model\supply\SupplierModel;
use app\model\system\OrganizationModel;
use app\model\system\OperationModel;
use think\Request;

class Supplier extends BaseController
{
    /**
     * 我是供应商 查看的是采购者的信息
     */

    /**
     * @notes 供应商客户的列表
     * @author fengweizhe
     */
    public function index(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $title = isset($_GET['title']) ? $_GET['title'] : '';
        $state = isset($_GET['state']) ? $_GET['state'] : '';
        $industry_id = isset($_GET['industry_id']) ? $_GET['industry_id'] : '';
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : CustomerModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : CustomerModel::pageIndex;
        $main_id = $data_redis['main_organization_id'];
        $list = CustomerModel::getList($page_size, $page_index, ['title'=>$title, 'state'=>$state,
            'industry_id'=>$industry_id, 'main_id'=>$main_id])->toArray();
        $industry_id_map = IndustryModel::getList();
        $state_select_map = CustomerModel::STATE_SELECT_MAP;
        $state_map = CustomerModel::STATE_MAP;

        $list_new = [];
        foreach ($list['data'] as $k => $v) {
            $supplier_product = SupplierModel::getProductCountByCustomerId($v['id'], $main_id);
            if ($supplier_product != null) {
                $v['number'] = $supplier_product['count'];
            } else {
                $v['number'] = '';
            }

            if (isset($state_map[$v['state']])) {
                $v['state_name'] = $state_map[$v['state']];
            } else {
                $v['state_name'] = '';
            }

            $list_new[$k] = $v;
        }

        unset($list['data']);

        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['industry_id'] = $industry_id_map;
        $data['data']['state'] = $state_select_map;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * @notes 供应商客户的详情
     * @author fengweizhe
     */
    public function info(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $customer_data['main_id'] = $data_redis['main_organization_id'];
        $customer_data['id'] = isset($_GET['id']) ? $_GET['id'] : '';
        $info = CustomerModel::getDataById($customer_data);
        if ($info == null) {
            return json(['code'=>201, 'message'=>"未查询到客户供应商相关信息"]);
        }

        $supplier_product = SupplierModel::getProductCountByCustomerId($customer_data['id'], $customer_data['main_id']);
        if ($supplier_product != null) {
            $info['product_count'] = $supplier_product['count'];
        } else {
            $info['product_count'] = '';
        }

        $data['code'] = 200;
        $data['data'] = $info;

        return json($data);
    }

    /**
     * @notes 供应商客户的启用/禁用
     * @author fengweizhe
     */
    public function state(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $data['main_id'] = $data_redis['main_organization_id'];
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $data['state'] = isset($params_payload_arr['state']) ? $params_payload_arr['state'] : '';
        if ($data['state'] == '' || $data['state'] == null || $data['state'] == 0) {
            return json(['code'=>201, 'message'=>"参数state错误"]);
        }

        if (!in_array($data['state'], [CustomerModel::STATE_YES, CustomerModel::STATE_NO])) {
            return json(['code'=>201, 'message'=>"参数state错误"]);
        }

        CustomerModel::updateState($data);
        $state_select_map = SupplierModel::STATE_SELECT_MAP;
        $supplier_data = OrganizationModel::getOrganization($data['id']);

        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '供应链碳管理';
        $data_log['type'] = '功能操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $request->pathinfo();
        $data_log['log'] = '更新客户：' . $supplier_data['name'] . '的状态' . '为：' . $state_select_map[$data['state'] - 1]['name'];
        OperationModel::addOperation($data_log);

        if ($data['state'] == SupplierModel::STATE_YES) {
            return json(['code'=>200, 'message'=>"启用成功"]);
        } else {
            return json(['code'=>200, 'message'=>"禁用成功"]);
        }
    }
}