<?php

namespace app\api\controller\supply;

use app\BaseController;
use app\model\admin\IndustryModel;
use app\controller\system\User;
use app\model\supply\SupplierModel;
use app\model\supply\CustomerProductModel;
use app\model\supply\CustomerProductDischargeModel;
use app\model\system\OrganizationModel;
use app\model\system\OperationModel;
use app\model\system\UserModel;
use think\facade\Db;
use think\Request;

class Customer extends BaseController
{
    /**
     * 我是采购者  查看的是供应商的信息
     */

    /**
     * @notes 客户供应商的列表仓配OMS 01月16日 09.30-11.30 V1.6.0版本上线
     * @author fengweizhe
     */
    public function index(Request $request) {
        $data_redis = $request->middleware('data_redis');

        $title = isset($_GET['title']) ? $_GET['title'] : '';
        $state = isset($_GET['state']) ? $_GET['state'] : '';
        $risk_state = isset($_GET['risk_state']) ? $_GET['risk_state'] : '';
        $industry_id = isset($_GET['industry_id']) ? $_GET['industry_id'] : '';
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : SupplierModel::pageSize;
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : SupplierModel::pageIndex;
        $main_id = $data_redis['main_organization_id'];
        $list = SupplierModel::getList($page_size, $page_index, ['title'=>$title, 'state'=>$state,
            'risk_state'=>$risk_state, 'industry_id'=>$industry_id, 'main_id'=>$main_id])->toArray();

        $industry_id_map = IndustryModel::getList();
        $state_select_map = SupplierModel::STATE_SELECT_MAP;
        $risk_state_select_map = SupplierModel::RISK_STATE_SELECT_MAP;
        $state_map = SupplierModel::STATE_MAP;
        $risk_state_map = SupplierModel::RISK_STATE_MAP;

        $list_new = [];
        foreach ($list['data'] as $k => $v) {
            $supplier_product = SupplierModel::getProductCountBySupplierId($main_id, $v['id']);
            if ($supplier_product != null) {
                $v['number'] = $supplier_product['count'];
            } else {
                $v['number'] = '';
            }

            if (isset($state_map[$v['state']])) {
                $v['state_name'] = $state_map[$v['state']];
            } else {
                $v['state_name'] = '';
            }
            if (isset($risk_state_map[$v['risk_state']])) {
                $v['risk_state_name'] = $risk_state_map[$v['risk_state']]; 
            } else {
                $v['risk_state_name'] = '';
            }
            $list_new[$k] = $v;
        }
        unset($list['data']);

        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['industry_id'] = $industry_id_map;
        $data['data']['state'] = $state_select_map;
        $data['data']['risk_state'] = $risk_state_select_map;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * @notes 客户供应商的详情

     * @author fengweizhe
     */
    public function info(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $data['main_id'] = $data_redis['main_organization_id'];
        $data['id'] = isset($_GET['id']) ? $_GET['id'] : '';
        $info = SupplierModel::getDataById($data);
        if ($info == null) {
            return json(['code'=>201, 'message'=>"未查询到客户供应商相关信息"]);
        }

        $state_map = SupplierModel::STATE_MAP;
        $risk_state_map = SupplierModel::RISK_STATE_MAP;

        $info_new = [];
        foreach ($info as $k => $v) {
            if ($k == 'state' ) {
                $v = $state_map[$v];
            }
            if ($k == 'risk_state') {
                $v = $risk_state_map[$v];
            }
            $info_new[$k] = $v;
        }

        $supplier_product = SupplierModel::getProductCountBySupplierId($data['main_id'], $data['id']);
        if ($supplier_product != null) {
            $info_new['number'] = $supplier_product['count'];
        } else {
            $info_new['number'] = '';
        }

        unset($info);
        $data['code'] = 200;
        $data['data'] = $info_new;

        return json($data);
    }

    /**
     * @notes 客户供应商的启用/禁用
     * @author fengweizhe
     */
    public function state(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $data['main_id'] = $data_redis['main_organization_id'];
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $data['state'] = isset($params_payload_arr['state']) ? $params_payload_arr['state'] : '';
        if ($data['state'] == '' || $data['state'] == null || $data['state'] == 0) {
            return json(['code'=>201, 'message'=>"参数state错误"]);
        }

        if (!in_array($data['state'], [SupplierModel::STATE_YES, SupplierModel::STATE_NO])) {
            return json(['code'=>201, 'message'=>"参数state错误"]);
        }

        Db::startTrans();
        try {
            if ($data['state'] == SupplierModel::STATE_NO) {
                // 获取被禁用供应商所有待审核的产品
                $audit_list = CustomerProductModel::getAllAudits($data['main_id'], $data['id'])->toArray();

                foreach ($audit_list as $value) {
                    // 被禁用的供应商处于待审核状态的产品报送会立即被驳回
                    CustomerProductModel::updateAuditState($value['id'], CustomerProductModel::AUDIT_STATE_EDITING);

                    $update_product_data['id'] = $value['id'];
                    $update_product_data['auth'] = '';
                    $update_product_data['week_start'] = '';
                    $update_product_data['week_end'] = '';
                    $update_product_data['number'] = '';
                    $update_product_data['unit_type'] = '';
                    $update_product_data['unit'] = '';
                    $update_product_data['emissions'] = '';
                    $update_product_data['coefficient'] = '';

                    // 驳回的产品需要清空碳排放信息
                    CustomerProductModel::editCustomerProduct($update_product_data);
                    CustomerProductDischargeModel::delProductDischarge($value['id']);
                }
            }

            SupplierModel::updateState($data);
            $state_select_map = SupplierModel::STATE_SELECT_MAP;
            $supplier_data = OrganizationModel::getOrganization($data['id']);

            // 添加操作日志
            $data_log['main_organization_id'] = $data_redis['main_organization_id'];
            $data_log['user_id'] = $data_redis['userid'];
            $data_log['module'] = '供应链碳管理';
            $data_log['type'] = '功能操作';
            $data_log['time'] = date('Y-m-d H:i:s');
            $data_log['url'] = $request->pathinfo();
            $data_log['log'] = '更新供应商：' . $supplier_data['name'] . '的状态' . '为：' . $state_select_map[$data['state'] - 1]['name'];
            OperationModel::addOperation($data_log);

            Db::commit();
            if ($data['state'] == SupplierModel::STATE_YES) {
                return json(['code'=>200, 'message'=>"启用成功"]);
            } else {
                return json(['code'=>200, 'message'=>"禁用成功"]);
            }
        } catch (\Exception $e) {
            Db::rollback();
            return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
        }
    }

    /**
     * @notes 客户供应商风险状态调整表单获取
     * 
     * @author wuyinghua
     * @return \think\response\Json
     */
    public function riskStateMap() {
        $risk_state_select_map = SupplierModel::RISK_STATE_SELECT_MAP;
        $data['code'] = 200;
        $data['data'] = $risk_state_select_map;

        return json($data);
    }

    /**
     * @notes 客户供应商的风险状态调整
     * @author fengweizhe
     */
    public function riskState(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $data['main_id'] = $data_redis['main_organization_id'];
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
        if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
            return json(['code'=>201, 'message'=>"参数id错误"]);
        }

        $data['risk_state'] = isset($params_payload_arr['risk_state']) ? $params_payload_arr['risk_state'] : '';
        if ($data['risk_state'] == '' || $data['risk_state'] == null || $data['risk_state'] == 0) {
            return json(['code'=>201, 'message'=>"参数risk_state错误"]);
        }

        SupplierModel::updateRiskState($data);
        $risk_state_select_map = SupplierModel::RISK_STATE_SELECT_MAP;
        $supplier_data = OrganizationModel::getOrganization($data['id']);

        // 添加操作日志
        $data_log['main_organization_id'] = $data_redis['main_organization_id'];
        $data_log['user_id'] = $data_redis['userid'];
        $data_log['module'] = '供应链碳管理';
        $data_log['type'] = '功能操作';
        $data_log['time'] = date('Y-m-d H:i:s');
        $data_log['url'] = $request->pathinfo();
        $data_log['log'] = '更新供应商：' . $supplier_data['name'] . '的风险状态' . '为：' . $risk_state_select_map[$data['risk_state'] - 1]['name'];
        OperationModel::addOperation($data_log);

        return json(['code'=>200, 'message'=>"风险等级变更成功"]);
    }

    /**
     * @notes 生成供应商邀请链接
     * 
     * @author wuyinghua
     */
    public function supplierInvite(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $data['main_id'] = $data_redis['main_organization_id'];
        $data['create_by'] = $data_redis['userid'];
        $data['create_time'] = date('Y-m-d H:i:s');
        // 加密方式：id&time
        $str = $data['main_id'] . '&' . time();
        $data['invite_code'] = User::encrypt($str, 'E', UserModel::ENCRYPT_KEY);
        $url = $_SERVER["HTTP_REFERER"] . '#/supplierinvite?invite_code=' . $data['invite_code'];
        $add = SupplierModel::addSupplierInvite($data);

        if ($add) {
            return json(['code'=>200, 'message'=>"供应商邀请链接生成成功", 'url'=>$url]);
        } else {
            return json(['code'=>201, 'message'=>"供应商邀请链接生成失败"]);
        }
    }

    /**
     * @notes 供应商确认绑定
     * 
     * @author wuyinghua
     */
    public function supplierConfirm(Request $request) {
        $data_redis = $request->middleware('data_redis');
        $invite_id = $data_redis['main_organization_id'];
        $params_payload = $this->req_payload;
        $params_payload_arr = json_decode($params_payload, true);
        $confirm = $params_payload_arr['confirm'];

        // 判断邀请码是否已使用
        $supplier_invite_data = SupplierModel::getSupplierInviteByCode($params_payload_arr['invite_code']);
        if ($supplier_invite_data['is_use'] == SupplierModel::IS_USE_NO) {

            // 用户确认绑定
            if ($confirm == SupplierModel::CONFIRM_TYPE_YES) {
                // 解密
                $encrypt_str = User::encrypt($params_payload_arr['invite_code'], 'D', UserModel::ENCRYPT_KEY);
                $encrypt_arr = explode('&', $encrypt_str);
                $main_id = $encrypt_arr[0];
                if ($invite_id == $main_id) {
                    return json(['code'=>201, 'message'=>"接受邀请失败，无法与自己绑定供应关系"]);
                }

                // 不可重复绑定供应关系
                $relation_data = SupplierModel::getRelation($main_id, $invite_id)->toArray();
                if ($relation_data != NULL) {
                    return json(['code'=>201, 'message'=>"接受邀请失败，无法重复绑定供应关系"]);
                }

                Db::startTrans();
                try {
                    // 供应商邀请表中更新邀请码使用状态
                    $data_supplie_invite['is_use'] = UserModel::USE_TYPE_YES;
                    $data_supplie_invite['invite_code'] = $params_payload_arr['invite_code'];
                    SupplierModel::updateSupplierInvite($data_supplie_invite);

                    // 供应商采购者关系表建立关联 主体采购者
                    $data_relation_customer['main_id'] = $main_id;
                    $data_relation_customer['relation_id'] = $invite_id;
                    $data_relation_customer['is_supplier'] = SupplierModel::IS_MAIN_NO;
                    $data_relation_customer['is_customer'] = SupplierModel::IS_MAIN_YES;
                    $data_relation_customer['create_time'] = date('Y-m-d H:i:s');
                    $data_relation_customer['modify_time'] = date('Y-m-d H:i:s');

                    SupplierModel::addRelation($data_relation_customer);

                    // 供应商采购者关系表建立关联 主体供应商
                    $data_relation_supplier['main_id'] = $invite_id;
                    $data_relation_supplier['relation_id'] = $main_id;
                    $data_relation_supplier['is_supplier'] = SupplierModel::IS_MAIN_YES;
                    $data_relation_supplier['is_customer'] = SupplierModel::IS_MAIN_NO;
                    $data_relation_supplier['create_time'] = date('Y-m-d H:i:s');
                    $data_relation_supplier['modify_time'] = date('Y-m-d H:i:s');

                    SupplierModel::addRelation($data_relation_supplier);

                    // 添加操作日志
                    $customer_data = OrganizationModel::getOrganization($invite_id);
                    $supplier_data = OrganizationModel::getOrganization($main_id);
                    $data_log['main_organization_id'] = $data_redis['main_organization_id'];
                    $data_log['user_id'] = $data_redis['userid'];
                    $data_log['module'] = '供应链碳管理';
                    $data_log['type'] = '功能操作';
                    $data_log['time'] = date('Y-m-d H:i:s');
                    $data_log['url'] = $request->pathinfo();
                    $data_log['log'] = $customer_data['name'] . '接受' . $supplier_data['name'] . '的邀请成为供应商';
                    OperationModel::addOperation($data_log);

                    Db::commit();
                    return json(['code'=>200, 'message'=>"接受邀请成功"]);
                } catch (\Exception $e) {
                    Db::rollback();
                    return json(['code'=>201, 'message'=>$e->getMessage().$e->getLine()]);
                }
            } else {
                return json(['code'=>200, 'message'=>"拒绝邀请"]);
            }
        } else {
            return json(['code'=>201, 'message'=>"邀请码已被使用"]);
        }
    }

    /**
     * 获取客户端IP地址
     * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
     * @author wuyinghua
     * @return mixed
    */
    public function get_client_url(){
        $client_url = 'http';
        $client_url .= "://";
        
        if ($_SERVER["SERVER_PORT"] != "80") {
            // $client_url .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"];
            $client_url .= $_SERVER["SERVER_ADDR"] . ":" . $_SERVER["SERVER_PORT"]; // IP
        } else {
            // $client_url .= $_SERVER["SERVER_NAME"];
            $client_url .= $_SERVER["SERVER_ADDR"];
        }

        return $client_url;
    }

    /**
     * 获取客户端IP地址
     * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
     * @author wuyinghua
     * @return mixed
    */
    public function get_client_ip($type = 0) {
        $type = $type ? 1 : 0;
        static $ip = NULL;
       
        if ($ip !== NULL) return $ip[$type];
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos = array_search('unknown',$arr);
            if(false !== $pos) unset($arr[$pos]);
            $ip = trim($arr[0]);
        }elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        // IP地址合法验证
        $long = sprintf("%u",ip2long($ip));
        $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);

        return $ip[$type];
    }
}